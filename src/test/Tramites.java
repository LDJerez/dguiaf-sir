package test;

import java.io.IOException;
import java.util.List;

import javax.swing.JOptionPane;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.apache.pdfbox.pdmodel.font.PDFont;

import static org.testng.Assert.assertTrue;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import org.apache.pdfbox.cos.COSDocument;
import org.apache.pdfbox.pdfparser.PDFParser;
import org.apache.pdfbox.pdmodel.PDDocument;
import pages.BUIPage;
import pages.Login;
import pages.TramitesPage;
import pages.setConexion;


public class Tramites  {

	protected WebDriver driver;
	protected  WebDriverWait wait;
	String usuario ;
	String paswd ;

	@AfterClass
	public void CloseChrome() {
		//JOptionPane.showMessageDialog(null, "CERRAR NAVEGADOR");
		driver.quit();	
	}

	@BeforeClass
	public void Init() throws Exception	{
		this.driver = setConexion.setup();
		wait = new WebDriverWait(driver, 10);
	}	 

	@AfterMethod
	public void IngresoAmbiente() {
		TestBase TB = new TestBase(driver);
		TB.PortalTramites();
	}
	
	/*
	
	 
	 
	 
	 
	 */

	public void espera(int espera) {
		try {Thread.sleep(espera);} catch (InterruptedException e) {e.printStackTrace();}
	}
	/*
	 ********************TEST********************
	 */
	
	@Test
	public void TS75_Portal_tramite_Pago_de_boleta_con_MP_VISA() {
		String tipo = "Documento Nacional de Identidad";
		String nroDoc= "1000000";
		String mail = "correo@prueba.com";
		TramitesPage TP = new TramitesPage(driver);
		TestBase TB = new TestBase(driver);
		BUIPage BUI = new BUIPage(driver);
		String nroBoleta = BUI.GenerarBoletaPagoVisa();
		TB.PortalTramites();
		TP.menu("Paga tu Boleta");
		TP.DatosContribuyentePago(tipo, nroDoc, nroBoleta, mail);
		espera(5000);
		TP.SeleccionarFormaDepago("VISA");
		TP.btnRealizarPago();
		TP.DatosTarjeta();
		Assert.assertTrue(TP.ValidarPagoVISA());
	}

	@Test
	public void TS76_Portal_tramite_Pago_de_tramite_con_MP_forzar_BUI() {
		String nombre = "Testing Automatizado";
		String tipo = "Documento Nacional de Identidad";
		String nroDoc= "1000000";
		String mail = "correo@prueba.com";
		String direccion = "calle_prueba";
		String piso = "";
		String dpto = "";
		String obs = "TEST";
		String localidad = "CABA";
		String CP = "1234	";	
		TramitesPage TP = new TramitesPage(driver);
		TP.SeleccionarTramite("Disolución de Unión Civil");
		TP.DatosContribuyenteDiso(nombre, tipo, nroDoc, mail, direccion, piso, dpto, obs, localidad, CP);
		Assert.assertTrue(TP.ValidarForzarBUI());	
	}

	@Test
	public void TS37_Portal_tramite_Pago_de_tramite_con_MP_Visa() {

		String nombre = "Testing Automatizado";
		String tipo = "Documento Nacional de Identidad";
		String nroDoc= "1000000";
		String mail = "correo@prueba.com";
		String direccion = "calle_prueba";
		String piso = "";
		String dpto = "";
		String obs = "TEST";
		String localidad = "CABA";
		String CP = "1234	";	
		String tramite = "tramite_automatizacion_001";

		TramitesPage TP = new TramitesPage(driver);
		TP.SeleccionarTramite(tramite);

		TP.DatosContribuyenteDiso2(nombre, tipo, nroDoc, mail, direccion, piso, dpto, obs, localidad, CP); //aca deberia tildar xq la cantidad es variable
		Assert.assertTrue(TP.ValidarMpVisa());

	}

	@Test
	public void TS78_Portal_tramite_Pago_de_boleta_con_MP_INTERBANKING() {//ES CASI IGUAL QUE EL TS02
		String tipo = "Documento Nacional de Identidad";
		String nroDoc= "1000000";
		String mail = "correo@prueba.com";
		TramitesPage TP = new TramitesPage(driver);
		TestBase TB = new TestBase(driver);
		BUIPage BUI = new BUIPage(driver);
		String nroBoleta = BUI.GenerarBoletaPagoVisa();

		TB.PortalTramites();
		TP.menu("Paga tu Boleta");
		TP.DatosContribuyentePago(tipo, nroDoc, nroBoleta, mail);
		TP.SeleccionarFormaDepago("INTERBANKING");
		TP.btnRealizarPago();
		Assert.assertTrue(TP.ValidarPagoINTERBANKING());
	}

	@Test
	public void TS79_Portal_tramite_Pago_de_boleta_con_MP_RAPIPAGO() {//ES CASI IGUAL QUE EL TS02
		String tipo = "Documento Nacional de Identidad";
		String nroDoc= "1000000";
		String mail = "correo@prueba.com";
		TramitesPage TP = new TramitesPage(driver);
		TestBase TB = new TestBase(driver);
		BUIPage BUI = new BUIPage(driver);
		String nroBoleta = BUI.GenerarBoletaPagoVisa();

		TB.PortalTramites();
		TP.menu("Paga tu Boleta");
		TP.DatosContribuyentePago(tipo, nroDoc, nroBoleta, mail);

		TP.SeleccionarFormaDepago("RAPIPAGO");
		TP.btnRealizarPago();

		Assert.assertTrue(TP.ValidarPagoRAPIPAGO());
	}

	@Test
	public void TS77_Portal_tramite_Pago_de_boleta_con_MP_REDLINK() {//ES CASI IGUAL QUE EL TS02
		String tipo = "Documento Nacional de Identidad";
		String nroDoc= "1000000";
		String mail = "correo@prueba.com";
		TramitesPage TP = new TramitesPage(driver);
		TestBase TB = new TestBase(driver);
		BUIPage BUI = new BUIPage(driver);
		String nroBoleta = BUI.GenerarBoletaPagoVisa();

		TB.PortalTramites();
		TP.menu("Paga tu Boleta");
		TP.DatosContribuyentePago(tipo, nroDoc, nroBoleta, mail);

		TP.SeleccionarFormaDepago("REDLINK");
		TP.btnRealizarPago();

		Assert.assertTrue(TP.ValidarPagoREDLINK());
	}

	@Test
	public void TS49_Portal_tramite_Pago_de_tramite_con_BUI() {
		/*
		 * PRECONDICIONES:
		 * TENER BUI Y TARJETA VISA COMO METODO DE PAGO
		 * CLIENTE JBAUCHI ASOCIADO AL tramite_automatizacion_697conmediodepagoBUI
		 */
		
		String nombre = "Testing Automatizado";
		String tipo = "Documento Nacional de Identidad";
		String nroDoc= "12345678";
		String mail = "correo@prueba.com";
		String direccion = "calle_prueba";
		String piso = "";
		String dpto = "";
		String obs = "TEST";
		String localidad = "CABA";
		String CP = "1234";	
		String nombreTramite = "tramite_automatizacion_697tramite_automatizacion_697conmediodepagoBUI";
		String nroBoleta;
		TramitesPage TP = new TramitesPage(driver);
		TestBase TB = new TestBase(driver);
		TB.PortalTramites();
		espera(8000);
		TP.SeleccionarTramite(nombreTramite);
		espera(5000);
		TP.DatosContribuyenteDiso2(nombre, tipo, nroDoc, mail, direccion, piso, dpto, obs, localidad, CP);
		TP.SeleccionarFormaDepago("BUI");
		TP.btnRealizarPago();
		
		nroBoleta = TP.direcionPagoElectronicoParaObtenerNroBuiParaTS49().substring(5);
		TB.PortalTramites();
		TP.menu("Paga tu Boleta");
		TP.DatosContribuyentePago(tipo, nroDoc, nroBoleta, mail);
		TP.SeleccionarFormaDepago("VISA");
		TP.btnRealizarPago();
		TP.DatosTarjeta();
		WebElement textoExito = driver.findElement(By.cssSelector(".texto"));
		Assert.assertTrue(textoExito.getText().contains("La transacción ha sido APROBADA."));
		
	}
	

}
