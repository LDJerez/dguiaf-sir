package test;

import javax.swing.JOptionPane;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.BUIPage;
import pages.BackOfficePage;
import pages.Login;
import pages.TramitesPage;
import pages.setConexion;

public class BUI  {

	protected WebDriver driver;
	protected  WebDriverWait wait;
	String usuario = "jbauchi";
	String paswd = "Ab123456";

//	@AfterClass
	public void CloseChrome() {
		//JOptionPane.showMessageDialog(null, "CERRAR NAVEGADOR");
		driver.quit();	

	}

	@BeforeClass
	public void IngresoAmbiente() {
		this.driver = setConexion.setup();
		wait = new WebDriverWait(driver, 10);
		TestBase TB = new TestBase(driver);
		TB.PortalBUI();
		Login lg = new Login(driver);
		lg.logearBUI(usuario, paswd);
	}

//	@AfterMethod
	public void IrHome() {	
		TestBase TB = new TestBase(driver);
		TB.PortalBUI();
	}

	public void espera(int espera) {
		try {Thread.sleep(espera);} catch (InterruptedException e) {e.printStackTrace();}
	}


	/*
	 *********************TEST*******************
	 */


	//@Test
	public void TS74_BUI_Generar_boleta_Registro_Civil_010250_MP_Todos() {
		
		String persona = "fisica";
		String doc = "dni";
		String cod = "01.02.50";
		String cant ="1";

		BUIPage BUI = new BUIPage(driver);
		BUI.SeleccionarDependencia("Registro Civil");
		BUI.CompletarDatos(persona,doc);
		BUI.AgregarConceptos(cod, cant);

		BUI.GenerarBUI();
		Assert.assertTrue(BUI.validBUI());		
	}


	//@Test(priority=14)
	public void TS46_01_BUI_AtributosDelItem_agregar() {
		/*
		 * 
		 * DEMO QUITARLE EL 1.60.37 A REGISTRO CIVIL
		 */
		
		BUIPage BUI = new BUIPage(driver);
		
		String dependencia = "Registro Civil";
		String item = "1.60.37";
		String vigencia = "35";
		Boolean valor_fijo = true;
		Boolean cantidad_fija = false;
		Boolean admite_reglas = false;		
		
		BUI.atributosDelItem_agregar(dependencia, item,vigencia,valor_fijo,cantidad_fija,admite_reglas);
		JOptionPane.showMessageDialog(null, "PAUSA");

	}	
	
	@Test()
	public void TS47_BUI_Generar_BuiForzada_en_tramite_y_pagar_en_tramite() {
		/*
		 * Generar automatizamente el -> nombreTramite, debe tener la opcion forzar bui
		 */
		TramitesPage TP = new TramitesPage(driver);
		TestBase TB = new TestBase(driver);
		String nroBoleta = "1802-00042162";		
		String tipo = "Documento Nacional de Identidad";
		String nroDoc= "29000000";//debe ser el mismo con el q se genero la bui
		String mail = "correo@prueba.com";
		String nombre = "Testing Automatizado";
		String direccion = "calle_prueba";
		String piso = "";
		String dpto = "";
		String obs = "TEST";
		String localidad = "CABA";
		String CP = "1234	";	
		String nombreTramite = "tramite_automatizacion_901conopcionForzarBUIparaTS47"; 
		/*
		 * 1-Generar una bui tratando de pagar un tramite con opcion forzarbui:true  puedo usar tramite_automatizacion_901conopcionForzarBUI
		 * 
		 * 
		 * ir a portaltramite, seleccionar tramite y al final tendre una bui de la que tengo que obtener el numero
		 * 
		 */	
		TP.ingresoAmbientePortalTramite();
		TP.SeleccionarTramite(nombreTramite);
		TP.DatosContribuyenteDiso2(nombre, tipo, nroDoc, mail, direccion, piso, dpto, obs, localidad, CP);
		Assert.assertTrue(TP.ValidarForzarBUI());	
		nroBoleta = TP.direcionPagoElectronicoParaObtenerNroBui().substring(5);
		/*
		 * TS75
		 */
		TB.PortalTramites();
		TP.menu("Paga tu Boleta");
		TP.DatosContribuyentePago(tipo, nroDoc, nroBoleta, mail);
		TP.SeleccionarFormaDepago("VISA");
		TP.btnRealizarPago();	
		TP.DatosTarjeta();
		Assert.assertTrue(TP.ValidarPagoVISA());
		
		
	}
	
	@Test
	public void TS73_Regresion_BUI_Generar_BUI_con_monto_mayor_a_9999999999() {
		BUIPage BUI = new BUIPage(driver);
		String cod = BUI.Generar_Concepto_RC_PE_VV();
		String persona = "fisica";
		String doc = "dni";
		String cant ="1";
		String valor = "9999999999";
		TestBase TB = new TestBase(driver);
		TB.PortalBUI();
		Login lg = new Login(driver);
		lg.logearBUI(usuario, paswd);
		BUI.SeleccionarDependencia("Registro Civil");
		BUI.CompletarDatos(persona,doc);
		BUI.AgregarConceptosValor(cod, cant, valor);
		BUI.GenerarBUI();
		Assert.assertTrue(BUI.validBUI());
	}
	
	@Test
	public void TS49_Pagar_BUI_por_trámite_en_el_que_se_eligio_BUI_como_medio_de_pago() {
		
		TramitesPage TP = new TramitesPage(driver);
		TP.CrearTramiteConRapipago();
	}
	
	@Test
	public void TS63_Regresion_BUI_Crear_BUI_por_forzar_bui_con_dependencia_3_digitos() {
		//FIXME
		/*
		 * PRECONDICION: Tener cliente asociado al tramite.
		 * 				Tener metodo de pago vinculado al tramite.
		 */
		TramitesPage TP = new TramitesPage(driver);
		/*
		TestBase TB = new TestBase(driver);
		Login lg = new Login(driver);
		String titulo = "tramite_automatizacion_TS63";
		String desc = "TS63";
		String msg = "Este es un Msg para el Usuario";
		String usuario = "jbauchi";
		String password = "Ab123456";
		String dependenciaBui = "(111) Planetario";
		String concepto ="79.1.20 Funciones especiales 20:00";
		
		
		TB.PortalTramitesABM();
		lg.logearTramiteAbm("jbauchi", "Ab123456");
		TP.seleccionarTarea("Tr\u00e1mites");
		TP.crearNuevoTramite(8000);
		
		espera(5000);
		
		TP.tramiteTituloYDescripcion(titulo, desc);
		TP.tramiteInfoAlUsuario(msg);
		TP.tramiteServPagoElectronico(usuario, password);
		TP.seleccionButton("SI","NO","SI"); // ULTIMO SI ES FORZAR BUI
		
		TP.seleccionarTramiteBui(dependenciaBui);
		espera(1000);

		TP.agregarNuevoGrupoConceptos(concepto);		
		TP.guardarTramite();
		espera(10000);
		*/
		
		String nombre = "Testing Automatizado TS63";
		String tipo = "Documento Nacional de Identidad";
		String nroDoc= "12345678";
		String mail = "correo@prueba.com";
		String direccion = "calle_prueba";
		String piso = "";
		String dpto = "";
		String obs = "TEST";
		String localidad = "CABA";
		String CP = "1234";	
		String nombreTramite = "tramite_automatizacion_TS63";
		TP.ingresoAmbientePortalTramite();
		TP.SeleccionarTramite(nombreTramite);
		espera(5000);
		TP.DatosContribuyenteDiso2(nombre, tipo, nroDoc, mail, direccion, piso, dpto, obs, localidad, CP);
		TP.clickPagoOnline();
		TP.SeleccionarFormaDepago("VISA");
		TP.btnRealizarPago();
		TP.DatosTarjeta();
		WebElement textoExito = driver.findElement(By.cssSelector(".texto"));
		Assert.assertTrue(textoExito.getText().contains("La transacción ha sido APROBADA."));
		
		
	}
	
}
