package test;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

public class TestBase {
final WebDriver driver;

	public TestBase(WebDriver driver){
		this.driver = driver;
        PageFactory.initElements(driver, this);
} 
	 String urlBackOffice_Sirt = "http://10.73.100.48";			
	 String urlBUIBoletaUnica_Sirt = "http://buisir-testing.buenosaires.gob.ar/WEB/Usuario/SignIn?ReturnUrl=%2fWEB";
	 String urlPortalTramitesAbm_Sirt = "http://10.73.100.48:2942/ABM/Index"; //fue modificado el 25/6, antes la url obligaba a logearse aun logeado
	 String urlPortalTramites_Sirt = "http://10.73.100.48:2942/";
	 String urlPortalPagoElectronico_Sirt = "http://10.73.100.48:2485/ABM";	 
	 String urlSirTools_Sirt="http://10.73.100.48:333/";
	 
	 
	 String urlBackOffice_Sirum = "http://10.73.100.49:1030/";
	 String urlPortalPagoElectronico_Sirum =  "http://10.73.100.49:1020/ABM";

	 
	//Portales	
public void PortalTramites() {
	driver.get(urlPortalTramites_Sirt);
}

public void PortalTramitesABM() {
	driver.get(urlPortalTramitesAbm_Sirt);
}

public void PortalPagos() {
	//driver.get(urlPortalPagoElectronico_Sirt);
	driver.get(urlPortalPagoElectronico_Sirt);
}

public void PortalBackOffice() {
	//driver.get(urlBackOffice_Sirum);
	driver.get(urlBackOffice_Sirt);
}

public void PortalBUI() {
	driver.get(urlBUIBoletaUnica_Sirt);
}

public void SirTools() {
	driver.get(urlSirTools_Sirt);
}

//*****************************************************************************************************//
	//Verificar Logins
public boolean validarLoginBO() {
	boolean a;
	String txt = driver.findElement(By.id("_base")).getText();
	System.out.println(txt);
	a = txt.equals("Bienvenido a SIR - Back Office | Seleccione alg�n �tem del men� para operar");
	return a;	
}
//***************************************************************************************************//



}
