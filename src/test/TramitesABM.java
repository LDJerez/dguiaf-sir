package test;

import static org.testng.Assert.assertTrue;

import javax.swing.JOptionPane;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.BasePage;
import pages.Login;
import pages.PagosPage;
import pages.TramitesPage;
import pages.setConexion;

public class TramitesABM {

	protected WebDriver driver;
	protected  WebDriverWait wait;
	String usuario = "jbauchi";
	String paswd = "Ab123456";

	public void espera(int espera) {
		try {Thread.sleep(espera);} catch (InterruptedException e) {e.printStackTrace();}
	}

	@AfterClass
	public void CloseChrome() {
		//JOptionPane.showMessageDialog(null, "CERRAR NAVEGADOR");
		driver.quit();	
	}

	@BeforeClass
	public void IngresoAmbiente() {
		this.driver = setConexion.setup();
		wait = new WebDriverWait(driver, 10);
		TestBase TB = new TestBase(driver);
		TB.PortalTramitesABM();
		Login lg = new Login(driver);
		lg.logearTramiteAbm(usuario, paswd);

	}

	@AfterMethod
	public void IrHome() {	
		TestBase TB = new TestBase(driver);
		TB.PortalTramitesABM();
	}

	/**********************TEST********************/


	//@Test
	public void TS23_Portal_tramiteAbm_Crear_tramite_no_forzar_bui(){//ESTA FIJO PARA EL CONCEPTO 1.3.2
		TramitesPage tramite = new TramitesPage(driver);
		String titulo_tramite = "tramite_automatizacion_001";
		String concepto ="1.3.2 concepto_automatizacion_01_03_02";


		tramite.seleccionarTarea("Tr\\u00e1mites");
		tramite.crearNuevoTramite(5000);

		espera(5000);

		tramite.completarInput("data-ng-model","nombre",titulo_tramite);		//PASAR UN LIST LA PROXIMA
		tramite.completarInput("data-ng-model","descripcion",titulo_tramite);
		tramite.completarInput("data-ng-model","usuario","jbauchi");
		tramite.completarInput("data-ng-model","password","Ab123456");
		tramite.seleccionButton("SI","SI","NO");
		espera(5000);
		tramite.agregarNuevoGrupoConceptos(concepto);

		tramite.guardarTramite();

		espera(10000);

		assertTrue(tramite.validarCreacionTramite());
	
	}	

	
	@Test(priority=13)
	public void TS24_Portal_TramiteAbm_Asociar_dependencia_a_tramite() {
		/*
		 * PRECONDICION: Generar concepto antes de este test.
		 */

		TramitesPage tramite = new TramitesPage(driver);
		String titulo = "tramite_automatizacion_002";
		String desc = "Es una prueba";
		String msg = "Este es un Msg para el Usuario";
		String usuario = "jbauchi";
		String password = "Ab123456";
		String dependenciaBui = "(2) Registro Civil";
		String concepto ="1.2.39 Informaciones Sumarias. Convivencia";
		
		tramite.seleccionarTarea("Tr\u00e1mites");
		tramite.crearNuevoTramite(8000);
		
		espera(5000);
		
		tramite.tramiteTituloYDescripcion(titulo, desc);
		tramite.tramiteInfoAlUsuario(msg);
		tramite.tramiteServPagoElectronico(usuario, password);
		tramite.modificarParametros("SI","SI","SI"); // ULTIMO SI ES FORZAR BUI
		
		tramite.seleccionarTramiteBui(dependenciaBui);
		espera(1000);

		tramite.agregarNuevoGrupoConceptos(concepto);		
		tramite.guardarTramite();
		espera(10000);
		
		assertTrue(tramite.validarCreacionTramite());
		
		

	}
	
	@Test()
	public void TS26_Portal_tramiteAbm_Crear_tramite_con_par_de_grupo_concepto_no_forzar_bui(){ //ESTA FIJO PARA LOS CONCEPTOS DADOS
		TramitesPage tramite = new TramitesPage(driver);
		
		String titulo = "tramite_automatizacion_003";
		String desc = "Es una prueba";
		String msg = "Este es un Msg para el Usuario";
		String usuario = "jbauchi";
		String password = "Ab123456";
		//Integer cantGruposConcepto = 3;
		//String concepto1 ="1.5.4 Disolución de Unión Civil";
		//String concepto2 ="1.2.33 Libreta de Familia - Original";


		tramite.seleccionarTarea("Trámites");
		tramite.crearNuevoTramite(5000);

		espera(25000);  //FIXME ACA A VECES FALLA - SERA POR TIEMPO WAIT?

		tramite.tramiteTituloYDescripcion(titulo, desc);
		tramite.tramiteInfoAlUsuario(msg);
		tramite.tramiteServPagoElectronico(usuario, password);
		tramite.seleccionButton("SI","SI","NO");
		espera(6000);
		
		tramite.agregarNuevoGrupoConceptosVarios();

		tramite.guardarTramite();

		espera(10000);

		assertTrue(tramite.validarCreacionTramite());
		
		tramite.btnAceptar();
	
	}	
	
	@Test(priority=16)
	public void TS27_Portal_tramiteAbm_Crear_rubro(){
		TramitesPage tramite = new TramitesPage(driver);
		
		tramite.seleccionarTarea("Rubros");
		tramite.crearNuevoRubro(5000); //fallo aca
		tramite.descNuevoRubro("Test Automat");
		espera(2000);
		
		tramite.guardarNuevoRubro();
		espera(5000);
		
		assertTrue(tramite.validarCreacionRubro());
	}
	
	@Test(priority=17)
	public void TS28_Portal_tramiteAbm_Crear_pagina(){
		TramitesPage tramite = new TramitesPage(driver);
		
		tramite.seleccionarTarea("Páginas");
		
		tramite.crearNuevaPagina(5000);
		
		espera(5000);
		tramite.descNuevaPagina("Test Automat");
		tramite.HTMLNuevaPagina("<p></p><h4>Tiempos de acreditación de acuerdo al medio de pago seleccionado:\"\r\n");
		
		espera(2000);
		tramite.guardarNuevaPagina();
		
		assertTrue(tramite.validarCreacionPagina());
		
		espera(2000);
		
	}
	
	@Test(priority=18)
	public void TS29_Portal_tramiteAbm_Editar_tramite_datos_particulares(){
		TramitesPage tramite = new TramitesPage(driver);
		TestBase TB = new TestBase(driver);
		Login lg = new Login(driver);
		
		TB.PortalTramitesABM();
		lg.logearTramiteAbm("jbauchi", "Ab123456");
		tramite.seleccionarTarea("Trámites");
		espera(2000);
		tramite.pbuscaEnGrillaNgTextoYejecutaAccion("tramite_automatizacion_003", "Tramite", "editar", 6, 100);
		espera(15000);
		tramite.btnDatoParticular();
		espera(2000);
		tramite.descDatoParticular("TestAutomat");
		tramite.selectDatoPart();
		tramite.inputValoresDatosParticulares("1", "100");
		espera(2000);
		
		tramite.guardarTramite();
		espera(5000);
		
		assertTrue(tramite.validarCreacionTramite());
		espera(1000);
		tramite.btnAceptar();

		// ACA CONTINUA EN OTRO AMBIENTE
		
		espera(1500);
		TB.PortalTramites();
		espera(1500);
		//lg.logearTramiteAbm("jbauchi", "Ab123456"); FIXME ESTE SITIO NUNCA TIENE LOGIN O SOLO EN AMBIENTE DE TEST?
		String nombre = "Testing Automatizado";
		String tipo = "Documento Nacional de Identidad";
		String nroDoc= "1000000";
		String mail = "correo@prueba.com";
		String direccion = "calle_prueba";
		String piso = "";
		String dpto = "";
		String obs = "TEST";
		String localidad = "CABA";
		String CP = "1234	";
		String inputDatoParticular = "10";
		
		espera(2000);
		tramite.SeleccionarTramite("tramite_automatizacion_003");
		espera(8000);
		tramite.DatosContribuyenteDisoConDatoParticular(nombre, tipo, nroDoc, mail, direccion, piso, dpto, obs, localidad, CP, inputDatoParticular);
		espera(2000);

		/*FIXME El TC no especifica donde termina. Hasta aca llega a la generacion del Pago online.
		 *	--------------------------------- NO FUNCIONA CON FORZAR BUI (CAMBIAN LAS PANTALLAS)-----------------------------------------------------------------
		 */
	}
/* 		------------------------- 	ESTE TEST ES PARA CORRER LA PRECONDICION DEL TS29 (PRECON-TS26) -------------------------
	@Test(priority = 1)
	public void TestTS29_con_PrecondTS26() {
		TS26_Portal_tramiteAbm_Crear_tramite_con_par_de_grupo_concepto_no_forzar_bui();
		espera(2500);
		TS29_Portal_tramiteAbm_Editar_tramite_datos_particulares();
	}
*/
	@Test
	public void TS25_Portal_tramiteAbm_Habilitar_forzar_BUI() {
		TramitesPage tramite = new TramitesPage(driver);
		String nombre = "Testing Automatizado";
		String tipo = "Documento Nacional de Identidad";
		String nroDoc= "1000000";
		String mail = "correo@prueba.com";
		String direccion = "calle_prueba";
		String piso = "";
		String dpto = "";
		String obs = "TEST";
		String localidad = "CABA";
		String CP = "1234	";
		String dependenciaBui = "(2) Registro Civil";
		String inputDatoParticular = "10";
		
		tramite.seleccionarTarea("Tr\u00e1mites");
		espera(5000);
		tramite.pbuscaEnGrillaNgTextoYejecutaAccion("tramite_automatizacion_003", "Tramite", "editar", 6, 100);
		espera(5000);
		tramite.modificarParametros("SI","NO","SI");
		tramite.seleccionarTramiteBui(dependenciaBui);
		tramite.guardarTramite();
		espera(10000);
		assertTrue(tramite.validarCreacionTramite());
		
		tramite.ingresoAmbientePortalTramite();
		tramite.SeleccionarTramite("tramite_automatizacion_003");		
		

		espera(5000);
		tramite.DatosContribuyenteDisoConDatoParticular(nombre, tipo, nroDoc, mail, direccion, piso, dpto, obs, localidad, CP, inputDatoParticular);
		espera(5000);
		assertTrue(tramite.ValidarForzarBUI());
		
	}
	
	//@Test
	public void TS30_Portal_tramiteAbm_Editar_tramite_parametro_tramite() {
		TramitesPage tramite = new TramitesPage(driver);
		
		tramite.seleccionarTarea("Tr\u00e1mites");
		espera(5000);
		tramite.pbuscaEnGrillaNgTextoYejecutaAccion("tramite_automatizacion_003", "Tramite", "editar", 6, 100);
		espera(5000);
		tramite.modificarParametros("SI","NO","NO"); 
		tramite.guardarTramite();
		espera(10000);
		assertTrue(tramite.validarCreacionTramite());
		
	}
	

	//@Test(priority=13)
	public void TS31_Portal_tramiteAbm_Deshabilitar_tramite() {
		
		/*
		 * 
		 * DEMO VERIFICAR QUE EL tramite_automatizacion_001 ESTE ACTIVO
		 * 
		 */
		TramitesPage tramite = new TramitesPage(driver);
		String titulo_tramite = "tramite_automatizacion_001";
				
		tramite.deshabilitarTramite(titulo_tramite);
		
		
	}
	
	
	@Test
	public void TS32_Portal_tramiteAbm_Editar_tramite_ServicioPagoElectronico(){
		/*
		 * POSEER UN TRAMITE QUE PUEDA MODIFICAR
		 * 
		 */
		TramitesPage tramite = new TramitesPage(driver);
		BasePage bp = new BasePage();
		String nombreTramite = "tramite_automatizacion_003";
		String usuario ="usuario_modificado";
		String password ="password_modificado";

		tramite.seleccionarTarea("Trámites");

		bp.base__esperaDesaparezcaPantallaBloqueo(driver, 20, 1000);
		tramite.pbuscaEnGrillaNgTextoYejecutaAccion(nombreTramite, "Tramite", "editar", 6, 1000);
		
		bp.base__esperaDesaparezcaPantallaBloqueo(driver, 20, 1000);
		tramite.tramiteServPagoElectronico(usuario, password);
		
		tramite.guardarTramite();
		
		IrHome();
		tramite.seleccionarTarea("Trámites");
		bp.base__esperaDesaparezcaPantallaBloqueo(driver, 20, 1000);
				
		String textoquebusco = tramite.pbuscaEnGrillaNgTextoyDevuelveTextoMismaFilaEnColumnaX(nombreTramite, "Tramite", "5", 6, 5000);
		Assert.assertTrue( textoquebusco.equals(usuario));
		textoquebusco = tramite.pbuscaEnGrillaNgTextoyDevuelveTextoMismaFilaEnColumnaX(nombreTramite, "Tramite", "6", 6, 2000);
		Assert.assertTrue( textoquebusco.equals(password));
	}
	
	@Test
	public void TS33_Portal_tramiteAbm_Editar_tramite_ConceptosVarios_cantidadVarias(){
		/*
		 * El CONCEPTO1 y 2 DEBE ESTAR CONFIGURADOS CON CANTIDAD VARIABLE
		 * EL nombreTramite DEBE ESTAR creado en PORTAL PAGOS ABM, 
		 */
		
		
		
		
		// genero las precondiciones
		//-----------crearConceptoAprobarloAsignarloADependencia <---para el concepto1
		//-----------BUI_AtributosDelItem		
		//---------- repetir para el concepto2
		//----------  logearme_sleeccionarmenu_ PagosAbm_Crear_Tramite_  con 2 conceptos
		//----------  logearme_sleeccionarmenu_ Portal_tramiteAbm_Crear_tramite_
		//-----------   logearme_sleeccionarmenu_  PagosAbm_Asociar_cliente_a_tramite
		
		
		
		/*
		 * para demo, usar los siguientes datos hardcodeados.
		 */
		TramitesPage tramite = new TramitesPage(driver);
		BasePage bp = new BasePage();
		String nombreTramite = "tramite_automatizacion_72_parats33";		
		String concepto1 = "1.60.36 concepto_automatizacion_1_60_36";		
		String concepto2 = "1.59.70 concepto_automatizacion_1_59_70";
	
		/*
		 * VOY A TRAMITE ABM Y EDITO UN TRAMITE. ese tramite debe estar creado en portal pagos ABM
		 */
		
		tramite.seleccionarTarea("Trámites");
		tramite.pbuscaEnGrillaNgTextoYejecutaAccion(nombreTramite, "Tramite", "editar", 6, 3000);
		espera(10000);
		bp.base__esperaDesaparezcaPantallaBloqueo(driver, 60, 1000);//aparece y desaparece varias veces. Por eso antes le pongo una demora grande
		tramite.borrarGruposConceptos();
		tramite.agregarNuevoGrupoConceptos(concepto1,concepto2);
		
		tramite.guardarTramite();
		
		/*
		 * SEGUNDA PARTE Ingresar a Portal de Trámites.
						 Elegir nuestro trámite recién creado.
						 Asignar un valor >1 como cantidad en alguno de los conceptos.
						 Continuar con el trámite hasta llegar a la BUI o  a la pantalla de Métodos de Pago según corresponda
		 */		
		
		
		tramite.ingresoAmbientePortalTramite();
		tramite.SeleccionarTramite(nombreTramite);		
		
		String nombre = "Testing Automatizado";
		String tipo = "Documento Nacional de Identidad";
		String nroDoc= "1000000";
		String mail = "correo@prueba.com";
		String direccion = "calle_prueba";
		String piso = "";
		String dpto = "";
		String obs = "TEST";
		String localidad = "CABA";
		String CP = "1234	";			
		tramite.DatosContribuyenteParaTS33(nombre, tipo, nroDoc, mail, direccion, piso, dpto, obs, localidad, CP); 
		
		tramite.validarPaginaSeleccioneMedioPago();

	}
	
	
	@Test
	public void TS34_Portal_tramiteAbm_Editar_tramite_GruposVarios_ConceptosVarios_cantidadVarias(){
		/*
		 * El CONCEPTO1 y 2 DEBE ESTAR CONFIGURADOS CON CANTIDAD VARIABLE
		 * EL nombreTramite DEBE ESTAR creado en PORTAL PAGOS ABM, 
		 */
		/*
		 * para demo, usar los siguientes datos hardcodeados.
		 */
		TramitesPage tramite = new TramitesPage(driver);
		BasePage bp = new BasePage();
		String nombreTramite = "tramite_automatizacion_72_parats33";		
		String concepto1 = "1.60.36 concepto_automatizacion_1_60_36";		
		String concepto2 = "1.59.70 concepto_automatizacion_1_59_70";
	
		/*
		 * VOY A TRAMITE ABM Y EDITO UN TRAMITE. ese tramite debe estar creado en portal pagos ABM
		 */
		
		tramite.seleccionarTarea("Trámites");
		tramite.pbuscaEnGrillaNgTextoYejecutaAccion(nombreTramite, "Tramite", "editar", 6, 3000);
		espera(10000);
		bp.base__esperaDesaparezcaPantallaBloqueo(driver, 60, 1000);//aparece y desaparece varias veces. Por eso antes le pongo una demora grande
		tramite.borrarGruposConceptos();
		
		tramite.agregarConceptosEnGruposDistintos(concepto1,concepto2);
			
		
		tramite.guardarTramite();
		
		/*
		 * SEGUNDA PARTE Ingresar a Portal de Trámites.
						 Elegir nuestro trámite recién creado.
						 Asignar un valor >1 como cantidad en alguno de los conceptos.
						 Continuar con el trámite hasta llegar a la BUI o  a la pantalla de Métodos de Pago según corresponda
		 */		
		
		
		tramite.ingresoAmbientePortalTramite();
		tramite.SeleccionarTramite(nombreTramite);		
		
		String nombre = "Testing Automatizado";
		String tipo = "Documento Nacional de Identidad";
		String nroDoc= "1000000";
		String mail = "correo@prueba.com";
		String direccion = "calle_prueba";
		String piso = "";
		String dpto = "";
		String obs = "TEST";
		String localidad = "CABA";
		String CP = "1234	";			
		tramite.DatosContribuyenteParaTS34(nombre, tipo, nroDoc, mail, direccion, piso, dpto, obs, localidad, CP); 
		
		tramite.validarPaginaSeleccioneMedioPago();
	
	}
}
