package test;

import static org.testng.Assert.assertTrue;

import java.util.List;

import javax.swing.JOptionPane;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import pages.BUIPage;
import pages.BackOfficePage;
import pages.BasePage;
import pages.Login;
import pages.PagosPage;
import pages.TramitesPage;
import pages.setConexion;

public class LimpiarAmbiente  extends BasePage{

	protected WebDriver driver;
	protected  WebDriverWait wait;
	String usuario = "jbauchi";
	String paswd = "Ab123456";	

	public void espera(long milisec) {
		try {Thread.sleep(milisec);} catch (InterruptedException e) {e.printStackTrace();}		
	}

	public void WaitForElement(By element, int tiempo) {
		WebDriverWait wait;
		wait = new WebDriverWait(driver, tiempo);
		wait.until(ExpectedConditions.elementToBeClickable(element));
	}

	@AfterMethod
	public void CloseChrome() {
		JOptionPane.showMessageDialog(null, "PAUSA");

		driver.quit();	
	}


	@BeforeMethod
	public void Init() throws Exception	{
		this.driver = setConexion.setup();
		wait = new WebDriverWait(driver, 10);
	}	


	/*
	 ************************LIMPIAR******************************
	 */
	
	//-------------------PORTAL PAGO ELECTRONICO ABM-----------

	@Test(priority = 2)
	public void eliminarTramiteEnPagos() {				
		PagosPage PagosPage = new PagosPage(driver);
		String titulo_tramite = "tramite_automatizacion_005f";

		PagosPage.IngresoAmbiente();

		//selecciono la opcion TRAMITE ABM		
		PagosPage.seleccionarTarea(".btn.btn-default","ABM Tr�mites");

		base__esperaDesaparezcaPantallaBloqueo(driver, 60, 1000);
		PagosPage.deshabilitarTramite(titulo_tramite);
	}


	@Test(priority = 3)
	public void eliminarClienteEnPagos() {	

		PagosPage PagosPage = new PagosPage(driver);
		String cliente="jbauchi";

		PagosPage.IngresoAmbiente();
		PagosPage.eliminarClienteTramite(cliente);

	}


	
	
	
	@Test
	public void PagosAbm_Restaurar_tiempo_de_acreditacion() {
		
		PagosPage PagosPage = new PagosPage(driver);
		PagosPage.IngresoAmbiente();
		PagosPage.seleccionarTarea(".btn.btn-default", "ABM de Tiempos de Acreditación");
		espera(5000);
		PagosPage.Texto_HTML_TAcred_Original();	
	
	}
	
	@Test
	public void PagosAbm_Cambiar_a_logo_original() {	//				ARTURO
		PagosPage PagosPage = new PagosPage(driver);	
		PagosPage.IngresoAmbiente();
		
		espera(2000);
		PagosPage.seleccionarTarea(".btn.btn-default", "ABM Logos Medios de Pago");									
		espera(2000);
		PagosPage.arturo__buscaEnGrillaNgTextoYejecutaAccion("BUI", "Medio de Pago", "cambiar", 2); //Medio de pago (AMEX, BUI, VISA, ETC), Titulo, Opcion a ejecutar (cambiar o editar), cantidad de columnas.
		espera(2000);
		PagosPage.ElegirLogoOriginal();
		
	}
	
	//-------------------PORTAL TRAMITE ABM-----------

	@Test(priority = 4)
	public void eliminarTramiteEnTramite() {			

		TramitesPage TramitePage = new TramitesPage(driver);
		String titulo_tramite = "tramite_automatizacion_001";

		TramitePage.ingresoAmbientePortalTramiteAbm();
		TramitePage.deshabilitarTramite(titulo_tramite);		
	}


	//-------------------PORTAL BACKOFFICE-----------

	@Test(priority = 1)
	public void borrarUnConcepto() {	

		BackOfficePage BO = new BackOfficePage(driver);
		String concepto = "01.";

		BO.IngresoAmbiente();
		espera(3000);
		BO.eliminarConcepto(concepto);		
	}

	@Test(priority = 1)
	public void borrarMuchosConceptos() {	

		BackOfficePage BO = new BackOfficePage(driver);
		String codigo = "01.";

		BO.IngresoAmbiente();
		espera(3000);

		//entro al menu CONCEPTOS
		BO.menu("administra", "Conceptos");

		//completa datos de BUSQUEDA
		WaitForElement(By.id("Codigo"), 60);
		BO.buscarConcepto("", "automa","","","",false, false,true,false,"");

		//click en BUSCAR
		espera(1000);
		BO.ConceptoBtns("buscar");

		espera(3000);
		//	List<WebElement> listado = driver.findElements(By.cssSelector(".t-no-data"));//mejorar la logica

		for(int i=0;i<10;i++) {
			if(true) {
				//selecciono ELIMINAR concepto  
				//WaitForElement(By.xpath("//a[contains(.,'Eliminar')]"), 60);
				driver.findElement(By.xpath("//a[contains(.,'Eliminar')]")).click();
				System.out.println("Se elimino el concepto: "+codigo);
			}
			else
				System.out.println("No hay concepto para eliminar: "+codigo);
			espera(1000);
		}
		espera(5000);
	}


	
	/*
	 ************************CREAR******************************
	 */
	
	@Test
	public void crearConceptoAprobarloAsignarloADependencia_AgregarAtributoItem_generaTramiteEnPortalPagosAbm__generaTramiteEnPortalTramite_asociaClienteTramite() {
		/* 
		 * TS03_BackOffice_CrearUnConcepto()
		 * TS03_01_BackOffice_AprobarConcepto()
		 * TS03_02_BackOffice_AsignarDependencia_Concepto()
		 */

		BackOfficePage BO = new BackOfficePage(driver);
		String periodo_tarifaria = "2018";
		String fecha_publicacion = base__fechaActual();
		String Cod1 = "1";
		String Cod2 = BasePage.random(50,60);
		String Cod3 = BasePage.random(1,99);
		String Descripcion = "concepto_automatizacion_" + Cod1 + "_" + Cod2 + "_" + Cod3; 
		String Renglon1 = "renglon1_concepto_automatizacion_" + Cod1 + "_" + Cod2 + "_" + Cod3;
		String Agencia_asignada = "01-Dir. Gral del Registro Civil y Capacidad de las Personas";
		String valor = "10";
		boolean es_redondeable = true;
		boolean tiene_precio_fijo = true;
		boolean cantidad_variable = true;
		boolean es_devolucion = true;
		String codigo = Cod1+"."+Cod2+"."+Cod3;


		BO.IngresoAmbiente();		
		BO.crearConceptoAprobarlo(periodo_tarifaria, fecha_publicacion, Cod1, Cod2, Cod3, Descripcion, Renglon1, Agencia_asignada, valor, es_redondeable, tiene_precio_fijo, cantidad_variable, es_devolucion,codigo);


		String dep1 = "Registro Civil";
		String dep2 = "Pago Electrónico";

		BO.asignarDependencia_Concepto(codigo, dep1, dep2);
		Assert.assertTrue(BO.asignacionDependenciaExitosa());

		
		
		
		
		
		/*
		 * TS46_01_BUI_AtributosDelItem_agregar()
		 * 
		 */		

		BUIPage BUI = new BUIPage(driver);	

		BUI.IngresoAmbiente();
		BUI.atributosDelItem_agregar(dep1, codigo, "30", tiene_precio_fijo, !cantidad_variable, false);


		
		
		

		/*
		 * TS10_PagosAbm_Crear_Tramite_Registro_Civil_010302_Mp_Todos()
		 * 
		 */

		PagosPage PagosPage = new PagosPage(driver);
		String titulo_tramite = "tramite_automatizacion_" + random(1, 1000);//EL NOMBRE DEL TRAMITE SI PUEDE REPETIRSE
		String concepto_y_descripcion = base__formateaConceptoSinCero(codigo)+" "+Descripcion; //concept(formateadoSinCeros(concepto))

		PagosPage.IngresoAmbiente();
		PagosPage.crearTramite(titulo_tramite,true,dep1,concepto_y_descripcion,true, false,"VISA","","","");
		assertTrue(PagosPage.validarCreacionTramite("El trámite se guardó correctamente."));


		/*
		 * TS23_Portal_tramiteAbm_Crear_tramite_no_forzar_bui()
		 * 
		 */

		TramitesPage tramite = new TramitesPage(driver);

		String concepto = codigo +" "+ Descripcion;

		tramite.ingresoAmbientePortalTramiteAbm();
		tramite.seleccionarTarea("Trámites");
		tramite.crearNuevoTramite(5000);

		/*TODO todo esto deberia estar en un metodo COMPLETAR EDICION TRAMITE*/
		espera(5000);

		tramite.completarInput("data-ng-model","nombre",titulo_tramite);		//PASAR UN LIST LA PROXIMA
		tramite.completarInput("data-ng-model","descripcion",titulo_tramite);
		tramite.completarInput("data-ng-model","usuario","jbauchi");
		tramite.completarInput("data-ng-model","password","Ab123456");
		tramite.seleccionButton("SI","SI","NO");
		espera(5000);
		tramite.agregarNuevoGrupoConceptos(concepto);
		tramite.guardarTramite();
		espera(10000);
		assertTrue(tramite.validarCreacionTramite());



		/*
		 * TS20_PagosAbm_Asociar_cliente_a_tramite()
		 */
		PagosPage.asociarClienteATramite("jbauchi - (Juan.Bauchi)", titulo_tramite);//ver antes que el cliente NO tenga asignado ningun tramite
		
		//------------------------PORTAL TRAMITE pagar tramite

	}
		
}



