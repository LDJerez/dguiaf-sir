package test;

import static org.testng.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.BackOfficePage;
import pages.BasePage;
import pages.Login;
import pages.PagosPage;
import pages.TramitesPage;
import pages.setConexion;

public class PagosABM extends BasePage {

	protected WebDriver driver;
	protected  WebDriverWait wait;
	String usuario = "jbauchi";
	String paswd = "Ab123456";
	
	public void espera(long milisec) {
		try {Thread.sleep(milisec);} catch (InterruptedException e) {e.printStackTrace();}		
	}
	
	
	@AfterClass
	public void CloseChrome() {
		//JOptionPane.showMessageDialog(null, "CERRAR NAVEGADOR");
		driver.quit();
	}
	 	
	
	@BeforeClass
	public void IngresoAmbiente() {
		this.driver = setConexion.setup();
		wait = new WebDriverWait(driver, 10);
		TestBase TB = new TestBase(driver);
		TB.PortalPagos();
		Login lg = new Login(driver);
		lg.logearPagosAbm(usuario, paswd);
		}
	
	
	@AfterMethod
	public void IrHome() {	
	TestBase TB = new TestBase(driver);
	TB.PortalPagos();
	}
	
	/*
	 *********************TEST*******************
	 */
	
	
	//@Test
	public void TS10_PagosAbm_Crear_Tramite_Registro_Civil_010302_Mp_Todos()  {
		PagosPage PagosPage = new PagosPage(driver);
		BackOfficePage BO = new BackOfficePage(driver);
		String titulo_tramite = "tramite_automatizacion_" + random(1, 1000);//EL NOMBRE DEL TRAMITE SI PUEDE REPETIRSE
		boolean pagablePorPortal = true;
		String dependencia = "2 - Registro Civil";
		
		String concepto ="1.3.20 concepto_automatizacion_01_03_20"; 	
		boolean decidir = true;
		boolean notificacion = false;
		String medio_pago1 = "VISA";
		String medio_pago2 = "INTERBANKING";
		String medio_pago3 = "RAPIPAGO";
		String medio_pago4 = "REDLINK";		
		
		/*
		 * SE ESTA USANDO UN CONCEPTO FIJO CREADO, APROBADO Y ASOCIADO A LA DEPENDENCIA.
		 * SINO LLAMAR A 
		 * BO.crearConcepto
		 * BO.aprobarConcepto
		 * BO.asignarConceptoAdependencia
		 */
		
				
		PagosPage.crearTramite(titulo_tramite,pagablePorPortal,dependencia,concepto,decidir, notificacion,medio_pago1,medio_pago2,medio_pago3,medio_pago4);
		
		assertTrue(PagosPage.validarCreacionTramite("El trámite se guardó correctamente."));
		}
	
	
	//@Test	
	public void TS11_PagosAbm_Desmarcar_Pagable_Por_Portal() {
		/*
		 * SE ESTA USANDO TRAMITE FIJO CREADO,
		 * SINO LLAMAR A 
		 * PagosPage.crearTramite()
		 */	
		
		PagosPage PagosPage = new PagosPage(driver);
		String titulo_tramite = "tramite_automatizacion_005g";
	
		
		PagosPage.desmarcarPagablePorPortal(titulo_tramite);
	
		assertTrue(PagosPage.validarCreacionTramite("El trámite se guardó correctamente."));	
	}
		

	//@Test(priority=5)	
	public void TS12_PagosAbm_Tramites_Filtrar_conceptos_por_dependencia() {
		/*
		 * VALORES DE PARAMETROS FIJOS, SIEMPRE ESTAN EN LA BASE DEFAULT, 
		 * NO no deberian modificarse. SINO HABRIA QUE CREAR DOS CONCEPTOS 
		 * NUEVOS Y ASOCIAR A DOS DEPENDENCIAS DISTINTAS
		 * 
		 * DEMO: INDEPENDIENTE
		 */
		
		PagosPage PagosPage = new PagosPage(driver);		

		String dependencia1 ="Registro Civil ";		
		String concepto1 = "1.5.4";
	
		String dependencia2 ="Dirección Gral de Tesorer";
		String concepto2 = "49.1.1";
		
		String conceptoDefault = "-- Elija un concepto --";

		PagosPage.seleccionarTarea(".btn.btn-default","ABM Trámites");
		
		base__esperaDesaparezcaPantallaBloqueo(driver,90,1000);		
		PagosPage.crearNuevoTramite_click();
		
		base__esperaDesaparezcaPantallaBloqueo(driver,90,1000);	
		PagosPage.dependenciaAsociada(dependencia1);
		
		PagosPage.seleccionarConceptoDelTramite(concepto1);
		
		PagosPage.dependenciaAsociada(dependencia2);

		espera(2000);
		if(PagosPage.getValorSeleccionadoDropConceptos().equals(conceptoDefault))
			assertTrue(true);
		else
			assertTrue(false);
		
		//selecciono concepto de dependencia 2 y lo agrego
		PagosPage.seleccionarConceptoDelTramite(concepto2);
		PagosPage.agregarConceptoDelTramite();
		
		//vuelvo a seleccionar la dependencia1	
		PagosPage.dependenciaAsociada(dependencia1);		
		
		assertTrue(PagosPage.validarModificacionDependencia("Estos conceptos no pertenecen a la dependencia seleccionada. Al Aceptar los mismos ser�n eliminados"));		
	
			
	}
	
	
	//@Test(priority=6)
	public void TS13_PagosAbm_Cambiar_medios_de_pago_tramite() {
		/*
		 * DEBEMOS TENER 1 TRAMITE QUE TENGA 1 MEDIO DE PAGO (!= RAPIPAGO) 
		 * SINO PagosPage.crearTramite()
		 * 
		 * DEMO: INDEPENDIENTE   USA FIJO EL 140 QUE TIENE SETEADO VISA
		 */
		PagosPage PagosPage = new PagosPage(driver);		
		
		String titulo_tramite = "tramite_automatizacion_140";
		String medio_pago1 = "RAPIPAGO";
		
		PagosPage.seleccionarTarea(".btn.btn-default","ABM Trámites");

		base__esperaDesaparezcaPantallaBloqueo(driver,90,1000);	
		PagosPage.editarTramite_click(titulo_tramite);		
		
		base__esperaDesaparezcaPantallaBloqueo(driver,90,1000);	
		
		PagosPage.eliminarMediosPago();
		PagosPage.seleccionarMediosPago(medio_pago1);		
	
		PagosPage.guardarTramite_click();
		assertTrue(PagosPage.validarCreacionTramite("El trámite se guardó correctamente."));		
	}
	
	
	//@Test(priority=7)
	public void TS14_PagosAbm_Dejar_tramite_sin_dependencia_asociada() {
		/* CREA UN NUEVO TRAMITE PERO NO COMPLETA LA DEPENDENCIA
		 * 
		 * 
		 * PUEDO USAR SIEMPRE EL MISMO NOMBRE DE TRAMITE XQ NO VALIDA
		 * EL CONCEPTO DEBE ESTAR APROBADO Y ASOCIADO A REGISTRO CIVIL.
		 * 
		 * DEMO: INDEPENDIENTE
		 */
		PagosPage PagosPage = new PagosPage(driver);
		
		String titulo_tramite = "tramite_automatizacion_sin_dependencia_001";
		boolean pagablePorPortal = true;
	
		String dependencia = "-- Sin dependencia asociada --";
		String concepto ="1.3.20"; 
		boolean decidir = true;
		boolean notificacion = false;
		String medio_pago1 = "VISA";
		String medio_pago2 = "";
		String medio_pago3 = "";
		String medio_pago4 = "";
		
		PagosPage.seleccionarTarea(".btn.btn-default","ABM Trámites");

		base__esperaDesaparezcaPantallaBloqueo(driver,90,1000);		
		PagosPage.crearNuevoTramite_click();
		
		base__esperaDesaparezcaPantallaBloqueo(driver,90,1000);		
		PagosPage.completarEdicionTramite(titulo_tramite,pagablePorPortal,dependencia,concepto,decidir, notificacion,medio_pago1,medio_pago2,medio_pago3,medio_pago4);
	
		PagosPage.guardarTramite_click();
		assertTrue(PagosPage.validarCreacionTramite("El trámite se guardó correctamente."));
		}
	
	
	//@Test(priority=8)
	public void TS15_PagosAbm_Tramite_Cambiar_conceptos_de_un_tramite() {
		/* 
		 *  TODO DEBO TENER UN TRAMITE Y UN CONCEPTO CREADO. 
		 * LimpiarAmbiente.crearConceptoAprobarloAsignarloADependenciaAgregarAtributoItemYgeneraTramiteEnPortalPagos()
		 *
		 *	Para la DEMO borrarle el concepto 1.2.4 si es que esta asignado
		 */
					
		PagosPage PagosPage = new PagosPage(driver);
		String titulo_tramite = "tramite_automatizacion_524";
		String concepto = "1.02.4";
		
		PagosPage.seleccionarTarea(".btn.btn-default","ABM Trámites");
		
		base__esperaDesaparezcaPantallaBloqueo(driver,90,1000);	
		PagosPage.editarTramite_click(titulo_tramite);		
		
		base__esperaDesaparezcaPantallaBloqueo(driver,90,1000);			
		PagosPage.seleccionarConceptoDelTramite(concepto);
		PagosPage.agregarConceptoDelTramite();
		
		PagosPage.guardarTramite_click();
		
		PagosPage.validarCreacionTramite("El trámite se guardó correctamente.");
		
		//TODO falta la verificacion de ir a Portal Tramite y ver si figura el concepto agregado
		
	}
	
	
	//@Test
	public void TS16_PagosAbm_DeshabilitarTramite() {
		PagosPage PagosPage = new PagosPage(driver);
		String titulo_tramite = "tramite_automatizado_005";
			
		PagosPage.seleccionarTarea(".btn.btn-default","ABM Trámites");
		
		base__esperaDesaparezcaPantallaBloqueo(driver, 60, 1000);
		PagosPage.deshabilitarTramite(titulo_tramite);
		
		//es necesario que termine de REDIRECCIONAR para finalizar la eliminacion
		espera(3000);
		
		//TODO assert que busque en la grilla de inactivas para ver si esta
	}
	
	
	//@Test
	public void TS17_PagosAbm_RehabilitarTramite() {
		PagosPage PagosPage = new PagosPage(driver);
		String tramite = "tramite_automatizado_005";
		
		PagosPage.seleccionarTarea(".btn.btn-default","ABM Trámites");
		
		base__esperaDesaparezcaPantallaBloqueo(driver, 60, 1000);		
		PagosPage.rehabilitarTramite(tramite);	
		
		//TODO assert() hacer que busque en la grilla de ACTIVOS a ver si lo encuentra
	}
	
	
	//@Test(priority=11)
	public void TS20_PagosAbm_Asociar_cliente_a_tramite() {	
		/*
		 * 
		 * 
		 * PARA LA DEMO BORRAR A JBAUCHI DE CUALQUIER TRAMITE RELACIONADO, Se puede hacer con el TS22 aca abajo
		 */
		PagosPage PagosPage = new PagosPage(driver);
		String titulo_tramite = "tramite_automatizacion_217";
		String cliente = "jbauchi - (Juan.Bauchi)";
				
		PagosPage.seleccionarTarea(".btn.btn-default","ABM Clientes");
		
		base__esperaDesaparezcaPantallaBloqueo(driver, 60, 1000);
		PagosPage.crearNuevoCliente_click();
		
		base__esperaDesaparezcaPantallaBloqueo(driver, 60, 1000);
		PagosPage.completarEdicionCliente(cliente,titulo_tramite);
		
		PagosPage.guardarCliente_click();
	
		assertTrue(PagosPage.validarEdicionCliente("El cliente se guardó correctamente."));	//FIXME  puede dar El cliente seleccionado ya posee un tr�mite asociado. �Desea modificarlo
	}
	
	@Test(priority=11)
	public void TS21_PagosAbm_Editar_Tramite_de_Cliente() {
		/*
		 * PRECONDICIONES: Generar Cliente y Tramite. (TS__)
		 */
		PagosPage PagosPage = new PagosPage(driver);
		
		PagosPage.seleccionarTarea(".btn.btn-default", "ABM Clientes");
		espera(5000);
		
		PagosPage.base__buscaEnGrillaNgTextoYejecutaAccion(driver, "purgentes", "Cliente", "editar", 2);
		espera(8000);
		
		PagosPage.elegirVerificarNombreTramite();
		espera(5000);
		
		PagosPage.guardarCliente_click();
		
		assertTrue(PagosPage.validarEdicionCliente("El cliente se guardó correctamente."));	//FIXME  puede dar El cliente seleccionado ya posee un tramite asociado. �Desea modificarlo
		
	}
	
	
	//@Test(priority=12)
	public void TS22_PagosAbm_Eliminar_cliente_de_tramite() {	
		/*
		 * DEMO: INDEPENDIENTE BORRA EL QUE CREO EN EL TEST ANTERIOR TS20
		 */
		
		
		PagosPage PagosPage = new PagosPage(driver);
		String cliente = "jbauchi";
		
		PagosPage.seleccionarTarea(".btn.btn-default","ABM Clientes");
		
		base__esperaDesaparezcaPantallaBloqueo(driver, 60, 1000);		
		PagosPage.eliminarClienteTramite(cliente);
		//FIXME la confirmacion de eliminacion deberia estar aca y no en el metodo anterior
		
		assertTrue(PagosPage.validarEliminacionCliente("El cliente se elimin� correctamente."));			
	}	
		
	
	
	//@Test
	public void TSNN_PagosAbm_Cambiar_logo_de_un_medio_de_pago() {
		//Para limpiar ambiente correr PagosAbm_Cambiar_a_logo_original.
		PagosPage PagosPage = new PagosPage(driver);	
		espera(2000);
		PagosPage.seleccionarTarea(".btn.btn-default", "ABM Logos Medios de Pago");									
		espera(2000);
		PagosPage.arturo__buscaEnGrillaNgTextoYejecutaAccion("BUI", "Medio de Pago", "cambiar", 2); //Medio de pago (AMEX, BUI, VISA, ETC), Titulo, Opcion a ejecutar (cambiar o editar), cantidad de columnas.
		espera(2000);
		PagosPage.SubirLogo();
		
	}
	
	
	//@Test
	public void TSNN_PagosAbm_Cambiar_tiempo_de_acreditacion() {
		//Para limpiar el ambiente correr PagosAbm_Restaurar_tiempo_de_acreditacion.
		PagosPage PagosPage = new PagosPage(driver);
		PagosPage.seleccionarTarea(".btn.btn-default", "ABM de Tiempos de Acreditación");
		espera(5000);
		PagosPage.arturo_enviarTextoTAcreditacion("Prueba Tiempo de Acreditacion - Automatizacion");
		espera(5000);
		TramitesPage TP = new TramitesPage(driver);
		TP.arturo_Portal_tramite_Verificacion_de_texto_tiempo_acreditacion();
	}

	//@Test(priority=9)
	public void TS19_ARTURO() {	}
	
	//@Test(priority=10)
	public void TS20_ARTURO() {	}
	

	
}

