package test;

import static org.testng.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.BUIPage;
import pages.BackOfficePage;
import pages.BasePage;
import pages.Login;
import pages.PagosPage;
import pages.SirToolsPage;
import pages.TramitesPage;
import pages.setConexion;



public class BackOffice  {


	protected WebDriver driver;
	protected  WebDriverWait wait;
	String usuario = "jbauchi";
	String paswd = "Ab123456";

	public static String random(int min, int max) {
		 int valorEntero = (int) Math.floor(Math.random()*(max-min+1)+min); 
			return String.valueOf(valorEntero);
	}
	
//	@AfterClass
	public void CloseChrome() {
		//JOptionPane.showMessageDialog(null, "CERRAR NAVEGADOR");
		driver.quit();	
	}


	@BeforeClass
	public void IngresoAmbiente() {
		this.driver = setConexion.setup();
		wait = new WebDriverWait(driver, 10);
		TestBase TB = new TestBase(driver);
		TB.PortalBackOffice();
		espera(3000);
		Login lg = new Login(driver);
		lg.logearBO(usuario, paswd);		
	}
	

//	@AfterMethod
	public void IrHome() {	
		TestBase TB = new TestBase(driver);
		TB.PortalBackOffice();

	}

	
	public void espera(long milisec) {
		try {Thread.sleep(milisec);} catch (InterruptedException e) {e.printStackTrace();}		
	}

	/*
	 *********************TEST*******************
	 */
	
	//@Test
	public void TS01_BackOffice_CrearUnUsuario() {
		/*
		 * FIXME contempla los parametros: BOLETA UNICA,PAGO ELECTRONICO, COBRANZA,PORTAL TRAMITE. 
		 * Se deberia contemplar
		 */
				
		BackOfficePage BO = new BackOfficePage(driver);
		
		String nombretxt = "prueba";
		String legajotxt = BO.LegajoRandom();
		String dependencia = "02-Registro Civil";
		String logintxt = "TestingAuto"+BO.LegajoRandom();
		String apellidotxt= "automatizada";
		String emailtxt="mail@deprueba.com";
		String sectortxt ="PruebasAutomatizadas";
		String dnitxt="24596729";
		
		BO.crearUsuario(nombretxt, apellidotxt, logintxt, emailtxt, sectortxt, dependencia, legajotxt, dnitxt);
				
		Assert.assertTrue(BO.usuariocreado());
		espera(3000);
	}

	
	//@Test
	public void TS02_BackOffice_Editar_un_usuario() {
		String nombretxt = "prueba";
		espera(3000);
		BackOfficePage BO = new BackOfficePage(driver);
		BO.menu("admin", "usuarios");
		BO.UsuariosDatosBscr(nombretxt);
		BO.UsuariosBtns("buscar");
		BO.editarUsuario();
		Assert.assertTrue(BO.usuarioeditado());		
	}

	
	//@Test
	public void TS03_BackOffice_CrearUnConcepto() {

		
		BackOfficePage BO = new BackOfficePage(driver);

		String periodo_tarifaria = "2018";
		String fecha_publicacion = "24/04/2018";
		String Cod1 = "01";
		String Cod2 = BasePage.random(50,60);
		String Cod3 = BasePage.random(1,99);	
		String Descripcion = "concepto_automatizacion_" + Cod1 + "_" + Cod2 + "_" + Cod3; 
		String Renglon1 = "renglon1_concepto_automatizacion_" + Cod1 + "_" + Cod2 + "_" + Cod3;
		
		String Agencia_asignada = "01-Dir. Gral del Registro Civil y Capacidad de las Personas";
		String valor = "10";
		boolean es_redondeable = true;
		boolean tiene_precio_fijo = true;
		boolean cantidad_variable = true;
		boolean es_devolucion = true;
		
		BO.crearConcepto(periodo_tarifaria, fecha_publicacion, Cod1, Cod2, Cod3, Descripcion, Renglon1,Agencia_asignada, valor, es_redondeable, tiene_precio_fijo, cantidad_variable, es_devolucion);
			
		Assert.assertTrue(BO.validacionConceptoCreado());		
	}

	
	//@Test(priority=1)
	public void TS03_01_BackOffice_AprobarConcepto() {				
		/*
		 *  CREO UN CONCEPTO
		 *  APRUEBO EL CONCEPTO
		 *  
		 *  DEMO: INDEPENDIENTE
		 */

		BackOfficePage BO = new BackOfficePage(driver);
		
		String periodo_tarifaria = "2018";
		String fecha_publicacion = "24/04/2018";
		String Cod1 = "01";
		String Cod2 = BasePage.random(50,60);
		String Cod3 = BasePage.random(1,99);
		String codigo = Cod1+"."+Cod2+"."+Cod3;
		String Descripcion = "concepto_automatizacion_" + Cod1 + "_" + Cod2 + "_" + Cod3; 
		String Renglon1 = "renglon1_concepto_automatizacion_" + Cod1 + "_" + Cod2 + "_" + Cod3;
		String Agencia_asignada = "01-Dir. Gral del Registro Civil y Capacidad de las Personas";
		String valor = "10";
		boolean es_redondeable = true;
		boolean tiene_precio_fijo = true;
		boolean cantidad_variable = true;
		boolean es_devolucion = true;
		
		BO.crearConcepto(periodo_tarifaria, fecha_publicacion, Cod1, Cod2, Cod3, Descripcion, Renglon1, Agencia_asignada, valor, es_redondeable, tiene_precio_fijo, cantidad_variable, es_devolucion);
				
		Assert.assertTrue(BO.validacionConceptoCreado());
		
		espera(3000);
		BO.aprobarConcepto(codigo);
						
		Assert.assertTrue(BO.validacionConceptoAprobado(codigo));				
	}

	
	//@Test(priority=2)
	public void TS03_02_BackOffice_AsignarDependencia_Concepto() {
		
		/*
		 *  CREO UN CONCEPTO
		 *  APRUEBO EL CONCEPTO
		 *  ASIGNO DEPENDENCIA AL CONCEPTO
		 *  
		 *  DEMO:INDEPENDIENTE
		 */
		BackOfficePage BO = new BackOfficePage(driver);
		
		String periodo_tarifaria = "2018";
		String fecha_publicacion = "24/04/2018";
		String Cod1 = "01";
		String Cod2 = BasePage.random(50,60);
		String Cod3 = BasePage.random(1,99);
		String codigo = Cod1+"."+Cod2+"."+Cod3;
		String Descripcion = "concepto_automatizacion_" + Cod1 + "_" + Cod2 + "_" + Cod3; 
		String Renglon1 = "renglon1_concepto_automatizacion_" + Cod1 + "_" + Cod2 + "_" + Cod3;
		String Agencia_asignada = "01-Dir. Gral del Registro Civil y Capacidad de las Personas";
		String valor = "10";
		boolean es_redondeable = true;
		boolean tiene_precio_fijo = true;
		boolean cantidad_variable = true;
		boolean es_devolucion = true;
		
		BO.crearConcepto(periodo_tarifaria, fecha_publicacion, Cod1, Cod2, Cod3, Descripcion, Renglon1, Agencia_asignada, valor, es_redondeable, tiene_precio_fijo, cantidad_variable, es_devolucion);
				
		Assert.assertTrue(BO.validacionConceptoCreado());
		
		espera(3000);
		BO.aprobarConcepto(codigo);
		
		String dep1 = "Registro Civil";
		String dep2 = "Pago Electr�nico";		
		
		BO.asignarDependencia_Concepto(codigo, dep1, dep2);
		Assert.assertTrue(BO.asignacionDependenciaExitosa());		
	}		
	
	
	//@Test(priority=3)
	public void TS03_03_BackOffice_eliminarConcepto() {
		/*
		 * CREACION DE UN CONCEPTO 
		 * ELIMINACION
		 * 
		 * DEMO: INDEPENDIENTE
		 */
		BackOfficePage BO = new BackOfficePage(driver);
		
		String periodo_tarifaria = "2018";
		String fecha_publicacion = "24/04/2018";
		String Cod1 = "01";
		String Cod2 = BasePage.random(50,60);
		String Cod3 = BasePage.random(1,99);
		//String codigo = Cod1+"."+Cod2+"."+Cod3;
		String Descripcion = "concepto_automatizacion_" + Cod1 + "_" + Cod2 + "_" + Cod3; 
		String Renglon1 = "renglon1_concepto_automatizacion_" + Cod1 + "_" + Cod2 + "_" + Cod3;
		String Agencia_asignada = "01-Dir. Gral del Registro Civil y Capacidad de las Personas";
		String valor = "10";
		boolean es_redondeable = true;
		boolean tiene_precio_fijo = true;
		boolean cantidad_variable = true;
		boolean es_devolucion = true;
		
		BO.crearConcepto(periodo_tarifaria, fecha_publicacion, Cod1, Cod2, Cod3, Descripcion, Renglon1, Agencia_asignada, valor, es_redondeable, tiene_precio_fijo, cantidad_variable, es_devolucion);
				
		Assert.assertTrue(BO.validacionConceptoCreado());
		
		espera(5000);
		BO.eliminarConcepto(Cod1+"."+Cod2+"."+Cod3);		
	}
	
	
	//@Test
	public void TS04_BackOffice_EditarUnConcepto() {
		/*
		 * CREO CONCEPTO
		 * EDITO CONCEPTO
		 */
		BackOfficePage BO = new BackOfficePage(driver);
		
		String periodo_tarifaria = "2018";
		String fecha_publicacion = "24/04/2018";//FIXME remplazar por fechahoy()
		String Cod1 = "01";
		String Cod2 = BasePage.random(50,60);
		String Cod3 = BasePage.random(1,99);
		String codigo = Cod1+"."+Cod2+"."+Cod3;
		String Descripcion = "concepto_automatizacion_" + Cod1 + "_" + Cod2 + "_" + Cod3; 
		String Renglon1 = "renglon1_concepto_automatizacion_" + Cod1 + "_" + Cod2 + "_" + Cod3;
		
		String Agencia_asignada = "01-Dir. Gral del Registro Civil y Capacidad de las Personas";
		String valor = "10";
		boolean es_redondeable = true;
		boolean tiene_precio_fijo = true;
		boolean cantidad_variable = true;
		boolean es_devolucion = true;
		
		BO.crearConcepto(periodo_tarifaria, fecha_publicacion, Cod1, Cod2, Cod3, Descripcion, Renglon1, Agencia_asignada, valor, es_redondeable, tiene_precio_fijo, cantidad_variable, es_devolucion);
				
		Assert.assertTrue(BO.validacionConceptoCreado());
		
		espera(3000);
		BO.EdicionConcepto(codigo);
			
		Assert.assertTrue(BO.validacionConceptoModificado());
		//FIXME validar mejor, que busque en la grilla y vea si figura la modificacion 
	}

	
	//@Test
	public void TS05_BackOffice_Consultar_Recaudaciones_Resultados_mayores_a_0() {
		try {Thread.sleep(3000);} catch (InterruptedException e) {e.printStackTrace();}
		BackOfficePage BO = new BackOfficePage(driver);
		BO.menu("reportes", "Consulta de Recaudaciones");
		BO.ConsultaDeRecaudaciones("01-Direcci�n Gral de Tesorer�a", "092 - Registros Manuales Centralizados", "- Ninguno -", "- Ninguno -","- Ninguno -", "Medio de Pago � Concepto � C�d. Dep � C�d. Pos - Cbte � OT");
		Assert.assertTrue(BO.validarbusquedareporte());
	}
	
	
	//@Test
	public void TS06_BackOffice_Consultar_Recaudaciones_Consolidadas_Resultados_mayores_a_0() {
		try {Thread.sleep(3000);} catch (InterruptedException e) {e.printStackTrace();}
		BackOfficePage BO = new BackOfficePage(driver);
		BO.menu("reportes", "Consulta de Recaudaciones Consolidadas");
		BO.ConsultaDeRecaudaciones("01-Direcci�n Gral de Tesorer�a", "092 - Registros Manuales Centralizados", "- Ninguno -", "- Ninguno -","- Ninguno -", "Medio de Pago � Concepto � C�d. Dep � C�d. Pos - Cbte � OT");
		Assert.assertTrue(BO.validarbusquedareporte());
	}
	
	
	//@Test
	public void TS07_BackOffice_Consultar_Recaudaciones_por_Concepto_Resultados_mayores_a_0() {
		try {Thread.sleep(3000);} catch (InterruptedException e) {e.printStackTrace();}
		BackOfficePage BO = new BackOfficePage(driver);
		BO.menu("reportes", "Consulta de Recaudaciones por Concepto");
		BO.ConsultaDeRecaudacionesporconcepto("01-Direcci�n Gral de Tesorer�a", "092 - Registros Manuales Centralizados", "Total descendiente");
		Assert.assertTrue(BO.validarbusquedareporte());
	}
	
	
	//@Test
	public void TS08_BackOffice_Consultar_Recaudaciones_por_Concepto_Consolidado_Resultados_mayores_a_0() {
		try {Thread.sleep(3000);} catch (InterruptedException e) {e.printStackTrace();}
		BackOfficePage BO = new BackOfficePage(driver);
		BO.menu("reportes", "Consulta de Recaudaciones por Concepto Consolidado");
		BO.ConsultaDeRecaudacionesporconcepto("01-Direcci�n Gral de Tesorer�a", "092 - Registros Manuales Centralizados", "Total descendiente");
		Assert.assertTrue(BO.validarbusquedareporte());
	}
	
	
	//@Test
	public void TS09_BackOffice_Editar_cuotas_de_Tarjeta() {	
		BackOfficePage BO = new BackOfficePage(driver);
		
		//modificar para pasarle tambien el nombre del pos
		BO.configurarCuotasTarjeta("AMEX","1,3,6,7");
		
		//validacion y acepta mensaje de Grabado exitoso
		espera(2000);//falla sino el waitforelement luego
		Assert.assertTrue(BO.validacionEdicionCuotasModificado());	
			
	}
	
	
	@Test(priority=4)
	public void TS09_01_BackOffice_Editar_cuotas_de_Pos() {
		/*
		 * DEMO: INDEPENDIENTE
		 */
		BackOfficePage BO = new BackOfficePage(driver);
		
		//modificar para pasarle tambien el nombre del pos
		BO.editarCuotasPos("AMEX","1",true,"3", false, "5", true,"6",false);
			
		//validacion y acepta mensaje de Grabado exitoso
		Assert.assertTrue(BO.validacionEdicionCuotasPosModificado());		
		
		//hago un assert para verificar lo que modifique  				
		espera(2000);
		Assert.assertEquals("1,5", BO.buscaEnGrillaViejaTextoEnColumnaYDevuelveTextoEnColumna("AMEX","1","3"));
	}
	
	//no funciona la aplicacion
	//@Test
	public void TS72_Anular_un_Cierre_Z(){
		//parametros
		String tipo = "Documento Nacional de Identidad";
		String nroDoc= "1000000";
		String mail = "correo@prueba.com";
		//pages
		SirToolsPage ST = new SirToolsPage(driver);
		BUIPage BUI = new BUIPage(driver);
		TramitesPage TP = new TramitesPage(driver);
		TestBase TB = new TestBase(driver);
		BackOfficePage BO = new BackOfficePage(driver);
		//creamos boleta
		String boleta = BUI.GenerarBoletaPagoVisa();
		//pagamos la boleta
		TB.PortalTramites();
		TP.menu("Paga tu Boleta");
		TP.DatosContribuyentePago(tipo, nroDoc, boleta, mail);
		espera(5000);
		TP.SeleccionarFormaDepago("VISA");
		TP.btnRealizarPago();
		TP.DatosTarjeta();
		Assert.assertTrue(TP.ValidarPagoVISA());
		//SirTools
		ST.IngresoAmbiente();
		ST.ejecutar("Procesamiento Diario PE", "-g -e");
		assertTrue(ST.ValidarCierreZ());
		TB.PortalBackOffice();
		espera(3000);
		BO.menu("Cobranza", "Generar Anulación");
		assertTrue(false);
	}

	
	//@Test
	public void TS69_Realizar_Cierre_Z_de_PE_y_ejecutar_BUISYNC(){
		//parametros
		String tipo = "Documento Nacional de Identidad";
		String nroDoc= "1000000";
		String mail = "correo@prueba.com";
		//pages
		SirToolsPage ST = new SirToolsPage(driver);
		BUIPage BUI = new BUIPage(driver);
		TramitesPage TP = new TramitesPage(driver);
		TestBase TB = new TestBase(driver);
		BackOfficePage BO = new BackOfficePage(driver);
		//creamos boleta
		String boleta = BUI.GenerarBoletaPagoVisa();
		//pagamos la boleta
		TB.PortalTramites();
		TP.menu("Paga tu Boleta");
		TP.DatosContribuyentePago(tipo, nroDoc, boleta, mail);
		espera(5000);
		TP.SeleccionarFormaDepago("VISA");
		TP.btnRealizarPago();
		TP.DatosTarjeta();
		Assert.assertTrue(TP.ValidarPagoVISA());
		//SirTools
		ST.IngresoAmbiente();
		ST.ejecutar("BUI Sync", "");
		assertTrue(false);
	}
	
	//@Test
	public void TS71_Realizar_cierre_de_PE_verificando_la_actualización_del_pagoestado_de_token_correspondiente_en_cada_tipo_de_Estado_Decidir(){
		//parametros
		String tipo = "Documento Nacional de Identidad";
		String nroDoc= "1000000";
		String mail = "correo@prueba.com";
		//pages
		SirToolsPage ST = new SirToolsPage(driver);
		BUIPage BUI = new BUIPage(driver);
		TramitesPage TP = new TramitesPage(driver);
		TestBase TB = new TestBase(driver);
		BackOfficePage BO = new BackOfficePage(driver);
		//creamos boleta
		String boleta = BUI.GenerarBoletaPagoVisa();
		//pagamos la boleta
		TB.PortalTramites();
		TP.menu("Paga tu Boleta");
		TP.DatosContribuyentePago(tipo, nroDoc, boleta, mail);
		espera(5000);
		TP.SeleccionarFormaDepago("VISA");
		TP.btnRealizarPago();
		TP.DatosTarjeta();
		Assert.assertTrue(TP.ValidarPagoVISA());
		//SirTools
		ST.IngresoAmbiente();
		ST.ejecutar("Procesamiento Diario PE", "-g -e");
		assertTrue(ST.ValidarCierreZ());
	}

}