package pages;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.print.DocFlavor.STRING;
import javax.swing.JOptionPane;

import org.omg.PortableInterceptor.AdapterStateHelper;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import test.TestBase;



public class PagosPage extends BasePage {

	final WebDriver driver;
	public PagosPage(WebDriver driver){
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	public void WaitForElement(By element, int tiempo) {
		WebDriverWait wait;
		wait = new WebDriverWait(driver, tiempo);
		wait.until(ExpectedConditions.visibilityOfElementLocated(element));
	}
	public void IngresoAmbiente() {
		TestBase TB = new TestBase(driver);
		TB.PortalPagos();
		Login lg = new Login(driver);
		lg.logearPagosAbm("jbauchi","Ab123456");
		}
		
	public void espera(long milisec) {
		try {Thread.sleep(milisec);} catch (InterruptedException e) {e.printStackTrace();}		
	}

	public void WaitForElement2(By element, int tiempo) {
		WebDriverWait wait;
		wait = new WebDriverWait(driver, tiempo);
		wait.until(ExpectedConditions.elementToBeClickable(element));
	}
	
	
	/*
	 ****************WEBELEMENTS********** 
	 */

	@FindBy(how = How.CSS,using = ".btn.btn-success.ABMLinkTabla")	
	private WebElement BtnCrearNuevoTramite;

	@FindBy(how = How.CSS,using = ".btn.btn-success.ABMLinkTabla")  
	private WebElement BtnCrearNuevoCliente;


	//EDICION DE TRAMITE

	@FindBy(how = How.XPATH,using = "//select[@data-ng-model='edicion.tramite.Dependencia']")
	private WebElement DropDependecias;

	@FindBy(how = How.XPATH,using = "//select[@data-ng-model='edicion.conceptoSeleccionado']")
	private WebElement DropConceptos;

	@FindBy(how = How.XPATH,using = "//select[@data-ng-model='edicion.mdpSeleccionado']")
	private WebElement DropMediosPagos;

	@FindBy(how = How.ID,using = "b424c87c-9f38-4d2e-a58b-64390789a7f5")
	private WebElement LstbxCuotas;

	@FindBy(how = How.CSS,using = ".btn.btn-lg.btn-success")
	private WebElement btnGuardarTramite;

	@FindBy(how = How.CSS,using = ".btn.btn-lg.btn-success")
	private WebElement btnGuardarCliente;

	@FindBy(how = How.CSS,using = ".ABMLinkTabla.glyphicon.glyphicon-chevron-right")
	private WebElement btnAgregarCuota;

	@FindBy(how = How.CSS,using = ".ng-pristine.ng-valid")
	private WebElement DropUsuarios;

	@FindBy(how = How.XPATH,using = "//select[@data-ng-model='edicion.tramiteSeleccionado']")
	private WebElement DropTramite;
	
	@FindBy(how = How.XPATH,using = "//input[@data-ng-model='edicion.tramite.Nombre']")
	private WebElement nombreTramite;

	@FindBy(how = How.ID,using = "tablaTramites")
	private WebElement tablaTramite;

	//EDICION DE LOGO

	@FindBy(how = How.XPATH,using = "/html/body/div[2]/div/div[1]/div[2]/table/tbody/tr[1]/td[3]/a")
	private WebElement radioBtn_editarLogo;
	
	@FindBy(how = How.XPATH,using = "/html[1]/body[1]/div[2]/div[1]/div[1]/div[2]/table[1]/tbody[1]/tr[1]/td[1]/div[1]/div[2]/textarea[1]")
	private WebElement textBoxTAcreditacion;
	
	//TIEMPO DE ACREDITACION
	
	@FindBy (how = How.CSS,using = ".input-sm.form-control.ng-pristine.ng-valid")
	private WebElement textoHTMLtiempoAcreditacion;
	
	/*
	 *
	 *******************METODOS**************** 
	 */


	public void buscaClaseYTexto_Click(String claseparaccsselector, String texto) {
		List<WebElement> listado = driver.findElements(By.cssSelector(claseparaccsselector));

		for(WebElement e:listado) {					

			if(e.getText().toLowerCase().equals(texto.toLowerCase())) {
				e.click();
				break;					
			}

		}		
	}

	public void buscaClaseYAtributo_Click(String claseparaccsselector, String pAtributo,String pCampo) {
		List<WebElement> listado = driver.findElements(By.cssSelector(claseparaccsselector));

		for(WebElement e:listado) {	
			//System.out.println(e.getAttribute(pAtributo));
			if(e.getAttribute(pAtributo).toLowerCase().contains(pCampo.toLowerCase())) {
				e.click();
				break;

			}
		}		
	}

	public void buscaClaseYAtributo_Tipea(String claseparaccsselector, String pAtributo,String pCampo, String texto) {  
		List<WebElement> listado =  driver.findElements(By.cssSelector(claseparaccsselector));	

		for(WebElement e:listado) {	
			if(e.getAttribute(pAtributo).toLowerCase().contains(pCampo.toLowerCase())) {
				e.sendKeys(texto);
				break;
			}
		}	

	}

	public void seleccionarTarea(String clase, String texto) {	//PONERLO EN LA CLASE BASEPAGE
		WaitForElement(By.xpath("//a[contains(.,'ABM Clientes')]"), 60);
		espera(1000);
		buscaClaseYTexto_Click(clase,texto);
	}

	public void esperaDesaparezcaPantallaBloqueo(int ciclos, int espera)  {
		int cantidad = driver.findElements(By.id("esperador")).size();
		System.out.println("cantidad:" + cantidad);
		if(cantidad != 0) {
			for(int i=0;i<ciclos;i++) {
				if(driver.findElement(By.id("esperador")).getAttribute("style").equals("display: none;"))
				{System.out.println("Esta bloqueado");
				break;}
				//System.out.println(driver.findElement(By.id("esperador")).getAttribute("style"));
				if(driver.findElement(By.id("esperador")).getAttribute("style").equals("display: block;"))
					System.out.println("Esta desbloqueado");				
				espera(espera);				
			}
		}
		else
			System.out.println("No habia ventana de bloqueo");
	}

	
	
	public void crearNuevoTramite_click()  {		
		BtnCrearNuevoTramite.click();	
	}

	
	public void crearNuevoCliente_click()  {
		BtnCrearNuevoCliente.click();			
	}

	
	/*
	 * SELECCIONA 1 MEDIO DE PAGO
	 */
	public void seleccionarMediosPago(String mp1) {
		
		if(mp1.equals("VISA") || mp1.equals("TODOS")) {
			//selecciona el medio de pago
			setSimpleDropdown(DropMediosPagos,"VISA");

			//agrega el medio de pago
			espera(2000);
			buscaClaseYAtributo_Click(".btn.btn-success.btn-sm", "data-ng-click", "agregarMedioDePago");

			//selecciona la cantidad de cuotas		
			//buscaClaseYAtributo_Click(".ng-binding.ng-scope", "value", "1");			PODRIA SER ASI, SI EL METODO HICIERA UN EQUAL EN LUGAR DE CONTAIN, YA QUE ESTA EL VALOR 1 Y 12
			espera(3000);
			Select algo = new Select (LstbxCuotas);
			algo.selectByValue("1");

			//agrega las cuotas
			btnAgregarCuota.click();
		}
		
		if(mp1.equals("INTERBANKING") || mp1.equals("TODOS")) {
			//selecciona el medio de pago
			setSimpleDropdown(DropMediosPagos,"INTERBANKING");
			espera(5000);

			//agrega el medio de pago
			espera(5000);
			buscaClaseYAtributo_Click(".btn.btn-success.btn-sm", "data-ng-click", "agregarMedioDePago");

			buscaClaseYAtributo_Tipea(".input-sm.form-control.ng-pristine.ng-valid","data-ng-model","CodigoComunidad","34999032089");  
			buscaClaseYAtributo_Tipea(".input-sm.form-control.ng-pristine.ng-valid","data-ng-model","VendedorCuit","34999032089");
			buscaClaseYAtributo_Tipea(".input-sm.form-control.ng-pristine.ng-valid","data-ng-model","IdCuentaRecaudacion","0440102400100012006574");
		}

		if(mp1.equals("RAPIPAGO") || mp1.equals("TODOS")) {
			setSimpleDropdown(DropMediosPagos,"RAPIPAGO");
			espera(5000);
			

			//agrega el medio de pago
			espera(5000);
			buscaClaseYAtributo_Click(".btn.btn-success.btn-sm", "data-ng-click", "agregarMedioDePago");
		}


		if(mp1.equals("REDLINK") || mp1.equals("TODOS")) {
			setSimpleDropdown(DropMediosPagos,"REDLINK");
			espera(5000);

			//agrega el medio de pago
			espera(5000);
			buscaClaseYAtributo_Click(".btn.btn-success.btn-sm", "data-ng-click", "agregarMedioDePago");
		}		
	}
	
	/*
	 * SELECCIONA 2 MEDIO DE PAGO
	 */
	public void seleccionarMediosPago (String mp1, String mp2) {		
		if(mp1.equals("VISA") || mp2.equals("VISA"))
			seleccionarMediosPago("VISA");
		if(mp1.equals("INTERBANKING") || mp2.equals("INTERBANKING"))
			seleccionarMediosPago("INTERBANKING");
		if(mp1.equals("RAPIPAGO") || mp2.equals("RAPIPAGO"))
			seleccionarMediosPago("RAPIPAGO");
		if(mp1.equals("REDLINK") || mp2.equals("REDLINK"))
			seleccionarMediosPago("REDLINK");		
	}
	
	/*
	 * SELECCIONA 3 MEDIO DE PAGO
	 */
	public void seleccionarMediosPago (String mp1, String mp2, String mp3) {		
		if(mp1.equals("VISA") || mp2.equals("VISA") || mp3.equals("VISA"))
			seleccionarMediosPago("VISA");
		if(mp1.equals("INTERBANKING") || mp2.equals("INTERBANKING") || mp3.equals("INTERBANKING"))
			seleccionarMediosPago("INTERBANKING");
		if(mp1.equals("RAPIPAGO") || mp2.equals("RAPIPAGO") || mp3.equals("RAPIPAGO") )
			seleccionarMediosPago("RAPIPAGO");
		if(mp1.equals("REDLINK") || mp2.equals("REDLINK") || mp3.equals("REDLINK"))
			seleccionarMediosPago("REDLINK");		
	}
	
	/*
	 * SELECCIONA 4 MEDIO DE PAGO
	 */
	public void seleccionarMediosPago (String mp1, String mp2, String mp3, String mp4) {		
		if(mp1.equals("VISA") || mp2.equals("VISA") || mp3.equals("VISA") || mp4.equals("VISA"))
			seleccionarMediosPago("VISA");
		if(mp1.equals("INTERBANKING") || mp2.equals("INTERBANKING") || mp3.equals("INTERBANKING")|| mp4.equals("INTERBANKING"))
			seleccionarMediosPago("INTERBANKING");
		if(mp1.equals("RAPIPAGO") || mp2.equals("RAPIPAGO") || mp3.equals("RAPIPAGO") || mp4.equals("RAPIPAGO") )
			seleccionarMediosPago("RAPIPAGO");
		if(mp1.equals("REDLINK") || mp2.equals("REDLINK") || mp3.equals("REDLINK")|| mp4.equals("REDLINK"))
			seleccionarMediosPago("REDLINK");		
	}
	
	/*
	 * ELIMINA SOLO UN MEDIO DE PAGO
	 */
	public void eliminarMediosPago() {//FIXME mejorar la logica para que elimine el MP que le envio por parametro
		buscaClaseYAtributo_Click(".ABMLinkTabla.glyphicon.glyphicon-remove","data-ng-click","removerMedioDePago" );		
	}
	
	/*
	 * COMPLETA SECCION CONFIGURACION GENERAL DEL TRAMITE
	 */	
	public void configuracionGeneralTramite(String tituloTramite, boolean pagable) {
		buscaClaseYAtributo_Tipea(".input-sm.form-control.ng-pristine.ng-valid","data-ng-model","Nombre",tituloTramite);  
		pagablePorPortal(pagable);
	}
	
	/*
	 * COMPLETA SECCION DEPENDENCIA ASOCIADA DEL TRAMITE
	 */
	public void dependenciaAsociada(String dependencia) {
		if(!dependencia.equals("")) 
			{
			//setSimpleDropdown(DropDependecias,dependencia); EN PAGOS EL TRAMITE LOS VALORES DEL COMBO FIGURAN CON INDICE, POR ESO TENGO QUE BUSCAR PARCIAL EL NOMBRE
			setSimpleDropdownByPartialText(DropDependecias,dependencia);
			}
		}
	
	
	/*
	 * COMPLETA SECCION CONCEPTO DEL TRAMITE
	 */
	public void seleccionarConceptoDelTramite (String concepto) {
		/*
		 * EL COMBO TIENE EL NUMERO DE CONCEPTO SIN '0' EN LAS DUPLAS.
		 */		
		espera(5000);
		//setSimpleDropdown(DropConceptos,concepto);
		String conceptoFormateado = base__formateaConceptoSinCero(concepto);
		setSimpleDropdownByPartialText(DropConceptos,conceptoFormateado);
	}
	
	public void agregarConceptoDelTramite() {
		espera(5000);
		buscaClaseYAtributo_Click(".btn.btn-success.btn-sm","data-ng-click", "agregarItem");
	}
	
	/*
	 * DEVUELVE EL VALOR SELECCIONADO EN EL COMBO CONCEPTOS
	 */
	public String getValorSeleccionadoDropConceptos() {
		return getSimpleDropdown(DropConceptos);
	}
	
	/*
	 * COMPLETA SECCION DECIDIR DEL TRAMITE
	 */
	public void decidir(boolean decidir, String nroComercio) {
				if(decidir) {
			buscaClaseYAtributo_Click(".ng-pristine.ng-valid", "data-ng-model", "utilizaDecidir");	
			
			buscaClaseYAtributo_Tipea(".input-sm.form-control.ng-pristine.ng-valid","data-ng-model","NroComercio","00151218");
		}
	}
	
	/*
	 * COMPLETA SECCION NOTIFICACION DEL TRAMITE
	 */
	public void notificacion (boolean notifica) {
		if(notifica)
		{
			buscaClaseYAtributo_Click(".ng-pristine.ng-valid", "data-ng-model", "utilizaNotificacion");
			buscaClaseYAtributo_Tipea(".input-sm.form-control.ng-pristine.ng-valid","data-ng-model","Notificacion.Email","a@a.com");		
		}
	}
		
	
	/*
	 * COMPLETA LOS CAMPOS DE EDICION DEL TRAMITE PARA 4 MEDIOS DE PAGO
	 */
	public void completarEdicionTramite(String tituloTramite, boolean pagablePorPortal, String dependencia, String concepto,  boolean decidir, boolean notificacion, String medioPago1, String medioPago2, String medioPago3, String medioPago4)   {		
		String numeroDeComercio = "00151218"; //FIXME si usa decidir, le paso el valor de decidir 2.0. Ver si conviene que sea parametro del metodo
		
		configuracionGeneralTramite(tituloTramite,pagablePorPortal);
	
		dependenciaAsociada(dependencia);
		
		seleccionarConceptoDelTramite(concepto);
		agregarConceptoDelTramite();
	
		decidir(decidir,numeroDeComercio);

		notificacion(notificacion);
 
		seleccionarMediosPago(medioPago1,medioPago2,medioPago3,medioPago4);
	}

		
	
	public void completarEdicionTramiteCon2Conceptos(String tituloTramite, boolean pagablePorPortal, String dependencia, String concepto1,String concepto2,  boolean decidir, boolean notificacion, String medioPago1, String medioPago2, String medioPago3, String medioPago4)   {		
		String numeroDeComercio = "00151218"; //FIXME si usa decidir, le paso el valor de decidir 2.0. Ver si conviene que sea parametro del metodo
		
		configuracionGeneralTramite(tituloTramite,pagablePorPortal);
	
		dependenciaAsociada(dependencia);
		
		seleccionarConceptoDelTramite(concepto1);
		agregarConceptoDelTramite();
		
		seleccionarConceptoDelTramite(concepto2);
		agregarConceptoDelTramite();
	
		decidir(decidir,numeroDeComercio);

		notificacion(notificacion);
 
		seleccionarMediosPago(medioPago1,medioPago2,medioPago3,medioPago4);
	}
		
	
	
	public void completarEdicionCliente(String cliente, String tramite) {
		buscaClaseYAtributo_Tipea(".ng-pristine.ng-valid", "data-ng-model", "edicion.usuarioBusqueda", "jbauchi");

		//cargar usuario
		espera(3000);
		buscaClaseYAtributo_Click(".btn.btn-sm", "data-ng-click", "cargarUsuarios");

		//seleccionar usuario
		espera(3000);	
		setSimpleDropdown(DropUsuarios, cliente);

		//seleccionar tramite
		seleccionarTramite(tramite);	//ELIJE EL ULTIMO TRAMITE DEL MISMO NOMBRE DE LA LISTA
	}

	public void seleccionarTramite(String tramite) {
		List<String> lista= BuscarTramite(tramite);
		DropTramite.click();
		int b= (lista.size()-1);
		//System.out.println(b);
		String tramiting = lista.get(b).toString(); 
		//System.out.println("El tramitin es:"+ tramiting);
		setSimpleDropdown(DropTramite,tramiting);			//probar de poner drop usuario. Si es asi, entonces no importa si hay varios de la misma clase, importa que en alguno exista el value que voy a buscar y ese no se repita???
	}

	public List<String> BuscarTramite(String tramite) {
		List<String> options = new ArrayList<String>();
		for (WebElement option : new Select(DropTramite).getOptions()) {
			String txt = option.getText();

			if (option.getText().contains(tramite)) {
				options.add(option.getText());
				//System.out.println(txt);

			}
		}
		return options;
	}
	
	public void elegirVerificarNombreTramite() {
		String tramite1 = "Solicitud de Partidas Urgentes";
		String tramite2 = "Servicios Registro Civil";
		String texto = nombreTramite.getAttribute("value");
		System.out.println(texto);
		
		if ((texto.toLowerCase()).contains(tramite1.toLowerCase())) {
			seleccionarTramite(tramite2);
		} else {
			seleccionarTramite(tramite1);
		}
		
	}
	
	//FIXME PROBAR YA QUE ESTO ES SUPUESTAMENTE UN OVERLOAD DEL METODO DE ARRIBA.
	public void elegirVerificarNombreTramite(String tramite) {
		String tramite1 = "Solicitud de Partidas Urgentes";
		String tramite2 = "Servicios Registro Civil";
		String texto = nombreTramite.getAttribute("value");
		System.out.println(texto);
		
		if (!(texto.toLowerCase()).contains(tramite.toLowerCase())) {
			seleccionarTramite(tramite);
		} else if ((texto.toLowerCase()).contains(tramite.toLowerCase()) && !(texto.toLowerCase().contains(tramite1)) ) {
			seleccionarTramite(tramite1);
		} else {
			seleccionarTramite(tramite2);
		}
	}
	
	

	public void guardarTramite_click() {
		btnGuardarTramite.click();
	}

	public void guardarCliente_click() {
		btnGuardarCliente.click();		
	}

	//--------------------------VALIDACIONES-------------------------
	public boolean validarCreacionTramite(String mensaje)  {
		/*
		 * FIXME HABRIA QUE IGUALARLO A COMO FUNCIONAN LOS DE BACKOFFICE
		 */
		espera(5000);
		String txt = driver.findElement(By.id("dataMensaje")).getText();
		//Systemtem.out.println(txt);
		driver.findElement(By.id("btnAceptar")).click();
		return (txt.equals(mensaje));	
	}

	public boolean validarEdicionCliente(String mensaje)  {
		WaitForElement(By.id("dataMensaje"), 60);
		String txt = driver.findElement(By.id("dataMensaje")).getText();
		return (txt.equals(mensaje));	
	}

	public void buscaEnGrillaNgTextoYejecutaAccion(String texto, String tipo, String accion,int cant_columnas, long espera)//se usa para tramite y cliente en Portal Pago Electronico ABM y 
	{ 

		try {Thread.sleep(espera);}catch (InterruptedException e) {	e.printStackTrace();}
		List<WebElement> listado = driver.findElements(By.cssSelector(".ng-binding"));

		int i = 0;
		Boolean existe = false;
		for(WebElement e:listado) {

			//System.out.println(e.getText());
			if(e.getText().contains(texto)){	
				existe = true;
				break;
			}i++;
		}	

		if(existe) {
			int fila = (i/cant_columnas)+1;
			System.out.println("Fila: "+fila);
			String ruta_xpath="";
			if(tipo.equals("Tramite")) {
				ruta_xpath = "//*[@id='tablaTramites']/tbody/tr["+ String.valueOf(fila)  +"]/td[5]/a[1]";	
				driver.findElement(By.xpath(ruta_xpath)).click();

				//confirmo el eliminar
				try {Thread.sleep(2000);}catch (InterruptedException e) {	e.printStackTrace();}
				driver.findElement(By.id("btnConfirmarMensaje")).click();
			}

			else {
				ruta_xpath = "//*[@id='tablaClientes']/tbody/tr["+ String.valueOf(fila)  +"]/td[4]/a";	
				driver.findElement(By.xpath(ruta_xpath)).click();

				//confirmo el eliminar
				try {Thread.sleep(2000);}catch (InterruptedException e) {	e.printStackTrace();}

				Alert alert = driver.switchTo().alert();
				alert.accept(); 
				try {Thread.sleep(2000);}catch (InterruptedException e) {	e.printStackTrace();}

				driver.findElement(By.cssSelector(".btn.btn-sm.btn-default")).click();
			}					
		}
		else
			System.out.println("No hay "+ tipo + "  para eliminar");	
		try {Thread.sleep(5000);}catch (InterruptedException e) {	e.printStackTrace();}
	}

	public Boolean validarEliminacionCliente(String mensaje) {
		WaitForElement(By.id("dataMensaje"), 60);
		String txt = driver.findElement(By.id("dataMensaje")).getText();
		return (txt.equals(mensaje));

	}

	
	public void pagablePorPortal(boolean esPagablePorPortal) {
		WebElement elemento = driver.findElement(By.xpath("//input[@data-ng-model='edicion.tramite.EsPagablePorPortal']"));
		
		seteaCheckbox(elemento,esPagablePorPortal);
	}


	public void deshabilitarTramite(String titulo_tramite) {		
		base__buscaEnGrillaNgTextoYejecutaAccion(driver,titulo_tramite,"Tramite","eliminar",3);				
	}


	public void eliminarClienteTramite(String cliente) {		
		base__buscaEnGrillaNgTextoYejecutaAccion(driver,cliente,"Cliente","eliminar",2);
	}
	
	
	public void rehabilitarTramite(String tramite) {	
		driver.findElement(By.xpath("//button[contains(.,'Inactivo')]")).click();
		
		base__esperaDesaparezcaPantallaBloqueo(driver, 60, 1000);
		base__buscaEnGrillaNgTextoYejecutaAccion(driver,tramite,"Tramite","reactivar",3);					
	}


	public void editarTramite_click(String tramite) {		
		base__buscaEnGrillaNgTextoYejecutaAccion(driver,tramite,"Tramite","editar",3);		
	}


	
	public boolean validarModificacionDependencia(String mensaje) {
		WaitForElement(By.id("dataPrompt"), 60);
		String txt = driver.findElement(By.id("dataPrompt")).getText();
		System.out.println("mensaje:"+txt);
		driver.findElement(By.id("promptBtnAceptar")).click();		
		return (txt.contains(mensaje));	//se usa contains porque el mensaje del alert es compuesto con los numeros de los conceptos
	}

	
	
	
/////////////////////ACAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
/*		// seleccionar el ELIMINAR del (tramite_automatizacion_001);
		WaitForElement(By.cssSelector(".ng-binding"), 60);//TODO poner un waitforelement porque a veces tarda mucho
		base__buscaEnGrillaNgTextoYejecutaAccion(driver,titulo_tramite,"Tramite","eliminar",3);		*/

	
	
	
	
	
	public void crearTramite(String titulo_tramite, boolean pagablePorPortal, String dependencia, String concepto,
			boolean decidir, boolean notificacion, String medio_pago1, String medio_pago2, String medio_pago3,
			String medio_pago4) {

		seleccionarTarea(".btn.btn-default","ABM Trámites");
		
		base__esperaDesaparezcaPantallaBloqueo(driver,90,1000);			
		crearNuevoTramite_click();		

		
		base__esperaDesaparezcaPantallaBloqueo(driver,90,1000);				
		completarEdicionTramite(titulo_tramite,pagablePorPortal,dependencia,concepto,decidir, notificacion,medio_pago1,medio_pago2,medio_pago3,medio_pago4);
		
		guardarTramite_click();
		System.out.println("Se creo en PagosABM el tramite:"+titulo_tramite);
	}


	public void crearTramiteCon2Conceptos(String titulo_tramite, boolean pagablePorPortal, String dependencia, String concepto1,String concepto2,
			boolean decidir, boolean notificacion, String medio_pago1, String medio_pago2, String medio_pago3,
			String medio_pago4) {

		seleccionarTarea(".btn.btn-default","ABM Trámites");
		
		base__esperaDesaparezcaPantallaBloqueo(driver,90,1000);			
		crearNuevoTramite_click();		

		
		base__esperaDesaparezcaPantallaBloqueo(driver,90,1000);				
		completarEdicionTramiteCon2Conceptos(titulo_tramite,pagablePorPortal,dependencia,concepto1,concepto2,decidir, notificacion,medio_pago1,medio_pago2,medio_pago3,medio_pago4);
		
		guardarTramite_click();
		System.out.println("Se creo en PagosABM el tramite:"+titulo_tramite);
	}

	
	
	
	public void desmarcarPagablePorPortal(String titulo_tramite) {
		seleccionarTarea(".btn.btn-default","ABM Tr�mites");

		base__esperaDesaparezcaPantallaBloqueo(driver,90,1000);	
		editarTramite_click(titulo_tramite);		
		
		base__esperaDesaparezcaPantallaBloqueo(driver,90,1000);	
		pagablePorPortal(false);
		guardarTramite_click();		
		System.out.println("Se desmarco pagable por portal en el tramite:"+titulo_tramite);
	}
	


	public void esperaDesaparezcaElemento(int ciclos, int espera, By elemento)  {
		for (int i = 0; i < ciclos; i = i + 1) {
			if (driver.findElements(elemento).size() ==0) {
				System.out.println("existe");
				break;

			} else {
				System.out.println("NO existe");
				i = ciclos+1;
			}
			espera(espera);
		}
		System.out.println("salio del loop");
	}

	
	public void arturo_enviarTextoTAcreditacion(String textoModificado) {	// aca se mete al Tiempo de Acred y cambia el texto. - ARTURO
		textBoxTAcreditacion.click();
		textBoxTAcreditacion.sendKeys(Keys.chord(Keys.CONTROL, "a"), textoModificado);
		driver.findElement(By.cssSelector(".btn.btn-lg.btn-success")).click();
		espera(2000);
		driver.findElement(By.cssSelector(".btn.btn-sm.btn-default")).click();
	}	
	
	
	public void Texto_HTML_TAcred_Original () {  // ARTURO - Quiero volver a poner el texto HTML original.
		String textoOriginalTAcred = "<p></p><h4>Tiempos de acreditación de acuerdo al medio de pago seleccionado:"
				+ "</h4><div><br><strong><u>Tarjetas de Crédito:</u></strong> en línea.<br><strong><u>Interbanking:"
				+ "</u></strong> en línea.<br><strong><u>Cupón de Rapipago:</u></strong> 72 hs.hábiles.<br><strong>"
				+ "<u>PAGOS LINK:</u></strong> en línea.<br><strong><u> PagoMisCuentas:</u></strong> en línea.<strong>"
				+ "<br><u>BA:</u></strong> Boleta para pagar en:<br><ul><li>Cajas del GCBA: 24 hs.</li><li>Cajeros automáticos "
				+ "del GCBA(ATM): Inmediato.</li><li>Sucursales del Banco Ciudad(por ventanilla) y en terminales de autoservicio"
				+ "(TAS): 72 hs.</li><li>Pago Fácil, Rapipago y BaPro: 70 hs.hábiles.</li></ul><strong><u>Canales de REDLINK:</u>"
				+ "</strong> Los canales habilitados para abonar son los siguientes:<br><ul><li>Internet(www.linkpagos.com.ar): 24 "
				+ "hs.</li><li>Homebanking de entidades financieras adheridas a Red Link: 24 hs.</li><li>Cajeros Automáticos Link: "
				+ "24 hs.</li><li>Link Celular: 24 hs.</li><li>Banca Telefónica: 24 hs.</li></ul></div><h4>Usted podrá continuar con "
				+ "la gestión del trámite a partir de que se encuentre acreditado el pago.</h4><p></p>";
		espera(2000);
		textBoxTAcreditacion.click();
		textBoxTAcreditacion.sendKeys(Keys.chord(Keys.CONTROL, "a"), textoOriginalTAcred);
		espera(2000);
		driver.findElement(By.cssSelector(".btn.btn-lg.btn-success")).click();
		espera(2000);
		driver.findElement(By.cssSelector(".btn.btn-sm.btn-default")).click();
	}	
	
	

	 public void arturo__buscaEnGrillaNgTextoYejecutaAccion(String texto, String tipo, String accion, int cant_columnas) //*tipo=Medio de Pago *accion=cambiar/editar   *el valor cant_columnas = columnas de datos (no incluye botones) 
	  {  // ARTURO

	    List<WebElement> listado = driver.findElements(By.cssSelector(".ng-binding"));
	    System.out.println(listado.size());

	    int i = 0;
	    Boolean existe = false;
	    for(WebElement e:listado) {
	      System.out.println(e.getText());
	      if(e.getText().contains(texto)){  
	        existe = true;
	        break;
	      }i++;
	    }  
	    if(existe) {
	        int fila = (i/cant_columnas)+1;
	        System.out.println("Fila: "+fila);
	        String ruta_xpath="";

	        if(tipo.equals("Medio de Pago")) {
	          
	          if(accion.toLowerCase().equals("cambiar")) {
	            ruta_xpath = "//*[@id='tablaMP']/tbody/tr["+ String.valueOf(fila)  +"]/td[" + String.valueOf(cant_columnas+1)+ "]/a";
	            System.out.println(ruta_xpath);
	            driver.findElement(By.xpath(ruta_xpath)).click();

	          }
	          if(accion.toLowerCase().equals("editar")) {
	            ruta_xpath = "//*[@id='tablaTramites']/tbody/tr["+ String.valueOf(fila)  +"]/td[" + String.valueOf(cant_columnas+1) + "]/a[1]";
	            
	            //click en boton EDITAR
	            driver.findElement(By.xpath(ruta_xpath)).click();
	            
	          }
	          
	        }
	        else {
	          System.out.println("No hay "+ tipo + "  para " + accion);
	          esperaX(2000);
	          }
	        }

	    }



	public void SubirLogo() {
		try {	// LA LINEA DE ABAJO EJECUTA EL SCRIPT DE AUTOIT //@TOFIX hay que organizar donde poner los logos y modificar los scripts.
			Runtime.getRuntime().exec("C:\\LogosMediosDePago\\SubirLogo.exe");
			}
		catch (IOException e) {e.printStackTrace();}
		espera(5000);
		if (ValidarCambioDeLogo()) {
			driver.findElement(By.tagName("body")).click();
			System.out.println("Este test tuvo exito en validar");
		} else {
			System.out.println("Este test fallo en validar el cambio de logo");
			}
		}

	
	
	private boolean ValidarCambioDeLogo() {
		espera(5000);
		boolean a;
		String txt = driver.findElement(By.id("dataMensaje")).getText();
		System.out.println(txt);
		a = txt.equals("El nuevo logo se guardó correctamente.");
		return a;	
		}
	
	
		public void ElegirLogoOriginal() {  //TOFIX esto es solo para prueba
			try {	// LA LINEA DE ABAJO EJECUTA EL SCRIPT DE AUTOIT //@TOFIX hay que organizar donde poner los logos y modificar los scripts.
				Runtime.getRuntime().exec("C:\\\\LogosMediosDePago\\\\SubirLogoBUI.exe");
				}
			catch (IOException e) {e.printStackTrace();}
			espera(5000);
			if (ValidarCambioDeLogo()) {
				driver.findElement(By.tagName("body")).click();
				System.out.println("Este test tuvo exito en validar");
			} else {
				System.out.println("Este test fallo en validar el cambio de logo");
				}
			
		} 
		
		public void asociarClienteATramite(String cliente, String titulo_tramite) {
			TestBase TB = new TestBase(driver);
			TB.PortalPagos(); 

			seleccionarTarea(".btn.btn-default","ABM Clientes");

			base__esperaDesaparezcaPantallaBloqueo(driver, 60, 1000);
			crearNuevoCliente_click();

			base__esperaDesaparezcaPantallaBloqueo(driver, 60, 1000);
			completarEdicionCliente(cliente,titulo_tramite);

			guardarCliente_click();

			espera(1000);
			if(isAlertPresent(driver)) {
				driver.switchTo().alert().accept();
			}
			//luego de un segundo desaparece el mensaje de "El cliente se guardó correctamente."
			
			
		}
}

