package pages;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.swing.JOptionPane;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class BasePage {
	protected  WebDriver driver;

	/*
	 * Espera N milisegundos 
	 */
	public void esperaX(long milisec) {
		try {Thread.sleep(milisec);} catch (InterruptedException e) {e.printStackTrace();}		
	}

	/*
	 * Selecciona un elemento de la listbox segun el valor
	 */
	public void setSimpleDropdown(WebElement element, String value) {
		Select field = new Select(element);
		element.click();
		field.selectByVisibleText(value);
	}
	
	/*
	 * Selecciona un elemento de la listbox segun el texto parcial
	 */
	public void setSimpleDropdownByPartialText(WebElement element, String parcialText) {
		String rutaXpath = "//option[contains(text(),'" + parcialText + "')]";
		element.click();
		element.findElement(By.xpath(rutaXpath)).click();
	}
	
	
	/*
	 * Lee el elmento de la listbox seleccionado
	 */
	public String getSimpleDropdown(WebElement element) {
		Select field = new Select(element);
		String a = field.getFirstSelectedOption().getText();
		return a;		
	}

	/*
	 * Selecciona un elemento de una listbox con las flechas segun el valor
	 */
	public void setElementFromList(WebElement element, String value, WebElement arrow) {
		Select field = new Select(element);
		field.selectByVisibleText(value);
		arrow.click();
	}

	/*
	 * Selecciona un elemento de una lista de array segun el texto 
	 */
	public WebElement getElementFromList(List<WebElement> elements, String field) {
		List<Integer> a = new ArrayList<>();
		Integer i = 0;
		for(WebElement e : elements) {
			if(e.getText().equals(field)) {
				a.add(i);
			}
			i++;
		}
		return elements.get(a.get(0));
	}

	/*
	 * Setea el estado de un checkbox
	 */
	public void seteaCheckbox(WebElement elemento, boolean estado) {
		if(estado) {
			if(!elemento.isSelected())
				elemento.click();}
		else {
			if(elemento.isSelected())
				elemento.click();
		}	
	}

	/*
	 * Scrollea hasta el elemento y lo clickea
	 */
	public void scrollandclick(WebElement element) {	
		((JavascriptExecutor)driver).executeScript("window.scrollTo(0,"+element.getLocation().y+")");
		element.click();
	}

	/*
	 * Selecciona un elemento por una clase y texto y hace click
	 */
	public void buscaClaseYTexto_Click(String claseparaccsselector, String texto) {
		List<WebElement> listado = driver.findElements(By.cssSelector(claseparaccsselector));

		for(WebElement e:listado) {					

			if(e.getText().toLowerCase().equals(texto.toLowerCase())) {
				e.click();
				break;					
			}

		}		
	}

	/*
	 * En una grilla (NG) clickea en Modificar o Eliminar del registro del parametro Nombre
	 */
	public void base__buscaEnGrillaNgTextoYejecutaAccion(WebDriver driver2,String texto, String tipo, String accion, int cant_columnas) //*tipo=tramite/cliente *accion=eliminar/editar   *el valor cant_columnas = columnas de datos (no incluye botones) 
	{

		List<WebElement> listado = driver2.findElements(By.cssSelector(".ng-binding"));
		System.out.println(listado.size());

		int i = 0;
		Boolean existe = false;
		for(WebElement e:listado) {
			System.out.println(e.getText());
			if(e.getText().contains(texto)){	
				existe = true;
				break;
			}i++;
		}	

		if(existe) {
			int fila = (i/cant_columnas)+1;
			System.out.println("Fila: "+fila);
			String ruta_xpath="";

			if(tipo.equals("Tramite")) {

				if(accion.toLowerCase().equals("eliminar")) {
					ruta_xpath = "//*[@id='tablaTramites']/tbody/tr["+ String.valueOf(fila)  +"]/td[" + String.valueOf(cant_columnas+2)+ "]/a[1]";
					System.out.println(ruta_xpath);
					driver2.findElement(By.xpath(ruta_xpath)).click();

					//confirmo el eliminar
					esperaX(2000);
					//driver.findElement(By.id("mjeDesactivarBtnConfirmar")).click();//EL BOTON CONFIRMAR ES DISTINTO ID EN ELIMINAR TRAMITE DE PAGO ELECTRONICO Y LA DE TRAMITEABM, x eso no uso el ID
					driver2.findElement(By.cssSelector(".btn.btn-sm.btn-success")).click();

				}
				if(accion.toLowerCase().equals("editar")) {
					ruta_xpath = "//*[@id='tablaTramites']/tbody/tr["+ String.valueOf(fila)  +"]/td[" + String.valueOf(cant_columnas+1) + "]/a[1]";

					//click en boton EDITAR
					driver2.findElement(By.xpath(ruta_xpath)).click();
				}

				if(accion.toLowerCase().equals("reactivar")) {
					ruta_xpath = "//*[@id='tablaTramites']/tbody/tr["+ String.valueOf(fila)  +"]/td[" + String.valueOf(cant_columnas+2)+ "]/a[2]";
					System.out.println(ruta_xpath);
					driver2.findElement(By.xpath(ruta_xpath)).click();

					//confirmo el eliminar
					esperaX(2000);
					//driver.findElement(By.id("mjeDesactivarBtnConfirmar")).click();//EL BOTON CONFIRMAR ES DISTINTO ID EN ELIMINAR TRAMITE DE PAGO ELECTRONICO Y LA DE TRAMITEABM, x eso no uso el ID
					driver2.findElement(By.cssSelector(".btn.btn-sm.btn-success")).click();

				}				

			}
			if(tipo.equals("Cliente")) {
				if(accion.toLowerCase().equals("eliminar")) {
					ruta_xpath = "//*[@id='tablaClientes']/tbody/tr["+ String.valueOf(fila)  +"]/td[" +  String.valueOf(cant_columnas+2) + "]/a[1]";	
					driver2.findElement(By.xpath(ruta_xpath)).click();

					//confirmo el eliminar
					esperaX(2000);
					Alert alert = driver2.switchTo().alert();
					alert.accept(); 
					esperaX(2000);
					driver2.findElement(By.cssSelector(".btn.btn-sm.btn-default")).click();
				}
				if(accion.toLowerCase().equals("editar")) {
					ruta_xpath = "//*[@id='tablaClientes']/tbody/tr["+ String.valueOf(fila)  +"]/td[" +  String.valueOf(cant_columnas+1) + "]/a[1]";

					//click en boton EDITAR
					driver2.findElement(By.xpath(ruta_xpath)).click();
				}
			}
		}
		else {
			System.out.println("No hay "+ tipo + "  para " + accion);
			esperaX(2000);
		}
	}

	/*
	 * Imprime los estados de un elemento, ciclicamente
	 */
	public void ChequeaPropiedadElemento(WebElement elemento) {
		for(int i=0;i<30;i++) {
			if(elemento.isDisplayed())
				System.out.println("es displayed");
			if(elemento.isEnabled())
				System.out.println("es enabled");
			if(elemento.isSelected())
				System.out.println("es selected");
			esperaX(2000);}
	}

	/*
	 * Muestra un popup
	 */
	public void popup(String mensaje) { 
		JOptionPane.showMessageDialog(null, mensaje);
	}

	/*
		/*
	 * ESPERA A QUE NO SE VISUALIZE LA PANTALLA BLOQUEANTE CON MENSAJE "ESPERE, POR FAVOR"
	 */
	public void base__esperaDesaparezcaPantallaBloqueo(WebDriver driver, int ciclos, int espera)  {
		int cantidad = driver.findElements(By.id("esperador")).size();
		int cont =0;
		//System.out.println("cantidad:" + cantidad);
		if(cantidad != 0) {
			for(int i=0;i<ciclos;i++) {
				if(driver.findElement(By.id("esperador")).getAttribute("style").equals("display: none;"))
				{System.out.println("esta en none");
					cont++;
					if(cont == 3)
						break;
				}
				if(driver.findElement(By.id("esperador")).getAttribute("style").equals("display: block;"))
					{System.out.println("esta en block");	
					cont = 0;}
				esperaX(espera);				
			}
		}
		else
		{System.out.println("No habia ventana de bloqueo");}
		esperaX(1000);
	}

	/*
	 * GENERA UN RANDOM ENTRE EL MIN Y EL MAX
	 */
	public static String random(int min, int max) {
		 int valorEntero = (int) Math.floor(Math.random()*(max-min+1)+min); 
			return String.valueOf(valorEntero);
	}
	
	/*
	 * FORMATEA UN CODIGO DE CONCEPTO, AGREGA LOS 0 SI ES <10
	 */
	public String base__formateaConceptoConCero(String concepto_antes)
	//FIXME solo anda para x.x.x hasta xx.xx.xx
	{
		String concepto_despues="";
		String dupla1 ="";
		String dupla2 ="";
		String dupla3 ="";
		int i=0;		
		int longitud = concepto_antes.length();		

		while(concepto_antes.charAt(i) != '.') {			
			dupla1 = dupla1+ concepto_antes.substring(i,i+1);		
			i++;
		}i++;

		while(concepto_antes.charAt(i) != '.') {			
			dupla2 = dupla2+ concepto_antes.substring(i,i+1);		
			i++;
		}i++;

		while(concepto_antes.charAt(i) != '.') {			
			dupla3 = dupla3+ concepto_antes.substring(i,i+1);		
			i++;
			if(i==longitud)
				break;
		}

		//-----------armo con las duplas

		if(dupla1.length()==1)
			concepto_despues += "0"+dupla1+".";
		else	
			concepto_despues+=dupla1+".";

		if(dupla2.length()==1)
			concepto_despues+="0"+dupla2+".";
		else	
			concepto_despues+=dupla2+".";

		if(dupla3.length()==1)
			concepto_despues+="0"+dupla3;
		else	
			concepto_despues+=dupla3;		

		return(concepto_despues);				 
	}

	/*
	 * FORMATEA UN CODIGO DE CONCEPTO, QUITA LOS 0 SI ES <10
	 */
	public String base__formateaConceptoSinCero(String concepto_antes)
	//FIXME solo anda para x.x.x hasta xx.xx.xx
	{
		String concepto_despues="";
		String dupla1 ="";
		String dupla2 ="";
		String dupla3 ="";
		int i=0;		
		int longitud = concepto_antes.length();		

		while(concepto_antes.charAt(i) != '.') {			
			dupla1 = dupla1+ concepto_antes.substring(i,i+1);		
			i++;
		}i++;

		while(concepto_antes.charAt(i) != '.') {			
			dupla2 = dupla2+ concepto_antes.substring(i,i+1);		
			i++;
		}i++;

		while(concepto_antes.charAt(i) != '.') {			
			dupla3 = dupla3+ concepto_antes.substring(i,i+1);		
			i++;
			if(i==longitud)
				break;
		}
		
		//-----------armo con las duplas

		if(dupla1.charAt(0)== '0')
			concepto_despues += dupla1.substring(1,2)+".";
		else	
			concepto_despues+= dupla1 +".";

		if(dupla2.charAt(0)== '0')
			concepto_despues += dupla2.substring(1,2)+".";
		else	
			concepto_despues+= dupla2 +".";

		if(dupla3.charAt(0)== '0')
			concepto_despues += dupla3.substring(1,2);
		else	
			concepto_despues+= dupla3;		

		return(concepto_despues);				 
	}


	/*
	 * DEVUELVE LA FECHA EN FORMATO DD/MM/YYYY
	 */	
	public String base__fechaActual() {
		Calendar hoy = Calendar.getInstance();
		//hoy.add(Calendar.DATE, 3);
		//hoy.add(Calendar.MONTH, 2);
		SimpleDateFormat format1 = new SimpleDateFormat("dd/MM/yyyy");
		String formatted = format1.format(hoy.getTime());
		//System.out.println(hoy.getTime());
		return (formatted);		
	}
	
	/*
	 * DEVUELVE SI HAY UN ALERT
	 */
	public boolean isAlertPresent(WebDriver driver) 
	{ 
	    try 
	    { 
	        driver.switchTo().alert(); 
	        return true; 
	    }  
	    catch (NoAlertPresentException Ex) 
	    { 
	        return false; 
	    }  
	}  
}