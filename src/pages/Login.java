package pages;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;


public class Login extends BasePage{
	final WebDriver driver;

	public Login(WebDriver driver){
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	/*
	 ************WEBELEMENTS************* 
	 */

	@FindBy (how = How.ID, using = "txtUserName")
	private WebElement user;

	@FindBy (how = How.ID, using = "txtPassword")
	private WebElement pswd;

	@FindBy (how = How.XPATH, using = "//button[@type='submit']")
	private WebElement btnLogin;


	@FindBy (how = How.XPATH, using = "//input[@type='submit']")
	private WebElement btnLoginst;
	//TRAMITEABM

	@FindBy (how = How.XPATH, using = "//input[@type='text']")
	private WebElement userAbm;

	@FindBy (how = How.XPATH, using = "//input[@type='password']")
	private WebElement pswdAbm;


	//SirTools
	@FindBy (how = How.ID, using = "User")
	private WebElement userST;

	@FindBy (how = How.ID, using = "Pass")
	private WebElement pswdST;
	/*
	 *********************METODOS************* 	 
	 */

	public void logearBO(String usuario, String paswd) {
		user.sendKeys(usuario);
		pswd.sendKeys(paswd);
		btnLogin.click();
	}


	public void logearBUI(String usuario, String paswd) {
		user.sendKeys(usuario);
		pswd.sendKeys(paswd);
		btnLogin.click();	
	}


	public void logearTramiteAbm(String usuario, String paswd) {
		userAbm.sendKeys(usuario);
		pswdAbm.sendKeys(paswd);
		btnLogin.click();	

	}

	public void logearST(String usuario, String paswd) {
		userST.sendKeys(usuario);
		pswdST.sendKeys(paswd);
		btnLoginst.click();	
		

	}
	public void logearPagosAbm(String usuario, String paswd) {
		userAbm.sendKeys(usuario);
		pswdAbm.sendKeys(paswd);
		btnLogin.click();		

	}

}

