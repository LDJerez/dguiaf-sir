package pages;

import static org.testng.Assert.assertTrue;

import java.util.List;

import javax.swing.JOptionPane;
import javax.swing.plaf.synth.SynthSpinnerUI;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import test.TestBase;

public class BUIPage extends BasePage {

	final WebDriver driver;
	public BUIPage(WebDriver driver){
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	public void espera(int espera) {
		try {Thread.sleep(espera);} catch (InterruptedException e) {e.printStackTrace();}
	}

	public void WaitForElement(By element, int tiempo) {
		WebDriverWait wait;
		wait = new WebDriverWait(driver, tiempo);
		wait.until(ExpectedConditions.elementToBeClickable(element));
	}
	
	
	public void IngresoAmbiente() {
		TestBase TB = new TestBase(driver);
		TB.PortalBUI();
		Login lg = new Login(driver);
		lg.logearBUI("jbauchi", "Ab123456");
	}
	
	String nroboleta;

	/*
	 **********************WEBELEMENTS***************
	 */ 


	@FindBy(how = How.CSS,using = ".k-dropdown")
	private List<WebElement> Drops;

	@FindBy(how = How.CSS,using = ".k-input")
	private List<WebElement> DropText;

	@FindBy(how = How.ID,using = "Contribuyente_Documento")
	private WebElement Documento;

	@FindBy(how = How.ID,using = "Contribuyente_Nombre")
	private WebElement Nombre;

	@FindBy(how = How.ID,using = "Contribuyente_Email")
	private WebElement Email;

	@FindBy(how = How.ID,using = "Contribuyente_Direccion")
	private WebElement Direccion;

	@FindBy(how = How.ID,using = "Contribuyente_Piso")
	private WebElement Piso;

	@FindBy(how = How.ID,using = "Contribuyente_Departamento")
	private WebElement Departamento;

	@FindBy(how = How.ID,using = "Contribuyente_Localidad")
	private WebElement Localidad;

	@FindBy(how = How.ID,using = "Contribuyente_CodigoPostal")
	private WebElement CodigoPostal;

	@FindBy(how = How.ID, using = "addConcepto")
	private WebElement BtnAddConcepto;

	//conceptos tab	
	@FindBy(how = How.CSS,using =".txtCodigo.k-input")
	private WebElement ConceptCod;

	@FindBy(how=How.ID,using="txtQty")
	private WebElement ConceptCant;

	@FindBy(how=How.ID,using="txtValor")
	private WebElement ConceptValor;

	@FindBy(how=How.ID,using="Add")
	private WebElement ConceptAdd;

	@FindBy(how=How.ID,using="Cancel")
	private WebElement ConceptCancel;

	@FindBy(how=How.ID,using="btnGenerar")
	private WebElement btnGenerar;


	/*
	 ***************METODOS****************
	 */


	public void SeleccionarDependencia(String Dependencia) {
		espera(500);
		Drops.get(0).click();
		espera(500);
		Drops.get(0).sendKeys(Dependencia);
		Drops.get(0).sendKeys(Keys.ENTER);
		String depe = DropText.get(0).getText();
		Assert.assertEquals(depe.toLowerCase(), Dependencia.toLowerCase());
	}

	public void SeleccionarTipoPersona(String tipo) {
		espera(500);
		Drops.get(1).click();
		espera(500);
		Drops.get(1).sendKeys(tipo);
		Drops.get(1).sendKeys(Keys.ENTER);
		String type = DropText.get(2).getText();
		Assert.assertEquals(type.toLowerCase(), tipo.toLowerCase());
	}

	public void SeleccionarTipoDoc(String tipo) {
		espera(500);
		Drops.get(2).click();
		espera(500);
		Drops.get(2).sendKeys(tipo);
		Drops.get(2).sendKeys(Keys.ENTER);
		String type = DropText.get(3).getText();
		Assert.assertEquals(type.toLowerCase(), tipo.toLowerCase());
	}

	public void CompletarDatos(String persona, String doc) {
		espera(500);
		SeleccionarTipoPersona(persona);
		SeleccionarTipoDoc(doc);
		Documento.sendKeys("1000000");
		Nombre.sendKeys("Test Automatizado");
		Email.sendKeys("correo@prueba.com");
		Direccion.sendKeys("calle_prueba");
		Piso.sendKeys("");
		Departamento.sendKeys("");
		Localidad.sendKeys("");
		CodigoPostal.sendKeys("1234");
	}

	public void AgregarConceptos(String cod, String cant) {
		espera(500);
		BtnAddConcepto.click();
		espera(1000);
		ConceptCod.sendKeys(cod.substring(0, 3));
		espera(1000);
		ConceptCod.sendKeys(cod.substring(3,5));
		espera(1000);
		ConceptCod.sendKeys(cod.substring(5,cod.length()));
		espera(1000);
		ConceptCod.sendKeys(Keys.ENTER);
		//agrega cantidad

		//ConceptCant.clear();
		//ConceptCant.sendKeys(cant);
		espera(500);
		ConceptAdd.click();
	}
	
	public void AgregarConceptosValor(String cod, String cant,String valor) {
		espera(500);
		BtnAddConcepto.click();
		espera(1000);
		ConceptCod.sendKeys(cod.substring(0, 3));
		espera(1000);
		ConceptCod.sendKeys(cod.substring(3,5));
		espera(1000);
		ConceptCod.sendKeys(cod.substring(5,cod.length()));
		espera(1000);
		ConceptCod.sendKeys(Keys.ENTER);
		//agrega cantidad

		//ConceptCant.clear();
		//ConceptCant.sendKeys(cant);
		ConceptValor.clear();
		ConceptValor.sendKeys(valor);
		espera(500);
		ConceptAdd.click();
	}
	public void AgregarConceptos(String cod) {
		espera(500);
		BtnAddConcepto.click();
		espera(1000);
		ConceptCod.sendKeys(cod.substring(0, 3));
		espera(1000);
		ConceptCod.sendKeys(cod.substring(3,5));
		espera(1000);
		ConceptCod.sendKeys(cod.substring(5,cod.length()));
		espera(1000);
		ConceptCod.sendKeys(Keys.ENTER);
		espera(1000);
		ConceptAdd.click();
	}

	public void GenerarBUI() {
		((JavascriptExecutor)driver).executeScript("window.scrollTo(0,"+btnGenerar.getLocation().y+")");
		espera(500);
		btnGenerar.click();
	}

	public boolean validBUI() {
		boolean a;
		espera(10000);
		WaitForElement(By.id("msgConfirm"), 10000000);
		nroboleta = driver.findElement(By.id("msgConfirm")).getText();
		a= nroboleta.contains("grabada con exito.");
		System.out.println(nroboleta);
		System.out.println(a);
		driver.findElement(By.id("btnConfirm")).click();
		return a;
	}

	public String GuardarNroBoleta() {
		System.out.println(nroboleta);
		String boleta =nroboleta.replaceAll(" ", "");
		System.out.println(boleta);
		boleta =boleta.substring(10, 23);
		System.out.println(boleta);
		return boleta;


	}

	public String GenerarBoletaPagoVisa() {//EN REALIDAD ES DE TODOS LOS MP XQ AHORA USA EL CONCEPTO 01.02.07
		TestBase TB = new TestBase(driver);
		TB.PortalBUI();
		Login lg = new Login(driver);
		lg.logearBUI("jbauchi", "Ab123456");
		String persona = "fisica";
		String doc = "dni";
		String cod = "01.02.07";
		String cant ="1";
		SeleccionarDependencia("Registro Civil");
		CompletarDatos(persona,doc);
		AgregarConceptos(cod);
		GenerarBUI();
		Assert.assertTrue(validBUI());
		nroboleta = GuardarNroBoleta();
	//	driver.findElement(By.id("btnConfirm")).click();
		return 	nroboleta;
	}

	public void seleccionarDependenciaItem(String dependencia) {
		espera(500);
		Drops.get(0).click();
		espera(500);
		Drops.get(0).sendKeys(dependencia);
		Drops.get(0).sendKeys(Keys.ENTER);
	}

	public void seleccionarItem(String item) {
		/*
		 * BUI/ADMIN/ITEM ATRIBUTOS/ITEM el valor item tiene que estar formateado con '0' si es menor a 9 cada dupla.
		 */
		WebElement a = driver.findElement(By.id("ItemID"));
		String item_formateado = base__formateaConceptoConCero(item);
		a.sendKeys(item_formateado);

		espera(3000);
		a.sendKeys(Keys.ENTER);
	}

	public void agregar_atributo_click() {
		driver.findElement(By.id("addButton")).click();		
	}

	public void agregar_atributo2_click() {
		driver.findElement(By.id("Add")).click();		
	}

	public void completar_datos_para_agregar(String vigencia, boolean valorFijo, boolean cantidadFija, boolean admiteReglas) {

		WebElement valor_Fijo = driver.findElement(By.id("TieneValorFijo"));		
		WebElement Cantidad_fija = driver.findElement(By.id("TieneCantidadFija"));		
		WebElement admite_reglas = driver.findElement(By.id("AdmiteReglas"));

		driver.findElement(By.id("Vigencia")).clear();
		driver.findElement(By.id("Vigencia")).sendKeys(vigencia);

		seteaCheckbox(valor_Fijo,valorFijo);
		seteaCheckbox(Cantidad_fija,cantidadFija);
		seteaCheckbox(admite_reglas,admiteReglas);
	}

	/*
	 * Valida que exista Alert con mensaje y cierra alert 
	 */
	public boolean validaMensajeAlertaYCierra(String mensaje) {//"Grabado Exitoso."
		boolean a = false;
		WaitForElement(By.id("divAlert"), 60);
		if(driver.findElement(By.id("divAlert")).getText().toLowerCase().equals(mensaje.toLowerCase())) {
			a = true;
			WaitForElement((By.cssSelector(".ui-button.ui-widget.ui-state-default.ui-corner-all.ui-button-icon-only.ui-dialog-titlebar-close")), 60);
			driver.findElement(By.cssSelector(".ui-button.ui-widget.ui-state-default.ui-corner-all.ui-button-icon-only.ui-dialog-titlebar-close")).click();

		}
		return a;	
	}


	public void atributosDelItem_agregar(String p_dependencia, String p_item, String p_vigencia, Boolean p_valor_fijo, Boolean p_cantidad_fija, Boolean p_admite_reglas) {
		String dependencia = p_dependencia;
		String item = p_item;
		String vigencia = p_vigencia;
		Boolean valor_fijo = p_valor_fijo;
		Boolean cantidad_fija = p_cantidad_fija;
		Boolean admite_reglas = p_admite_reglas;		

		//ir a menu Admin/Atributos del item
		driver.get("http://buisir-testing.buenosaires.gob.ar/WEB/ABM/ItemAtributos");

		espera(2000);		
		seleccionarDependenciaItem(dependencia);

		espera(2000);
		seleccionarItem(item);

		agregar_atributo_click();

		espera(5000);		
		completar_datos_para_agregar(vigencia,valor_fijo,cantidad_fija,admite_reglas);

		agregar_atributo2_click();				

		espera(5000);
	}
	
	public String Generar_Concepto_RC_PE_VV() {
		BackOfficePage BO = new BackOfficePage(driver);
		String periodo_tarifaria = "2018";
		String fecha_publicacion = base__fechaActual();
		String Cod1 = "01";
		String Cod2 = BasePage.random(50,60);
		String Cod3 = BasePage.random(1,99);
		String Descripcion = "concepto_automatizacion_" + Cod1 + "_" + Cod2 + "_" + Cod3; 
		String Renglon1 = "renglon1_concepto_automatizacion_" + Cod1 + "_" + Cod2 + "_" + Cod3;
		String Agencia_asignada = "01-Dir. Gral del Registro Civil y Capacidad de las Personas";
		String valor = "1";
		boolean es_redondeable = true;
		boolean tiene_precio_fijo = false;
		boolean cantidad_variable = true;
		boolean es_devolucion = true;
		String codigo = Cod1+"."+Cod2+"."+Cod3;
		BO.IngresoAmbiente();		
		BO.crearConceptoAprobarlo(periodo_tarifaria, fecha_publicacion, Cod1, Cod2, Cod3, Descripcion, Renglon1, Agencia_asignada, valor, es_redondeable, tiene_precio_fijo, cantidad_variable, es_devolucion,codigo);
		String dep1 = "Registro Civil";
		String dep2 = "Pago Electrónico";
		BO.asignarDependencia_Concepto(codigo, dep1, dep2);
		Assert.assertTrue(BO.asignacionDependenciaExitosa());
		BUIPage BUI = new BUIPage(driver);	
		BUI.IngresoAmbiente();
		BUI.atributosDelItem_agregar(dep1, codigo, "30", tiene_precio_fijo, !cantidad_variable, false);
		return codigo;
		

	}
}
