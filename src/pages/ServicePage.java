package pages;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.InputStreamEntity;
import org.apache.http.impl.client.HttpClientBuilder;


public class ServicePage {
	
	
	public String GetToken(String request, String result) throws ClientProtocolException, IOException {
		String endpoint = "http://10.73.100.48:2486/PagosService.asmx?WSDL";
		File requestFile = new File(System.getProperty("user.dir")+"//Requests//"+request+".xml");	
		HttpClient cilent = HttpClientBuilder.create().build();
		//HttpGet post = new HttpGet(endpoint);
		HttpPut post = new HttpPut(endpoint);
		post.setEntity(new InputStreamEntity(new FileInputStream(requestFile)));
		post.setHeader("Content-type","text/xml");
		HttpResponse response = cilent.execute(post);
		System.out.println(response.getStatusLine().getStatusCode());
		BufferedReader br = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
		String line = "";
		StringBuffer sb = new StringBuffer();
		while((line=br.readLine())!=null)
		{sb.append(line);}
		PrintWriter pw = new PrintWriter(System.getProperty("user.dir")+"//Response//"+result+".xml");
		pw.write(sb.toString());
		int a = (sb.length()-64);
		String dato = sb.toString().substring(278,a );
		pw.close();
		pw.flush();
	return dato;
}
	
	
	
	public String RequestToken(String request, String result) throws ClientProtocolException, IOException {
		String endpoint  = "http://10.73.100.48:2840/SIRAPI/SIR.API.PaymentService.svc?WSDL";
		File requestFile = new File(System.getProperty("user.dir")+"//Requests//"+request+".xml");	
		HttpClient cilent = HttpClientBuilder.create().build();
		HttpPost post = new HttpPost(endpoint);
		post.setEntity(new InputStreamEntity(new FileInputStream(requestFile)));
		post.setHeader("Content-type","text/xml; charset=utf-8");
		HttpResponse response = cilent.execute(post);
		//el HttpResponse esta dando error 500
		System.out.println(response.getStatusLine().getStatusCode());
		BufferedReader br = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
		String line = "";
		StringBuffer sb = new StringBuffer();
		while((line=br.readLine())!=null)
		{sb.append(line);}
		PrintWriter pw = new PrintWriter(System.getProperty("user.dir")+"//Response//"+result+".xml");
		pw.write(sb.toString());
		int a = (sb.length()-64);
		String dato = sb.toString().substring(240,a );
		pw.close();
		pw.flush();
	return dato;
}
	
		
	public String ConvertOracleFromGUID(String guid){
		 return guid.replaceAll("(.{8})(.{4})(.{4})(.{4})(.{12})", "$1$2$3$4$5")
				 .replaceAll("(.{2})(.{2})(.{2})(.{2}).(.{2})(.{2}).(.{2})(.{2})(.{18})", "$4$3$2$1$6$5$8$7$9");
	}
}
