package pages;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Query_Db_Sirt {
	
	
	public String ConectarYRealizarQuery(String usuario, String password, String query) throws ClassNotFoundException, SQLException {
	
	
	//step1 load the driver class  
	Class.forName("oracle.jdbc.driver.OracleDriver");  

	
	//step2 create  the connection object  
	Connection con=DriverManager.getConnection(  
	"jdbc:oracle:thin:@ORADB11:1521:SIRT",usuario,password);  
	
	
	//step3 create the statement object  
	Statement stmt=con.createStatement();  


	//step4 execute query  
	ResultSet rs=stmt.executeQuery(query);  
	String ret = "";
	while(rs.next())  
		ret = (rs.getString(1)); 
	
	//step5 close the connection object  
		con.close();  
		
	return ret;
	}	
	
}
