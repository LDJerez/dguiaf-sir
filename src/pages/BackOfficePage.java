package pages;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.net.DefaultNetworkInterfaceProvider;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import test.TestBase;



public class BackOfficePage extends BasePage{

	final WebDriver driver;
	


	public BackOfficePage(WebDriver driver){
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	public void IngresoAmbiente() {
		
		TestBase TB = new TestBase(driver);
		TB.PortalBackOffice();
		espera(3000);
		Login lg = new Login(driver);
		lg.logearBO("jbauchi","Ab123456");		
	}

	/*
	 ******PASAR A BASEPAGE
	 */
	public void espera(long milisec) {
		try {Thread.sleep(milisec);} catch (InterruptedException e) {e.printStackTrace();}		
	}


	public void WaitForElement(By element, int tiempo) {
		WebDriverWait wait;
		wait = new WebDriverWait(driver, tiempo);
		wait.until(ExpectedConditions.elementToBeClickable(element));
	}

	public void WaitForElement(WebElement element, int tiempo) {
		WebDriverWait wait;
		wait = new WebDriverWait(driver, tiempo);
		wait.until(ExpectedConditions.elementToBeClickable(element));
	}




	/*
	 **********DEFINICION WEBELEMENTS**********
	 */


	@FindBy (how = How.CSS, using = ".dropdown-toggle")
	private List<WebElement> menues;

	@FindBy (how = How.CSS, using = ".dropdown-menu")
	private List<WebElement> USUopcmenues;

	//Usuarios
	@FindBy (how = How.CLASS_NAME, using = "col-md-3")
	private WebElement USUnombre;

	@FindBy (how = How.ID, using = "C433d92450d184fe6a87121d68d4c102f")
	private WebElement USUlegajo;

	@FindBy (how = How.CSS, using = ".t-widget.t-dropdown.t-header")
	private List <WebElement> USUcombox;

	@FindBy (how = How.ID, using = "Cebf8b71ca1e2439c924d893242f139b0")
	private WebElement USUlogin;
	//botones

	@FindBy (how = How.CSS, using = ".t-button.t-grid-edit")
	private List <WebElement> USUbtnbuscarylimpiar;

	@FindBy (how = How.ID, using = "NewAction")
	private WebElement USUbtnnuevo;
	//nuevo usuario
	@FindBy (how = How.ID, using = "Nombre")
	private WebElement Nombre;

	@FindBy (how = How.ID, using = "Apellido")
	private WebElement Apellido;

	@FindBy (how = How.ID, using = "Login")
	private WebElement Login;

	@FindBy (how = How.ID, using = "EMail")
	private WebElement EMail;

	@FindBy (how = How.ID, using = "Sector")
	private WebElement Sector;

	@FindBy (how = How.ID, using = "Legajo")
	private WebElement Legajo;

	@FindBy (how = How.ID, using = "Dni")
	private WebElement Dni;
	//botones Usuario Nuevo
	@FindBy (how = How.ID, using = "btnSave")
	private WebElement crear;

	@FindBy (how = How.ID, using = "btnBack")
	private WebElement volver;




	//-----CONCEPTOS------------------------

	@FindBy (how = How.XPATH, using = "//a[contains(.,'Limpiar')]")
	private WebElement concepto_btn_limpiar;

	@FindBy (how = How.XPATH, using = "//a[contains(.,'Buscar')]")
	private WebElement concepto_btn_buscar;

	@FindBy (how = How.ID, using = "NewAction")
	private WebElement concepto_btn_nuevo;

	@FindBy (how = How.ID, using = "Codigo")
	private WebElement concepto_txt_codigo;

	@FindBy (how = How.ID, using = "Descripcion")
	private WebElement concepto_txt_descripcion;

	@FindBy (how = How.ID, using = "IncluyeAprobados")
	private WebElement concepto_chk_incluyeAprobados;

	@FindBy (how = How.ID, using = "IncluyeModificados")
	private WebElement concepto_chk_incluyeModificados;

	@FindBy (how = How.ID, using = "IncluyeRechazados")
	private WebElement concepto_chk_incluyeRechazados;

	@FindBy (how = How.ID, using = "IncluyePendientes")
	private WebElement concepto_chk_incluyePendientes;



	@FindBy (how = How.XPATH, using = "//a[contains(.,'Asignar Dependencias')]")
	private WebElement concepto_btn_asignar_dependencias;

	//-----NUEVO-CONCEPTO--------------------

	@FindBy (how = How.CSS, using = ".t-widget.t-dropdown.t-header") 
	private List <WebElement> conceptos_PERIODOcombox;

	@FindBy (how = How.ID, using = "NormativaFecha")
	private WebElement conceptos_cldNormativaFecha;

	@FindBy (how = How.ID, using = "Cod1")
	private WebElement conceptos_txtCod1;

	@FindBy (how = How.ID, using = "Cod2")
	private WebElement conceptos_txtCod2;

	@FindBy (how = How.ID, using = "Cod3")
	private WebElement conceptos_txtCod3;

	@FindBy (how = How.ID, using = "Descripcion")
	private WebElement conceptos_txtDescripcion;

	@FindBy (how = How.ID, using = "Renglon1")
	private WebElement conceptos_txtRenglon1;

	@FindBy (how = How.ID, using = "EsRedondeable")
	private WebElement conceptos_chk_esRedondeable;

	@FindBy (how = How.ID, using = "EsPrecioFijo")
	private WebElement conceptos_chk_esPrecioFijo;

	@FindBy (how = How.ID, using = "EsCantidadVariable")
	private WebElement conceptos_chk_esCantidadVariable;

	@FindBy (how = How.ID, using = "EsDevolucion")
	private WebElement conceptos_chk_esDevolucion;

	@FindBy (how = How.ID, using = "btnSave")
	private WebElement concepto_btn_crear;

	@FindBy (how = How.ID, using = "btnBack")
	private WebElement concepto_btn_volver;

	@FindBy (how = How.ID, using = "btnSave")
	private WebElement concepto_btn_actualizar;

	//-------EDICION CUOTAS TARJETAS------------

	@FindBy (how = How.ID, using = "chk_2")
	private WebElement ediciontarjetas_chk_2cuotas;

	@FindBy (how = How.ID, using = "btnSave")
	private WebElement edicionTarjeta_btn_actualizar;

	@FindBy (how = How.ID, using = "btnBack")
	private WebElement edicionTarjeta_btn_volver;	

	//------------------------------------------
	//------------------------------------------


	public void menu(String menu, String opcion) {
		//elegir el titulo del menu
		for(WebElement e :menues) {
			if(e.getText().toLowerCase().contains(menu.toLowerCase())) {
				e.click();
				break;}}
		try {Thread.sleep(3000);} catch (InterruptedException e) {e.printStackTrace();}

		//elegimos la opcion del menu
		List<WebElement> opcmenues2 = USUopcmenues;
		for(WebElement e :opcmenues2) {	
			List<WebElement> opcmenues3 =e.findElements(By.tagName("li"));
			if(!e.getText().equals("")) {
				for(WebElement e2 :opcmenues3) {	

					if(e2.getText().toLowerCase().equals(opcion.toLowerCase())) {
						e2.click();
						break;}}
				break;}}
		try {Thread.sleep(3000);} catch (InterruptedException e) {e.printStackTrace();}
	}

	public void SeleccionarEnElListbox(WebElement list, String opcion) {
		espera(1000);
		list.click();
		espera(1000);
		List<WebElement> opc = driver.findElements(By.cssSelector(".t-item"));
		for(WebElement e : opc) {
			if(e.getText().toLowerCase().equals(opcion.toLowerCase())){
				e.click();
				break;}
		}
	}


	//-------------USUARIOS-------------------

	public boolean usuariocreado() {
		boolean a = false;
		WaitForElement(By.id("divAlert"), 50);
		if(driver.findElement(By.id("divAlert")).getText().toLowerCase().equals("Grabado Exitoso.".toLowerCase())) {
			a = true;		
			WaitForElement((By.cssSelector(".ui-button.ui-widget.ui-state-default.ui-corner-all.ui-button-icon-only.ui-dialog-titlebar-close")), 60);
			
			espera(1000);//FIXME SIN ESTA ESPERA, A VECES NO FUNCIONA BIEN, VER PORQUE
			driver.findElement(By.cssSelector(".ui-button.ui-widget.ui-state-default.ui-corner-all.ui-button-icon-only.ui-dialog-titlebar-close")).click();
							
		}
		return a;	
	}

	public void UsuariosDatosBscr(String nombretxt, String legajotxt, String dependencia, String logintxt, String modulo) {
		try {Thread.sleep(3000);} catch (InterruptedException e) {e.printStackTrace();}
		USUnombre.sendKeys(nombretxt);
		USUlegajo.sendKeys(legajotxt);
		SeleccionarEnElListbox(USUcombox.get(0), dependencia);
		USUlogin.sendKeys(logintxt);
		SeleccionarEnElListbox(USUcombox.get(1), modulo);
	}

	public void UsuarioNuevoCheks(String check) {
		List<WebElement> checks = driver.findElements(By.cssSelector(".col-md-2"));
		for(WebElement e : checks) {
			if(e.getText().toLowerCase().equals(check.toLowerCase())) {
				e.click();
				break;} }}

	public void UsuariosDatosBscr(String nombretxt) {
		try {Thread.sleep(5000);} catch (InterruptedException e) {e.printStackTrace();}
		USUnombre.findElement(By.tagName("input")).sendKeys(nombretxt);
	}

	public void UsuarioNuevoDatos(String nombretxt, String apellidotxt,String logintxt,String emailtxt,
			String sectortxt, String dependencia, String legajotxt, String dnitxt) {
		espera(3000);
		Nombre.sendKeys(nombretxt);
		Apellido.sendKeys(apellidotxt);
		Login.sendKeys(logintxt);
		EMail.sendKeys(emailtxt);
		Sector.sendKeys(sectortxt);
		SeleccionarEnElListbox(USUcombox.get(0), dependencia);
		espera(1000);
		Legajo.sendKeys(legajotxt);
		Dni.sendKeys(dnitxt);
	}

	public void UsuariosBtns(String accion) {
		espera(5000);
		if(accion.toLowerCase().equals("limpiar")) {
			USUbtnbuscarylimpiar.get(0).click();}
		if(accion.toLowerCase().equals("buscar")) {
			USUbtnbuscarylimpiar.get(1).click();}
		if(accion.toLowerCase().equals("nuevo")) {
			USUbtnnuevo.click();}
	}

	public void NuevoUsuarioBtns(String accion) {
		espera(5000);
		if(accion.toLowerCase().equals("crear")) {
			crear.click();}
		if(accion.toLowerCase().equals("volver")) {
			volver.click();}

	}

	public void ConceptoBtns(String accion) {
		if(accion.toLowerCase().equals("limpiar")) {
			WaitForElement(concepto_btn_limpiar,60);
			concepto_btn_limpiar.click();}
		if(accion.toLowerCase().equals("buscar")) {
			WaitForElement(concepto_btn_buscar,60);
			concepto_btn_buscar.click();}
		if(accion.toLowerCase().equals("nuevo")) {
			WaitForElement(concepto_btn_nuevo, 60);
			concepto_btn_nuevo.click();}
		if(accion.toLowerCase().equals("asignar dependencias")) {
			WaitForElement(concepto_btn_asignar_dependencias,60);
			concepto_btn_asignar_dependencias.click();}

	}


	/*
	 **********************METODOS***********************
	 */

	public String LegajoRandom() {
		String numero;
		int num1=111;
		int num2=9999;
		int numAleatorio=(int)Math.floor(Math.random()*(num1-num2)+num2);
		numero = String.valueOf(numAleatorio);
		return numero;
	}

	
	//------------CONCEPTOS-------------------


	public void NuevoConceptoBtns(String accion) {
		try {Thread.sleep(3000);} catch (InterruptedException e) {e.printStackTrace();}
		if(accion.toLowerCase().equals("crear")) {
			concepto_btn_crear.click();}
		if(accion.toLowerCase().equals("volver")) {
			concepto_btn_volver.click();}

	}

	public void scrollandclick(WebElement element) {	
		((JavascriptExecutor)driver).executeScript("window.scrollTo(0,"+element.getLocation().y+")");
		element.click();
	}

	public void EdicionConceptoBtns(String accion) {
		try {Thread.sleep(3000);} catch (InterruptedException e) {e.printStackTrace();}
		if(accion.toLowerCase().equals("actualizar")) {
			concepto_btn_actualizar.click();}
		if(accion.toLowerCase().equals("volver")) {
			concepto_btn_volver.click();}

	}

	public boolean validacionConceptoCreado() {
		boolean a = false;
		WaitForElement(By.id("divAlert"), 50);
		if(driver.findElement(By.id("divAlert")).getText().equals("Grabado Exitoso.")) {
			if(driver.findElement(By.id("divAlert")).getText().toLowerCase().equals("Grabado Exitoso.".toLowerCase())) {
				a = true;
				espera(3000);
				driver.findElement(By.cssSelector(".ui-button-icon-primary.ui-icon.ui-icon-closethick")).click();
			}

		}	return a;		
	}

	public boolean validacionConceptoModificado() {
		boolean a = false;
		WaitForElement(By.id("divAlert"), 50);
		if(driver.findElement(By.id("divAlert")).getText().equals("Grabado Exitoso.")) {
			if(driver.findElement(By.id("divAlert")).getText().toLowerCase().equals("Grabado Exitoso.".toLowerCase())) {
				a = true;
				espera(3000);
				driver.findElement(By.cssSelector(".ui-button-icon-primary.ui-icon.ui-icon-closethick")).click();
			}

		}	return a;		
	}

	public void ConceptoNuevosDatos(String periodo_tarifaria, String fecha_publicacion, String cod1, String cod2,
			String cod3, String descripcion, String renglon1, String agencia_asignada, String valor,
			boolean es_redondeable, boolean tiene_precio_fijo, boolean cantidad_variable, boolean es_devolucion) {

		WaitForElement(By.cssSelector(".t-widget.t-dropdown.t-header"), 60);
		SeleccionarEnElListbox(conceptos_PERIODOcombox.get(0), periodo_tarifaria);

		conceptos_cldNormativaFecha.sendKeys(fecha_publicacion);

		conceptos_txtCod1.sendKeys(cod1);
		conceptos_txtCod2.sendKeys(cod2);
		conceptos_txtCod3.sendKeys(cod3);

		conceptos_txtDescripcion.sendKeys(descripcion);

		conceptos_txtRenglon1.sendKeys(renglon1);

		//NO SELECCIONA CORRECTAMENTE
		SeleccionarEnElListbox(conceptos_PERIODOcombox.get(4),"02-Dir. Gral de Cementerios" );//agencia_asignada);

		driver.findElement(By.id("Valor")).sendKeys(valor);

		if(es_redondeable)
			conceptos_chk_esRedondeable.click();
		if(tiene_precio_fijo)
			conceptos_chk_esPrecioFijo.click();
		if(cantidad_variable)
			conceptos_chk_esCantidadVariable.click();
		if(es_devolucion)
			conceptos_chk_esDevolucion.click();
	}

	public void editarUsuario() {//FIXME mejorarlo para que reciba por parametro el usuario a editar
		espera(3000);
		((JavascriptExecutor)driver).executeScript("window.scrollTo(0,"+driver.findElement(By.cssSelector(".t-pager.t-reset")).getLocation().y+")");
		espera(3000);
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", driver.findElement(By.xpath("//*[text() ='Editar']")));
		espera(3000);
		List<WebElement> btneditar = driver.findElements(By.xpath("//*[text() ='Editar']"));
		btneditar.get(1).click();
		espera(3000);
		Nombre.sendKeys("editado");
		espera(3000);
		scrollandclick(driver.findElement(By.id("btnSave")));
		System.out.println("Se edito usuario:");
	}

	public boolean usuarioeditado() {
		boolean a = false;
		WaitForElement(By.id("divAlert"), 50);
		if(driver.findElement(By.id("divAlert")).getText().toLowerCase().equals("Grabado Exitoso.".toLowerCase())) {
			a = true;
		}
		return a;		
	}	

	public void buscarConcepto(String codigo, String descripcion, String periodo_tarifario, String estado, String agencia,boolean incluye_aprobados, boolean incluye_modificados, boolean incluye_pendientes, boolean incluye_rechazados, String ordenamiento) {
	
		WaitForElement(By.id("Codigo"), 60);
		concepto_txt_codigo.clear();
		espera(2000); //es necesaria porque a veces borra y no llega a escribir
		concepto_txt_codigo.sendKeys(codigo);

		concepto_txt_descripcion.clear();
		espera(1000); //es necesaria porque a veces borra y no llega a escribir
		concepto_txt_descripcion.sendKeys(descripcion);

		seteaCheckbox(concepto_chk_incluyeAprobados,incluye_aprobados);
		seteaCheckbox(concepto_chk_incluyeModificados,incluye_modificados);
		seteaCheckbox(concepto_chk_incluyePendientes,incluye_pendientes);
		seteaCheckbox(concepto_chk_incluyeRechazados,incluye_rechazados);		
	}

	public void editarRenglon1(String descripcion) {
		WaitForElement(By.id("Renglon1"), 60);
		conceptos_txtRenglon1.sendKeys(descripcion);
	}

	//-------------EDICION TARJETA--------------

	public String buscaEnGrillaViejaTextoEnColumnaYDevuelveTextoEnColumna(String texto_buscar, String columna_busco, String columna_devuelvo) {	
		String texto_devolver="";

		for(int i=0;i<100;i++) {		
			texto_devolver = driver.findElement(By.xpath("//*[@class='t-widget t-grid']/table/tbody/tr[" + String.valueOf(i+1) + "]/td[" + columna_busco + "]")).getText();
			if(texto_devolver.equals(texto_buscar.toUpperCase())) {
				texto_devolver = driver.findElement(By.xpath("//*[@class='t-widget t-grid']/table/tbody/tr[" + String.valueOf(i+1) + "]/td[" + columna_devuelvo + "]")).getText();
				break;// para que no falle xq no se cuantos ciclos son. Buscar como saber cuantos elementos hay de td[] y tr[]
			}
		}
		return texto_devolver;
	}

	public void buscaEnGrillaViejaTextoYejecutaAccion(String texto_buscar, String columna_busco,String titulo_boton) 
	{
		String texto="";	
		String ruta_xpath = "//a[contains(.,'" + titulo_boton + "')]";

		WaitForElement(By.xpath(ruta_xpath), 60);

		for(int i=0;i<100;i++) {		
			texto = driver.findElement(By.xpath("//*[@class='t-widget t-grid']/table/tbody/tr[" + String.valueOf(i+1) + "]/td[" + columna_busco + "]")).getText();
			if(texto.toUpperCase().equals(texto_buscar.toUpperCase())) {

				List<WebElement> listado = driver.findElements(By.xpath(ruta_xpath));
				listado.get(i).click();				
				break;// para que no falle xq no se cuantos ciclos son. Buscar como saber cuantos elementos hay de td[] y tr[]
			}
		}		
	}

	public void EdicionTarjetaBtns(String accion) {
		if(accion.toLowerCase().equals("actualizar")) {
			edicionTarjeta_btn_actualizar.click();}
		if(accion.toLowerCase().equals("volver")) {
			edicionTarjeta_btn_volver.click();}
	}

	public void estadoCuotas(String p_id, boolean estado) {
		String id = "chk_" + p_id;
		WebElement elemento = driver.findElement(By.id(id));
		if(estado) {
			if(!elemento.isSelected())
				elemento.click();}
		else {
			if(elemento.isSelected())
				elemento.click();
		}	
	}

	public boolean validacionEdicionCuotasPosModificado() {
		boolean a = false;
		WaitForElement(By.id("divAlert"), 50);
		if(driver.findElement(By.id("divAlert")).getText().equals("Grabado Exitoso.")) {
			if(driver.findElement(By.id("divAlert")).getText().toLowerCase().equals("Grabado Exitoso.".toLowerCase())) {
				a = true;
				espera(3000);
				driver.findElement(By.cssSelector(".ui-button-icon-primary.ui-icon.ui-icon-closethick")).click();
			}

		}	return a;		
	}
	
	public boolean validacionEdicionCuotasModificado() {
		boolean a = false;
		WaitForElement(By.id("divAlert"), 50);
		if(driver.findElement(By.id("divAlert")).getText().equals("Grabado Exitoso.")) {
			if(driver.findElement(By.id("divAlert")).getText().toLowerCase().equals("Grabado Exitoso.".toLowerCase())) {
				a = true;
				espera(3000);
				driver.findElement(By.cssSelector(".ui-button-icon-primary.ui-icon.ui-icon-closethick")).click();
			}

		}	return a;		
	}

	//---------------------------------------------


	public void ConsultaDeRecaudaciones(String depopc, String posopc, String cajero, String mpago, String canal, String orde) {
		List<WebElement> combos = driver.findElements(By.cssSelector(".t-dropdown-wrap.t-state-default"));
		WaitForElement(By.cssSelector(".t-dropdown-wrap.t-state-default"), 60);
		for(int i = 0;i<combos.size();i++) {
			try {Thread.sleep(3000);} catch (InterruptedException e) {e.printStackTrace();}
			((JavascriptExecutor)driver).executeScript("window.scrollTo(0,"+combos.get(i).getLocation().y+")");
			combos.get(i).click();
			List<WebElement> opc = driver.findElements(By.cssSelector(".t-popup.t-group"));
			try {Thread.sleep(1000);} catch (InterruptedException e) {e.printStackTrace();}
			List<WebElement> opc2 = opc.get(i).findElements(By.tagName("li"));
			for (WebElement e : opc2) {
				System.out.println(e.getText());
				if(e.getText().equals(depopc)) {
					e.click();
					break;}
				if(e.getText().equals(posopc)) {
					e.click();
					break;}
				if(e.getText().equals(cajero)) {
					e.click();
					break;}
				if(e.getText().equals(mpago)) {
					e.click();
					break;}
				if(e.getText().equals(canal)) {
					e.click();
					break;}
				if(e.getText().equals(orde)) {
					e.click();
					break;}
			}
			try {Thread.sleep(1000);} catch (InterruptedException e) {e.printStackTrace();}}
		//////////////////777
		List<WebElement> fields = driver.findElements(By.cssSelector(".t-input.form-control"));
		((JavascriptExecutor)driver).executeScript("window.scrollTo(0,"+fields.get(0).getLocation().y+")");
		fields.get(0).clear();
		fields.get(0).sendKeys("18/04/2018");
		try {Thread.sleep(1000);} catch (InterruptedException e) {e.printStackTrace();}
		fields.get(1).clear();
		fields.get(1).sendKeys("20/04/2018");
		////////////////////
		try {Thread.sleep(1000);} catch (InterruptedException e) {e.printStackTrace();}
			List<WebElement> btnes = driver.findElements(By.cssSelector(".t-button.t-grid-edit"));
			btnes.get(1).click();

		}
				
	public void ConsultaDeRecaudacionesporconcepto(String depopc, String posopc, String orde) {
		List<WebElement> combos = driver.findElements(By.cssSelector(".t-dropdown-wrap.t-state-default"));
		WaitForElement(By.cssSelector(".t-dropdown-wrap.t-state-default"), 60);
		for(int i = 0;i<combos.size();i++) {
			try {Thread.sleep(1000);} catch (InterruptedException e) {e.printStackTrace();}
			((JavascriptExecutor)driver).executeScript("window.scrollTo(0,"+combos.get(i).getLocation().y+")");
			combos.get(i).click();
			List<WebElement> opc = driver.findElements(By.cssSelector(".t-popup.t-group"));
			try {Thread.sleep(1000);} catch (InterruptedException e) {e.printStackTrace();}
			List<WebElement> opc2 = opc.get(i).findElements(By.tagName("li"));
			for (WebElement e : opc2) {
				System.out.println(e.getText());
				if(e.getText().equals(depopc)) {
					e.click();
					break;}
				if(e.getText().equals(posopc)) {
					e.click();
					break;}
				if(e.getText().equals(orde)) {
					e.click();
					break;}
			}
			

			try {Thread.sleep(1000);} catch (InterruptedException e) {e.printStackTrace();}}


		List<WebElement> btnes = driver.findElements(By.cssSelector(".t-button.t-grid-edit"));
		btnes.get(1).click();
		}
	
	public void seleccionarPaginado(String i) {

		List<WebElement> listado = driver.findElements(By.cssSelector(".t-link.t-pager-btn"));

		for(WebElement e:listado) {		
			if(e.getAttribute("page").equals(i))
				if(e.getText().equals(i))
				{e.click();
				break;}					
		}		
	}

	public boolean asignacionDependenciaExitosa() {
		boolean a = false;
		WaitForElement(By.id("divAlert"), 60);
		if(driver.findElement(By.id("divAlert")).getText().toLowerCase().equals("Grabado Exitoso.".toLowerCase())) {
			a = true;
			WaitForElement((By.cssSelector(".ui-button.ui-widget.ui-state-default.ui-corner-all.ui-button-icon-only.ui-dialog-titlebar-close")), 60);
			espera(1000);
			driver.findElement(By.cssSelector(".ui-button.ui-widget.ui-state-default.ui-corner-all.ui-button-icon-only.ui-dialog-titlebar-close")).click();
		}
		return a;	
	}



	/************METODOS DE TEST PARAMETRIZADOS**********/


	String usuario ="jbauchi";
	String paswd = "Ab123456";

	public void IrHome() {	
		TestBase TB = new TestBase(driver);
		TB.PortalBackOffice();
		espera(3000);
		Login lg = new Login(driver);
		lg.logearBO(usuario, paswd);
		espera(3000);
	}

	public void completarDatosConcepto(String p_periodo_tarifaria, String p_fecha_publicacion , String p_Cod1 , String p_Cod2 , String p_Cod3, String p_descripcion, String p_renglon1, String p_Agencia_asignada, String p_valor, Boolean p_es_redondeable, Boolean p_tiene_precio_fijo, Boolean p_cantidad_variable, Boolean p_es_devolucion) {

		String periodo_tarifaria = p_periodo_tarifaria;
		String fecha_publicacion = p_fecha_publicacion;
		String Cod1 = p_Cod1;
		String Cod2 = p_Cod2;
		String Cod3 = p_Cod3;	
		String Descripcion = p_descripcion;
		String Renglon1 = p_renglon1;
				
		String Agencia_asignada = p_Agencia_asignada;
		String valor = p_valor;
		boolean es_redondeable = p_es_redondeable;
		boolean tiene_precio_fijo = p_tiene_precio_fijo;
		boolean cantidad_variable = p_cantidad_variable;
		boolean es_devolucion = p_es_devolucion;

		//completar datos de creacion de CONCEPTO y GUARDAR
		ConceptoNuevosDatos(periodo_tarifaria,fecha_publicacion,Cod1,Cod2,Cod3,Descripcion,Renglon1,Agencia_asignada,valor,es_redondeable,tiene_precio_fijo,cantidad_variable,es_devolucion);
	}

	public void crearConcepto_click() {
		NuevoConceptoBtns("crear");
	}
	
	public void crearConceptoAprobarlo(String periodo_tarifaria, String fecha_publicacion,String 
			Cod1,String Cod2, String Cod3, String descripcion, String renglon1,String Agencia_asignada, String valor, boolean es_redondeable, 
			boolean tiene_precio_fijo, boolean cantidad_variable, boolean es_devolucion, String codigo) {
		BackOfficePage BO = new BackOfficePage(driver);
	
		BO.crearConcepto(periodo_tarifaria, fecha_publicacion, Cod1, Cod2, Cod3, descripcion, renglon1, Agencia_asignada, valor, es_redondeable, tiene_precio_fijo, cantidad_variable, es_devolucion);
		Assert.assertTrue(BO.validacionConceptoCreado());
	
		espera(3000);
		BO.aprobarConcepto(codigo);
		espera(2000);
		Assert.assertTrue(BO.validacionConceptoAprobado(codigo));
	}
	
	public void aprobarConcepto_click() {
		WaitForElement(By.xpath("//a[contains(.,'Aprobar')]"), 20);
		espera(1000); //aunque encuentre el boton, a veces no hace click bien
		driver.findElement(By.xpath("//a[contains(.,'Aprobar')]")).click();			
	}
	

	public void asignarDependencia_Concepto(String p_concepto, String p_dep1, String p_dep2) {
		
		String codigo = p_concepto;
		String dep1 = p_dep1;
		String dep2 = p_dep2;

		//entro al menu CONCEPTOS
		espera(3000);
		menu("administra", "Conceptos");				

		//completa datos de BUSQUEDA
		//espera(10000);
		buscarConcepto(codigo, "","","","",true, false, false,false,"");

		//click en BUSCAR
		//espera(1000);
		ConceptoBtns("buscar");

		//selecciono ASIGNAR DEPENDENCIA  
		espera(2000);
		ConceptoBtns("asignar dependencias");

		//en grilla vieja, presiono SELECCIONAR
		espera(3000);
		buscaEnGrillaViejaTextoYejecutaAccion(dep1,"2","Seleccionar"); 

		//en grilla vieja, presiono PAGINA N
		espera(3000);
		seleccionarPaginado("6");

		//en grilla vieja, presiono SELECCIONAR
		espera(3000);
		buscaEnGrillaViejaTextoYejecutaAccion(dep2,"2","Seleccionar");

		espera(3000);

		driver.findElement(By.id("btnSave")).click();
		System.out.println("Se asigno el concepto: "+codigo+" a la dependencia: "+dep1 +" y "+dep2 );

	}

	public Boolean validacionConceptoAprobado(String codigo) {
		
		buscarConcepto(codigo, "","","","",false, false, true,false,"");

		//click en BUSCAR
		espera(1000);
		ConceptoBtns("buscar");
		
		espera(4000);						
		if(driver.findElements(By.xpath("//a[contains(.,'Aprobar')]")).size() == 0)
			return true;
		else
			return false;
	}

	public boolean validarbusquedareporte() {
		boolean a = true;
		((JavascriptExecutor)driver).executeScript("window.scrollTo(0,"+driver.findElement(By.cssSelector(".t-toolbar.t-grid-toolbar.t-grid-top")).getLocation().y+")");

		
		
		return a;
	}
	
	public void editarCuotasPos(String tarjeta, String c1, boolean e1, String c2, boolean e2, String c3, boolean e3,
			String c4, boolean e4) {

		espera(2000);		
		menu("administra", "Cuotas de Tarjeta");
		
		buscaEnGrillaViejaTextoYejecutaAccion(tarjeta, "1", "Editar Cuotas de POS");		
		
		espera(2000);
		estadoCuotas(c1, e1);
		estadoCuotas(c2, e2);	
		estadoCuotas(c3, e3);	
		estadoCuotas(c4, e4);	
		
		//GUARDO cambios
		EdicionTarjetaBtns("actualizar");
		
	}

	public void configurarCuotasTarjeta(String tarjeta,String cuotas) {
		
		espera(2000);		
		menu("administra", "Cuotas de Tarjeta");
				
		buscaEnGrillaViejaTextoYejecutaAccion(tarjeta, "1", "Configurar");
				
		driver.findElement(By.id("Cuotas")).clear();
		driver.findElement(By.id("Cuotas")).sendKeys(cuotas);
		
		//GUARDO cambios
		driver.findElement(By.id("btnSave")).click();
	}

	public void eliminarConcepto(String p_concepto) {
	
		/*
		 * EL CONCEPTO DEBE ESTAR PENDIENTE PARA PODER ELIMINARLO
		 * SI ESTA APROBADO, NO SE PUEDE ELIMINAR
		 */
		String codigo = p_concepto;
		
		//entro al menu CONCEPTOS
		menu("administra", "Conceptos");
		
		//completa datos de BUSQUEDA
		WaitForElement(By.id("Codigo"), 60);
		buscarConcepto(codigo, "","","","",false, false,true,false,"");
		
		//click en BUSCAR
		espera(1000);
		ConceptoBtns("buscar");
				
		espera(2000);
		List<WebElement> listado = driver.findElements(By.cssSelector(".t-no-data"));//mejorar la logica
		
		if(listado.size() == 1) {
			//selecciono ELIMINAR concepto  
			WaitForElement(By.xpath("//a[contains(.,'Eliminar')]"), 60);
			driver.findElement(By.xpath("//a[contains(.,'Eliminar')]")).click();
			System.out.println("Se elimino el concepto: "+codigo);
		}
		else
			System.out.println("No hay concepto para eliminar: "+codigo);
	}
	


	public void crearConcepto(String p_periodo_tarifaria, String p_fecha_publicacion,String 
		p_Cod1,String p_Cod2, String p_Cod3, String p_descripcion, String p_renglon1,String p_Agencia_asignada, String p_valor, boolean p_es_redondeable, 
		boolean p_tiene_precio_fijo, boolean p_cantidad_variable, boolean p_es_devolucion) {
	
		String periodo_tarifaria = p_periodo_tarifaria;
		String fecha_publicacion = p_fecha_publicacion;
		String Cod1 = p_Cod1;
		String Cod2 = p_Cod2;
		String Cod3 =p_Cod3;	
		String codigo = Cod1 +"."+Cod2+"."+Cod3;
		String descripcion = p_descripcion;
		String renglon1 = p_renglon1;
		
		String Agencia_asignada = p_Agencia_asignada;
		String valor = p_valor;
		boolean es_redondeable = p_es_redondeable;
		boolean tiene_precio_fijo = p_tiene_precio_fijo;
		boolean cantidad_variable = p_cantidad_variable;
		boolean es_devolucion = p_es_devolucion;
		
		//ir al menu->conceptos
		menu("admin", "Conceptos");

		//click sobre el boton NUEVO
		espera(10000);
		ConceptoBtns("nuevo"); 
		
		espera(5000);
		completarDatosConcepto(periodo_tarifaria, fecha_publicacion, Cod1, Cod2, Cod3, descripcion, renglon1, Agencia_asignada, valor, es_redondeable, tiene_precio_fijo, cantidad_variable, es_devolucion);
		
		crearConcepto_click();
		System.out.println("Se creo el concepto: " +codigo);
	}
	
	public void aprobarConcepto(String codigo) {

		//entro al menu CONCEPTOS
		menu("administra", "Conceptos");

		//completa datos de BUSQUEDA
		espera(10000);
		buscarConcepto(codigo, "","","","",false, false, true,false,"");

		//click en BUSCAR
		espera(1000);
		ConceptoBtns("buscar");

		aprobarConcepto_click(); 
		System.out.println("Se aprobo el concepto: " +codigo);
	}

	public void EdicionConcepto(String codigo) {

		//entro al menu CONCEPTOS
		menu("administra", "Conceptos");				

		//completa datos de BUSQUEDA
		espera(10000);
		buscarConcepto(codigo, "","","","",false, false,true,false,"");

		//click en BUSCAR
		espera(1000);
		ConceptoBtns("buscar");

		//selecciono EDITAR concepto  
		espera(5000);
		WaitForElement(By.xpath("//a[contains(.,'Editar')]"), 60);
		driver.findElement(By.xpath("//a[contains(.,'Editar')]")).click();

		//modifica campo RENGLON1
		espera(2000);
		editarRenglon1("modific");

		//click boton CREAR
		EdicionConceptoBtns("actualizar");
		System.out.println("Se edito el concepto: "+codigo);

	}

	public void crearUsuario(String nombretxt, String apellidotxt, String logintxt, String emailtxt, String sectortxt,
			String dependencia, String legajotxt, String dnitxt) {

		menu("admin", "usuarios");

		UsuariosBtns("nuevo");
		UsuarioNuevoDatos(nombretxt, apellidotxt, logintxt, emailtxt, sectortxt, dependencia, legajotxt, dnitxt);
		NuevoUsuarioBtns("crear");
		System.out.println("Se creo el usuario con legajo: "+legajotxt);
	}


		

}