package pages;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import test.TestBase;

public class SirToolsPage extends BasePage {

final WebDriver driver;
	


	public SirToolsPage(WebDriver driver){
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	public void IngresoAmbiente() {
		
		TestBase TB = new TestBase(driver);
		TB.SirTools();
		esperaX(3000);
		Login lg = new Login(driver);
		lg.logearST("lpellegrini","lpellegrini");		
	}
	
	/*
	 **********DEFINICION WEBELEMENTS**********
	 */


	@FindBy (how = How.ID, using = "ddlAplicaciones")
	private WebElement ListApps;
	
	@FindBy(how = How.CSS,using=".ui-button-text")
	private WebElement execute;
	
	@FindBy(how = How.ID,using="Parametros")
	private WebElement parametros;

	@FindBy(how = How.ID,using="Respuesta")
	private WebElement Respuesta;
	
	/*
	 ***********METHODS*************
	 */
	 
	public void SeleccionarApp(String app) {
		setSimpleDropdown(ListApps, app);
	}
	
	public void Execute() {
		execute.click();
	}
	
	public void IngresarParametros(String parametro) {
		parametros.sendKeys(parametro);
	}
	
	public void ejecutar(String app,String parametro) {
		try {
			Thread.sleep(5000);	} catch (InterruptedException e1) {	e1.printStackTrace();}
		SeleccionarApp(app);
		IngresarParametros(parametro);
		Execute();
		try {Thread.sleep(20000);} catch (InterruptedException e) {e.printStackTrace();}
		}
	
	public boolean ValidarCierreZ() {
		boolean a;
		String resp = Respuesta.getAttribute("value");
		System.out.println(resp);
		a = resp.contains("Se ha procesado correctamente el lote");
		return a;
		
	}
}
