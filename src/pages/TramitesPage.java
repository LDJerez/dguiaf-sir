package pages;

import static org.testng.Assert.assertTrue;

import java.nio.charset.CodingErrorAction;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;

//@FindBy(how = How.XPATH,using = "/html/body/div[4]/div/div/div/div[2]/div/a[1]")
//@FindBy(how = How.XPATH,using = "//a[contains(.,'Tr�mites')]")
//private WebElement btnTramites;


import javax.swing.ScrollPaneLayout;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;
import okhttp3.Call;
import test.TestBase;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

public class TramitesPage extends BasePage{

	final WebDriver driver;
	public TramitesPage(WebDriver driver){
		this.driver = driver;
		PageFactory.initElements(driver, this);
	} 

	public void ingresoAmbientePortalTramite() {
		TestBase TB = new TestBase(driver);
		TB.PortalTramites();
	}
	
	public void ingresoAmbientePortalTramiteAbm() {
		TestBase TB = new TestBase(driver);
		TB.PortalTramitesABM();
		
		Login lg = new Login(driver);
		lg.logearTramiteAbm("jbauchi","Ab123456");
	}
	
	
	
	
	/*
	 ******PASAR A BASEPAGE
	 */
	public void WaitForElementClickable(By element, int tiempo) {
		WebDriverWait wait;
		wait = new WebDriverWait(driver, tiempo);
		wait.until(ExpectedConditions.elementToBeClickable(element));
	}

	public void WaitForElementClickable(WebElement element, int tiempo) {
		WebDriverWait wait;
		wait = new WebDriverWait(driver, tiempo);
		wait.until(ExpectedConditions.elementToBeClickable(element));
	}
	public void espera(int espera) {
		try {Thread.sleep(espera);} catch (InterruptedException e) {e.printStackTrace();}	
	}
	
	
	
	/*
	 ***************WEBELEMENTS*********** 
	 */

	@FindBy(how = How.CLASS_NAME,using = "form-control")
	private List<WebElement> campos;

	@FindBy(how = How.ID,using = "btnContinuar")
	private WebElement btnContinuar;

	@FindBy(how = How.ID,using = "btnPago")
	private WebElement btnPago;


	//----Datos Tarjeta
	@FindBy(how = How.NAME,using = "NOMBREENTARJETA")
	private WebElement NOMBREENTARJETA;

	@FindBy(how = How.NAME,using = "NROTARJETA")
	private WebElement NROTARJETA;

	@FindBy(how = How.NAME,using = "VENCTARJETA")
	private WebElement VENCTARJETA;

	@FindBy(how = How.NAME,using = "CODSEGURIDAD")
	private WebElement CODSEGURIDAD;

	@FindBy(how = How.NAME,using = "EMAILCLIENTE")
	private WebElement EMAILCLIENTE;

	@FindBy(how = How.NAME,using = "TIPODOC")
	private WebElement TIPODOC;

	@FindBy(how = How.NAME,using = "NRODOC")
	private WebElement NRODOC;

	@FindBy(how = How.NAME,using = "CALLE")
	private WebElement CALLE;

	@FindBy(how = How.NAME,using = "NROPUERTA")
	private WebElement NROPUERTA;

	@FindBy(how = How.NAME,using = "FECHANACIMIENTO")
	private WebElement FECHANACIMIENTO;

	@FindBy(how = How.NAME,using = "ok")
	private WebElement OK;

	//*****************************


	@FindBy(how = How.ID,using = "nombre")
	private WebElement nombre;

	@FindBy(how = How.ID,using = "tipoDoc")
	private WebElement tipoDoc;

	@FindBy(how = How.ID,using = "numeroDoc")
	private WebElement numeroDoc;

	@FindBy(how = How.ID,using = "email")
	private WebElement email; 

	@FindBy(how = How.ID,using = "direccion")
	private WebElement direccion; 

	@FindBy(how = How.ID,using = "piso")
	private WebElement piso; 

	@FindBy(how = How.ID,using = "depto")
	private WebElement depto; 

	@FindBy(how = How.ID,using = "observaciones")
	private WebElement observaciones; 

	@FindBy(how = How.ID,using = "localidad")
	private WebElement localidad; 

	@FindBy(how = How.ID,using = "codPostal")
	private WebElement codPostal;


	//----TRAMITE ABM 

	@FindBy(how = How.XPATH,using = "//*[text() ='Crear un nuevo trámite']")	//PONER EN LA BASE
	private WebElement BtnCrearNuevoTramite;
	
	
	@FindBy(how = How.XPATH,using = "//*[text() ='Crear un nuevo rubro']")	
	private WebElement BtnCrearNuevoRubro;
	
	@FindBy(how = How.XPATH,using = "//*[text() ='Crear una nueva página']")	
	private WebElement BtnCrearNuevaPagina;


	@FindBy(how = How.XPATH,using = "//*[text() ='Agregar nuevo grupo de conceptos']")	
	private WebElement btnAgregarNuevoGrupoConceptos;


	@FindBy(how = How.XPATH,using = "//*[text() ='Agregar concepto al grupo']")	
	private WebElement btnAgregarConceptoAlGrupo;

	@FindBy(how = How.XPATH,using = "//select[@data-ng-model='tc.concepto']")
	private WebElement DropConceptos;
	
	@FindBy(how = How.ID,using = "selectDependBUI")
	private WebElement dropTramiteBui;

	@FindBy(how = How.XPATH,using = "//*[text() ='Agregar el concepto seleccionado']")	
	private WebElement btnAgregarConceptoSeleccionado;


	@FindBy(how = How.XPATH,using = "//*[text() ='¡Guardar el Trámite!']")
	private WebElement btnGuardarTramite;
	
	@FindBy(how = How.XPATH,using = "//*[text() ='¡Guardar el Rubro!']")
	private WebElement btnGuardarRubro;
	
	@FindBy(how = How.XPATH,using = "//*[text() ='¡Guardar la Página!']")
	private WebElement btnGuardarPagina;

	@FindBy(how = How.XPATH,using = "//*[text() ='Agregar nuevo dato particular']")
	private WebElement btnDatoParticular;
	
	@FindBy(how = How.XPATH,using = "//select[@data-ng-model='datoPart.clase']")
	private WebElement datoPartMenu;
	
	@FindBy(how = How.XPATH,using = "//*[text() ='Datos específicos del trámite']")
	private WebElement checkDatosParticulares;
	
	
	@FindBy(how = How.ID,using = "btnAceptar")
	private WebElement btnAceptar;
	
	

	@FindBy(how = How.ID,using = "btnAceptar")
	private WebElement brtEDITAR;
	
	/*	SE USA OTRO METODO, QUE HACE UNA LISTA, A FUTURO VER SI SE PUEDE IMPLEMENTAR ESTE COMO ALGO MAS SEGURO.
	*	@FindBy(how = How.XPATH,using = "//button[@data-ng-class='!edicion.tramite.soloBUI']")
	*	private WebElement btnForzarBui;
	*/
	
	@FindBy(how = How.XPATH,using = "//input[@data-ng-model='edicion.tramite.nombre']")
	private WebElement tramiteNombre;
	
	@FindBy(how = How.XPATH,using = "//input[@data-ng-model='edicion.tramite.descripcion']")
	private WebElement tramiteDesc;
	
	@FindBy(how = How.XPATH,using = "//input[@data-ng-model='edicion.tramite.mensaje']")
	private WebElement tramiteMsg;
	
	@FindBy(how = How.XPATH,using = "//input[@data-ng-model='edicion.tramite.usuario']")
	private WebElement tramiteUsuario;
	
	@FindBy(how = How.XPATH,using = "//input[@data-ng-model='edicion.tramite.password']")
	private WebElement tramitePassword;
	
	@FindBy(how = How.XPATH,using = "//input[@data-ng-model='rubro.descripcion']")
	private WebElement rubroDescripcion;
	 
	@FindBy(how = How.XPATH,using = "//input[@data-ng-model='pagina.Descripcion']")
	private WebElement paginaDescripcion;
	
	@FindBy(how = How.XPATH,using = "//textarea[@data-ng-model='pagina.Html']")
	private WebElement paginaHTML;

	@FindBy(how = How.XPATH,using = "//input[@data-ng-model='datoPart.descripcion']")
	private WebElement descDatoParticular;
	
	@FindBy(how = How.XPATH,using = "//input[@data-ng-model='datoPart.valorMin']")
	private WebElement valorMinDatoParticular;
	
	@FindBy(how = How.XPATH,using = "//input[@data-ng-model='datoPart.valorMax']")
	private WebElement valorMaxDatoParticular;
	
	
	
	
	//----TRAMITE


	/*
	 **************METODOS***************** 
	 */

	
	

	public void seleccionarTarea(String opcion) {	//deberia estar igual que en PAGOS PAGE Y LUEGO PONERLO EN LA CLASE BASEPAGE

		espera(4000);		

		By opcionesTareas = By.cssSelector(".btn.btn-default");
		List<WebElement> listado = driver.findElements(opcionesTareas);


		//System.out.println(listado.size());


		for(WebElement e:listado) {

			//System.out.println(e.getText());
			if(e.getText().toLowerCase().equals(opcion.toLowerCase())) {
				e.click();
				break;					
			}					
		}		
	}


	public void pbuscaEnGrillaNgTextoYejecutaAccion(String texto, String tipo, String accion, int cant_columnas, long espera)//*tipo=tramite/cliente *accion=eliminar/editar   *el valor cant_columnas = columnas de datos (no incluye botones) 
	{
		try {Thread.sleep(espera);}catch (InterruptedException e) {	e.printStackTrace();}
		List<WebElement> listado = driver.findElements(By.cssSelector(".ng-binding"));

		int i = 0;
		Boolean existe = false;
		for(WebElement e:listado) {

			//System.out.println(e.getText());
			if(e.getText().contains(texto)){	
				existe = true;
				break;
			}i++;
		}	

		if(existe) {
			int fila = (i/cant_columnas)+1;
			System.out.println("Fila: "+fila);
			String ruta_xpath="";

			if(tipo.equals("Tramite")) {
				if(accion.toLowerCase().equals("eliminar")) {
					ruta_xpath = "//*[@id='tablaTramites']/tbody/tr["+ String.valueOf(fila)  +"]/td[" + String.valueOf(cant_columnas+2)+ "]/a[1]";
					driver.findElement(By.xpath(ruta_xpath)).click();

					//confirmo el eliminar
					espera(2000);
					driver.findElement(By.id("mjeDesactivarBtnConfirmar")).click();	
				}
				if(accion.toLowerCase().equals("editar")) {
					ruta_xpath = "//*[@id='tablaTramites']/tbody/tr["+ String.valueOf(fila)  +"]/td[" + String.valueOf(cant_columnas+1) + "]/a[1]";
					driver.findElement(By.xpath(ruta_xpath)).click();
				}
			}
			if(tipo.equals("Cliente")) {
				if(accion.toLowerCase().equals("eliminar")) {
					ruta_xpath = "//*[@id='tablaClientes']/tbody/tr["+ String.valueOf(fila)  +"]/td[" +  String.valueOf(cant_columnas+2) + "]/a[1]";	
					driver.findElement(By.xpath(ruta_xpath)).click();

					//confirmo el eliminar
					espera(2000);
					Alert alert = driver.switchTo().alert();
					alert.accept(); 
					espera(2000);
					driver.findElement(By.cssSelector(".btn.btn-sm.btn-default")).click();
				}
				if(accion.toLowerCase().equals("editar")) {
					ruta_xpath = "//*[@id='tablaClientes']/tbody/tr["+ String.valueOf(fila)  +"]/td[" +  String.valueOf(cant_columnas+1) + "]/a[1]";
					driver.findElement(By.xpath(ruta_xpath)).click();

					//confirmo el eliminar
					espera(2000);
					Alert alert = driver.switchTo().alert();
					alert.accept(); 
					espera(2000);
					driver.findElement(By.cssSelector(".btn.btn-sm.btn-default")).click();
				}
			}
		}
		else {
			System.out.println("No hay "+ tipo + "  para " + accion);
			espera(5000);
		}
	}


	public void crearNuevoTramite(int esperaInicial)  {//NO DEBERIA ESTAR DENTRO DE UN METODO QUE MANEJE TOdO Administraci�n de Tr�mites???
		espera(esperaInicial);			
		BtnCrearNuevoTramite.click();						
	}


	public void completarInput(String pAtributo, String pCampo, String pTexto ) {
		//FIXME REEMPLAZAR POR LOS METODOS "BETTER" QUE ESTAN ABAJO.
		List<WebElement> campos =  driver.findElements(By.cssSelector(".input-sm.form-control.ng-pristine.ng-valid"));
		for(WebElement e : campos) {
			if(e.getAttribute(pAtributo).toLowerCase().contains(pCampo)) {
				e.sendKeys(pTexto);
			}
		}
	}
	
	public void tramiteTituloYDescripcion(String titulo, String descripcion) { //BETTER
		tramiteNombre.sendKeys(titulo);
		tramiteDesc.sendKeys(descripcion);
	}
	
	public void tramiteInfoAlUsuario(String mensaje) { //BETTER
		tramiteMsg.sendKeys(mensaje);
	}
	
	public void tramiteServPagoElectronico(String usuario, String password) { //BETTER
		
		tramiteUsuario.clear();
		tramiteUsuario.sendKeys(usuario);
		espera(1000);
		tramitePassword.clear();
		tramitePassword.sendKeys(password);
	}
	
	public void seleccionarTramiteBui(String tramite) {
		Select dependencia = new Select(dropTramiteBui);
		dependencia.selectByVisibleText(tramite);
	}
	
	

	public void seleccionButton(String p1, String p2, String p3) {
		// ES PARA BOTON "ESTADO TRAMITE" "REDIRECCIONA?" Y "FORZAR BUI"
		List<WebElement> campos =  driver.findElements(By.cssSelector(".btn-group.btn-group-justified"));//btn-group btn-group-justified
		ArrayList<String> listA = new ArrayList<String>();
		listA.add(p1);
		listA.add(p2);
		listA.add(p3); // FORZAR BUI



		for(int i=0; i < campos.size() ; i++) {

			if(listA.get(i).toString().toLowerCase().equals("si")) {
				campos.get(i).findElement(By.cssSelector(".btn.btn-default")).click();
			}
			else {
				campos.get(i).findElement(By.cssSelector(".btn.btn-danger")).click();	//FIXME esto no funciona si tenes que seleccionar NO y esta en SI.			
			}
		}
		/*
		 * 
		 * 
		 * 1-crear una lista de grupos 
		 * 2-con el for recorro la lista, para buscar el atributo (detalle) que identifica a cada grupo (unico) . detalle = edicion.tramite.activo = true  accion activo
		 * 3- 
		 * 
		for(WebElement e : campos) {

					//System.out.println(e.getAttribute("data-ng-click").toString());	//getAtr
					if(e.getAttribute("data-ng-click").contains("activo")) {

						if(parametro.equals("SI")) 
							{e.findElement(By.cssSelector(".btn.btn-default")).click();}
						else 
							{e.findElement(By.cssSelector(".btn.btn-danger")).click();}



			//}
		 */

	}


	public void agregarNuevoGrupoConceptos(String concepto) {
		btnAgregarNuevoGrupoConceptos.click();
		espera(1000);
		btnAgregarConceptoAlGrupo.click();
		espera(1000);
		setSimpleDropdown(DropConceptos,concepto);
		espera(3000);
	}

	public void agregarNuevoGrupoConceptos(String concepto1, String concepto2) {
		btnAgregarNuevoGrupoConceptos.click();
		//se agrega el primer concepto
		espera(1000);
		btnAgregarConceptoAlGrupo.click();
		espera(1000);
		setSimpleDropdown(DropConceptos,concepto1);
		espera(3000);
		
		//se agrega el segundo concepto	
		btnAgregarConceptoAlGrupo.click();
		espera(1000);			
		List<WebElement> lista = driver.findElements(By.xpath("//select[@data-ng-model='tc.concepto']"));
		setSimpleDropdown(lista.get(1),concepto2);
		espera(3000);
		}
	
	
	public void agregarConceptosEnGruposDistintos(String concepto1, String concepto2) {
		btnAgregarNuevoGrupoConceptos.click();
		//se agrega el primer grupo
		espera(1000);
		btnAgregarConceptoAlGrupo.click();
		espera(1000);
		setSimpleDropdown(DropConceptos,concepto1);
		espera(3000);
		
		//se agrega el segundo grupo	
		btnAgregarNuevoGrupoConceptos.click();
		espera(1000);
		
		
		List<WebElement> listaBtnAgregarConceptoAlGrupo = driver.findElements(By.xpath("//div[@data-ng-click='edicion.agregarTC(grupo)']"));
		listaBtnAgregarConceptoAlGrupo.get(1).click();
			
		
		espera(1000);			
		List<WebElement> lista = driver.findElements(By.xpath("//select[@data-ng-model='tc.concepto']"));
		setSimpleDropdown(lista.get(1),concepto2);
		espera(3000);
		}
	
	public void guardarTramite() {
		btnGuardarTramite.click();		
	}


	public void menu(String menu) {
		List<WebElement> opc = driver.findElements(By.cssSelector(".opcionMenuSm.opcionMenuMd.opcionMenuLg"));
		for(WebElement e:opc) {
			if(e.getText().toLowerCase().equals(menu.toLowerCase())) {
				e.click();
				break;
			}	
			
		}	
		
	}
	
	
	public void crearNuevoRubro(int esperaInicial) {
		espera(esperaInicial);
		BtnCrearNuevoRubro.click();		
	}
	
	public void descNuevoRubro(String descRubro) {
	rubroDescripcion.sendKeys(descRubro);
	}
	
	public void guardarNuevoRubro() {
	btnGuardarRubro.click();
	}
	
	
	public void crearNuevaPagina(int esperaInicial) {
		espera(esperaInicial);
		BtnCrearNuevaPagina.click();	
	}

	public void descNuevaPagina(String descPagina) {
		paginaDescripcion.sendKeys(descPagina);
	}
	
	public void HTMLNuevaPagina(String textoHTML) {
		paginaHTML.sendKeys(textoHTML);
	}
	
	public void guardarNuevaPagina() {
		btnGuardarPagina.click();
	}
	
	

	
	public void DatosContribuyentePago(String tipo,String nroDoc,String nroBoleta,String mail) {
		setSimpleDropdown(campos.get(0), "Documento Nacional de Identidad");
		campos.get(1).sendKeys(nroDoc);
		campos.get(2).sendKeys(nroBoleta);
		campos.get(3).sendKeys(mail);
		resolvercaptcha();
		btnContinuar.click();
		espera(3000);
		btnContinuar.click();
	}

	public void DatosContribuyenteParaTS33(String nombres, String tipo,String nroDoc,String mail, String direcciontxt,
			String pisotxt,String deptotxt, String obstxt, String localidadtxt, String CP) { //para multiple conceptos unificar con los metodos de arriba
		List<WebElement> listaRbt = driver.findElements(By.cssSelector(".seleccionado"));
		listaRbt.get(0).click();
		//listaRbt.get(1).click();
		
		List<WebElement> listatxtCant = driver.findElements(By.id("cantidad"));
		listatxtCant.get(0).sendKeys("2");
		//listatxtCant.get(1).sendKeys("2");
		
		btnContinuar.click();
		espera(3000);
		nombre.sendKeys(nombres);
		setSimpleDropdown(tipoDoc, "Documento Nacional de Identidad");
		numeroDoc.sendKeys(nroDoc);
		email.sendKeys(mail);
		direccion.sendKeys(direcciontxt);
		piso.sendKeys(pisotxt);
		depto.sendKeys(deptotxt);
		observaciones.sendKeys(obstxt);
		localidad.sendKeys(localidadtxt);
		codPostal.sendKeys(CP);
		espera(3000);
		btnContinuar.click();
		espera(3000);
		btnContinuar.click();
		
	}
	

	
	public void DatosContribuyenteParaTS34(String nombres, String tipo,String nroDoc,String mail, String direcciontxt,
			String pisotxt,String deptotxt, String obstxt, String localidadtxt, String CP) { //para multiple conceptos unificar con los metodos de arriba
		
		
		
		List<WebElement> listaSeleccion = driver.findElements(By.cssSelector(".seleccionado"));
		List<WebElement> listatxtCant = driver.findElements(By.id("cantidad"));
		
		listaSeleccion.get(0).click();
		listatxtCant.get(0).sendKeys(Keys.BACK_SPACE);
		listatxtCant.get(0).sendKeys("1");
			
		listaSeleccion.get(1).click();
		listatxtCant.get(1).sendKeys(Keys.BACK_SPACE);		
		listatxtCant.get(1).sendKeys("1");
		
		btnContinuar.click();
	
		espera(3000);
		nombre.sendKeys(nombres);
		setSimpleDropdown(tipoDoc, "Documento Nacional de Identidad");
		numeroDoc.sendKeys(nroDoc);
		email.sendKeys(mail);
		direccion.sendKeys(direcciontxt);
		piso.sendKeys(pisotxt);
		depto.sendKeys(deptotxt);
		observaciones.sendKeys(obstxt);
		localidad.sendKeys(localidadtxt);
		codPostal.sendKeys(CP);
		espera(3000);
		btnContinuar.click();
		espera(3000);
		btnContinuar.click();
		
	}

	public void DatosContribuyenteDiso(String nombres, String tipo,String nroDoc,String mail, String direcciontxt,
		String pisotxt,String deptotxt, String obstxt, String localidadtxt, String CP) {
		
		btnContinuar.click();
		espera(3000);
		nombre.sendKeys(nombres);
		setSimpleDropdown(tipoDoc, "Documento Nacional de Identidad");
		numeroDoc.sendKeys(nroDoc);
		email.sendKeys(mail);
		direccion.sendKeys(direcciontxt);
		piso.sendKeys(pisotxt);
		depto.sendKeys(deptotxt);
		observaciones.sendKeys(obstxt);
		localidad.sendKeys(localidadtxt);
		codPostal.sendKeys(CP);
		espera(3000);
		btnContinuar.click();
		espera(3000);
		btnContinuar.click();
	}

	/*
	 * A DIFERENCIA DE DatosContribuyenteDiso() hace click en la pantalla de "Seleccion de los conceptos"
	 * Luego unificarlos y mejorarlos
	 */
	public void DatosContribuyenteDiso2(String nombres, String tipo,String nroDoc,String mail, String direcciontxt,
		String pisotxt,String deptotxt, String obstxt, String localidadtxt, String CP) {
		if(!driver.findElements(By.cssSelector(".seleccionado")).isEmpty()) {
			List<WebElement> listaSeleccion = driver.findElements(By.cssSelector(".seleccionado"));
			for(int i = 0 ; i < listaSeleccion.size() ; i++) {
				(listaSeleccion.get(i)).click();
				}
			}
		btnContinuar.click();
		espera(3000);
		nombre.sendKeys(nombres);
		setSimpleDropdown(tipoDoc, "Documento Nacional de Identidad");
		numeroDoc.sendKeys(nroDoc);
		email.sendKeys(mail);
		direccion.sendKeys(direcciontxt);
		piso.sendKeys(pisotxt);
		depto.sendKeys(deptotxt);
		observaciones.sendKeys(obstxt);
		localidad.sendKeys(localidadtxt);
		codPostal.sendKeys(CP);
		espera(3000);
		btnContinuar.click();
		espera(3000);
		btnContinuar.click();
	}
	
	public void DatosContribuyenteDisoConDatoParticular(String nombres, String tipo,String nroDoc,String mail, String direcciontxt,
			String pisotxt,String deptotxt, String obstxt, String localidadtxt, String CP, String inputDatoParticular) {
			if(!driver.findElements(By.cssSelector(".seleccionado")).isEmpty()) {
				List<WebElement> listaSeleccion = driver.findElements(By.cssSelector(".seleccionado"));
				for(int i = 0 ; i < listaSeleccion.size() ; i++) {
					(listaSeleccion.get(i)).click();
					}
				}
			btnContinuar.click();
			espera(3000);
			System.out.println(checkDatosParticulares.getText());
			if(checkDatosParticulares.getText().equals("Datos específicos del trámite")) {
				(campos.get(0)).sendKeys(inputDatoParticular);
				}
			btnContinuar.click();
			espera(3000);
			nombre.sendKeys(nombres);
			setSimpleDropdown(tipoDoc, "Documento Nacional de Identidad");
			numeroDoc.sendKeys(nroDoc);
			email.sendKeys(mail);
			direccion.sendKeys(direcciontxt);
			piso.sendKeys(pisotxt);
			depto.sendKeys(deptotxt);
			observaciones.sendKeys(obstxt);
			localidad.sendKeys(localidadtxt);
			codPostal.sendKeys(CP);
			espera(3000);
			btnContinuar.click();
			espera(3000);
			btnContinuar.click();
		}

	public void resolvercaptcha() {
		JOptionPane.showMessageDialog(null, "PRESIONE ACEPTAR Y LUEGO COMPLETE EL CAPTCHA");
		espera(10000);
	}

	public void SeleccionarFormaDepago(String value) {
		
		WaitForElementClickable(By.xpath("//input[@name='MedioPago' and @value='"+value+"' ]"),60);
		espera(5000);
		driver.findElement(By.xpath("//input[@name='MedioPago' and @value='"+value+"' ]")).click();
		espera(2000);
	}

	public void scrollingandclick(WebElement element) {	
		((JavascriptExecutor)driver).executeScript("window.scrollTo(0,"+element.getLocation().y+")");
		element.click();
	}

	public void btnRealizarPago() {
		scrollingandclick(driver.findElement(By.id("btnPago")));
	}

	public void DatosTarjeta() {
		NOMBREENTARJETA.sendKeys("titular_prueba");
		NROTARJETA.sendKeys("4507990000004905");
		VENCTARJETA.sendKeys("0818");
		CODSEGURIDAD.sendKeys("775");
		EMAILCLIENTE.sendKeys("correo@prueba.com");
		setSimpleDropdown(TIPODOC, "DNI");
		NRODOC.sendKeys("1000000");
		CALLE.sendKeys("calle_prueba");
		NROPUERTA.sendKeys("1");
		FECHANACIMIENTO.sendKeys("01011990");
		OK.click();		
	}

	public void SeleccionarTramite(String tramite){
		List<WebElement> tramites = driver.findElements(By.cssSelector(".well.well-sm.colorTransp"));
		for(WebElement e: tramites) {
			if(e.getText().contains(tramite)) {
				List<WebElement>tramites2 = e.findElements(By.tagName("li"));
				for(WebElement a: tramites2) {
					System.out.println(a.getText());
					String eltramite= a.getText();
					if(eltramite.toLowerCase().equals(tramite.toLowerCase())) {
						scrollingandclick(a.findElement(By.tagName("a")));

						break;}}
				break;}}

	}

	public boolean ValidarPagoVISA() {
		espera(3000);		
		boolean a;
		a = driver.findElements(By.className("texto")).get(0).getText().equals("La transacción ha sido APROBADA.");
		return a;
	}

	public boolean ValidarPagoINTERBANKING() {
		espera(3000);
		
		boolean a;
		a = driver.getCurrentUrl().contains("interbanking");
		return a;
	}

	public boolean ValidarForzarBUI() {
		espera(3000);
		boolean a;
		a = driver.findElement(By.id("overview")).getText().equals("Boleta de Pago - BUI");
		return a;

	}

	public boolean ValidarMpVisa() {
		espera(3000);
		
		boolean a;
		a = driver.findElement(By.id("MedioPago")).getAttribute("value").equals("VISA");
		return a;
	}
	
	public boolean ValidarMpInterbanking() {
		espera(3000);
				
		boolean a;
		a = driver.findElement(By.id("MedioPago")).getAttribute("value").equals("INTERBANKING");
		return a;
	}

	public boolean validarCreacionTramite()  {
		espera(5000);
		boolean a;
		String txt = driver.findElement(By.id("dataMensaje")).getText();
		System.out.println(txt);
		a = txt.equals("El tr\u00e1mite se guard\u00f3 correctamente.");
		return a;	
	}
	
	
	public boolean validarCreacionRubro()  {
		espera(5000);
		boolean a;
		String txt = driver.findElement(By.id("dataMensaje")).getText();
		System.out.println(txt);
		a = txt.equals("El rubro se guard\u00f3 correctamente.");
		return a;	
	}
	
	public boolean validarCreacionPagina()  {
		espera(8000);
		boolean a;
		String txt = driver.findElement(By.id("dataMensaje")).getText();
		System.out.println(txt);
		a = txt.equals("La página se guardó correctamente.");
		return a;	
	}

	public boolean ValidarPagoRAPIPAGO() {
		//validar que sea rapipago
		return false;
	}

	public boolean ValidarPagoREDLINK() {
		//validar que sea redlink
		return false;
	}

	public void deshabilitarTramite(String titulo_tramite) {

		TramitesPage TramitePage = new TramitesPage(driver);
		
		TramitePage.seleccionarTarea("Tr�mites");
		
		espera(3000);
		base__buscaEnGrillaNgTextoYejecutaAccion(driver,titulo_tramite,"Tramite","eliminar",6);
	}
	

	public void clickPagoOnline() { //			ARTURO
		espera(3000);
		driver.findElement(By.linkText("AQUÍ")).click();
		espera(2000);
	}
	
	public void checkearCambioHTML() { //		 ARTURO
		String texto;
		texto = driver.findElement(By.xpath("/html/body/div[2]/section/div/form/div[1]/div")).getText();		
		
		if(texto.equals("Prueba Tiempo de Acreditacion - Automatizacion")) {
			Assert.assertTrue(true);
			} else {
				Assert.assertTrue(false);
		}

	}
	
	public void arturo_Portal_tramite_Verificacion_de_texto_tiempo_acreditacion() {
		//FIXME ESTO VA EN ESTA PARTE?
		String nombre = "Testing Automatizado";
		String tipo = "Documento Nacional de Identidad";
		String nroDoc= "1000000";
		String mail = "correo@prueba.com";
		String direccion = "calle_prueba";
		String piso = "";
		String dpto = "";
		String obs = "TEST";
		String localidad = "CABA";
		String CP = "1234";	
		TestBase TB = new TestBase(driver);
		
		TB.PortalTramites();
		SeleccionarTramite("Contaduría - Cesiones");
		DatosContribuyenteDiso(nombre, tipo, nroDoc, mail, direccion, piso, dpto, obs, localidad, CP);
		clickPagoOnline();
		checkearCambioHTML(); // Hasta aca checkea el cambio.
	
	}
	
	public void agregarNuevoGrupoConceptosVarios() {
		
		ArrayList<String> itemsConceptos = new ArrayList<String>();
		itemsConceptos.add("1.2.2 Licencia de inhumación o cremación");
		itemsConceptos.add("1.2.3 Traslado de defunciones fuera del ámbito de la Capital Federal");
		itemsConceptos.add("1.2.4 Testigos no obligatorios");
		itemsConceptos.add("1.2.7 Libreta de familia- Matrimonio. Cerem de matrim., uniòn civ y entrega de libreta de matrim. y const. de uniòn civ. Fuera de las sedes del Registro Civil.");

		for (int i = 0 ; i < 2 ; i++) {
		btnAgregarNuevoGrupoConceptos.click();
		espera(1000);
		List<WebElement> lista = driver.findElements(By.xpath("//*[text() ='Agregar concepto al grupo']"));
		lista.get(i).click();
		espera(1000);
		List<WebElement> lista2 = driver.findElements(By.xpath("//select[@data-ng-model='tc.concepto']"));
		setSimpleDropdown(lista2.get(i), itemsConceptos.get(i)); 
		espera(3000);
		}
	
	}
	
	public void btnDatoParticular() {
		btnDatoParticular.click();
	}
	
	public void descDatoParticular(String descDato) {
		descDatoParticular.sendKeys(descDato);
	}
	
	public void selectDatoPart() {
	setSimpleDropdown(datoPartMenu, "Número entero"); //FIXME MEJORAR SON POCOS ITEMS, SE PUEDE HACER LISTA ENTERA.
	}
	
	public void inputValoresDatosParticulares(String valorMin, String valorMax) {
		valorMinDatoParticular.sendKeys(valorMin);
		valorMaxDatoParticular.sendKeys(valorMax);
	}

	
	public boolean verificacionDatosParticulares() {
		String a = checkDatosParticulares.getText();
		String b = "Datos específicos del trámite";
		 if (a.equals(b)) {
			 return true;
		 } else {
			 return false;
		 }
		 
		 
	}
	
	public void borrarGruposConceptos() {
		/*
		 * hace click en todos los botones 'borrar grupo' 
		 */
		while(driver.findElements(By.xpath("//div[@data-ng-click='edicion.eliminarGpo(grupo);']")).size() != 0) {
			driver.findElement(By.xpath("//div[@data-ng-click='edicion.eliminarGpo(grupo);']")).click();
			espera(1000);
		}
	}

	
	
	public String pbuscaEnGrillaNgTextoyDevuelveTextoMismaFilaEnColumnaX(String textoABuscar, String tipo, String columnaDelTextoADevolver, int cantColumnasDatos, long espera)//*tipo=tramite/cliente *accion=eliminar/editar   *el valor cant_columnas = columnas de datos (no incluye botones) 
	{//tomado como base para armarlo: pbuscaEnGrillaNgTextoYejecutaAccion()
		try {Thread.sleep(espera);}catch (InterruptedException e) {	e.printStackTrace();}
		List<WebElement> listado = driver.findElements(By.cssSelector(".ng-binding"));
		int i = 0;
		Boolean existe = false;
		String textoDevolver ="";
		
		for(WebElement e:listado) {

			//System.out.println(e.getText());
			if(e.getText().contains(textoABuscar)){	
				existe = true;
				break;
			}i++;
		}	

		if(existe) {
			int fila = (i/cantColumnasDatos)+1;
			System.out.println("Fila: "+fila);
			String ruta_xpath="";

			if(tipo.equals("Tramite")) {
				 {
					ruta_xpath = "//*[@id='tablaTramites']/tbody/tr["+ String.valueOf(fila)  +"]/td[" + String.valueOf(columnaDelTextoADevolver)+"]";
					textoDevolver = driver.findElement(By.xpath(ruta_xpath)).getText();
				}			
			}			
		}
		else {
			System.out.println("No hay "+ tipo + "  para " );
			espera(5000);
		}
		return textoDevolver;
	}

	
	public void validarPaginaSeleccioneMedioPago() {//FIXME 
		espera(2000);
		if	( driver.findElements(By.xpath("//li[contains(.,'Seleccione medio de pago')]")).size() == 1)
			Assert.assertTrue(true);
		else
			Assert.assertTrue(false);		
	}

	
	
	public String direcionPagoElectronicoParaObtenerNroBui() {
		String nroBoleta="";
		
		driver.findElement(By.xpath("//a[contains(@href,'/BUI/RedireccionPagoElectronico')]")).click();
		
		espera(3000);
		nroBoleta = driver.findElement(By.xpath("//h6[contains(.,'BUI:')]")).getText();
		System.out.println(nroBoleta);
		
		return nroBoleta;
	}
	
	public String direcionPagoElectronicoParaObtenerNroBuiParaTS49() {
		String nroBoleta="";
		
		driver.findElement(By.xpath("//a[contains(@href,'/Pago/RedireccionMedioDePago')]")).click();
		
		espera(3000);
		nroBoleta = driver.findElement(By.xpath("//h6[contains(.,'BUI:')]")).getText();
		System.out.println(nroBoleta);
		
		return nroBoleta;
	}
	
	
	public void btnContinuarClick() {
		btnContinuar.click();
		
		
	}
	
	public void inputDatoParticular(String datos) {
		(campos.get(0)).sendKeys(datos);
	}

	
	public void btnAceptar() {
		btnAceptar.click();
	}
	
	public void modificarParametros(String p1, String p2, String p3) {
		List<WebElement> campos =  driver.findElements(By.cssSelector(".btn-group.btn-group-justified"));//btn-group btn-group-justified
		System.out.println(campos.size());
		ArrayList<String> listA = new ArrayList<String>();
		listA.add(p1);
		listA.add(p2);
		listA.add(p3); // FORZAR BUI



		for(int i=0; i < 3 ; i++) {
			//espera(5000);
			List<WebElement> btnDef = driver.findElements(By.cssSelector(".btn.btn-default"));
 			System.out.println(btnDef.get(i).getText());
		
			if(listA.get(i).toString().toLowerCase().equals("no") && btnDef.get(i).getText().toLowerCase().equals("no")) {
				campos.get(i).findElement(By.cssSelector(".btn.btn-default")).click();
				continue;
			} 
			if(listA.get(i).toString().toLowerCase().equals("si") && btnDef.get(i).getText().toLowerCase().equals("activo")) {
				campos.get(i).findElement(By.cssSelector(".btn.btn-default")).click();
				continue;
			} 
			if(listA.get(i).toString().toLowerCase().equals("no") && btnDef.get(i).getText().toLowerCase().equals("inactivo")) {
				campos.get(i).findElement(By.cssSelector(".btn.btn-default")).click();
				continue;
			} 
			if(listA.get(i).toString().toLowerCase().equals("si") && btnDef.get(i).getText().toLowerCase().equals("si")) {
				campos.get(i).findElement(By.cssSelector(".btn.btn-default")).click();
				continue;
			} else {
				continue;
			}

		}
		
	}
	
	
	public void CrearTramiteConRapipago() {

		BackOfficePage BO = new BackOfficePage(driver);
		String periodo_tarifaria = "2018";
		String fecha_publicacion = base__fechaActual();
		String Cod1 = "1";
		String Cod2 = BasePage.random(50,60);
		String Cod3 = BasePage.random(1,99);
		String Descripcion = "concepto_automatizacion_" + Cod1 + "_" + Cod2 + "_" + Cod3; 
		String Renglon1 = "renglon1_concepto_automatizacion_" + Cod1 + "_" + Cod2 + "_" + Cod3;
		String Agencia_asignada = "01-Dir. Gral del Registro Civil y Capacidad de las Personas";
		String valor = "10";
		boolean es_redondeable = true;
		boolean tiene_precio_fijo = true;
		boolean cantidad_variable = true;
		boolean es_devolucion = true;
		String codigo = Cod1+"."+Cod2+"."+Cod3;
		BO.IngresoAmbiente();		
		BO.crearConceptoAprobarlo(periodo_tarifaria, fecha_publicacion, Cod1, Cod2, Cod3, Descripcion, Renglon1, Agencia_asignada, valor, es_redondeable, tiene_precio_fijo, cantidad_variable, es_devolucion,codigo);
		String dep1 = "Registro Civil";
		String dep2 = "Pago Electrónico";
		BO.asignarDependencia_Concepto(codigo, dep1, dep2);
		Assert.assertTrue(BO.asignacionDependenciaExitosa());
		
		BUIPage BUI = new BUIPage(driver);	

		BUI.IngresoAmbiente();
		BUI.atributosDelItem_agregar(dep1, codigo, "30", tiene_precio_fijo, !cantidad_variable, false);


		PagosPage PagosPage = new PagosPage(driver);
		String titulo_tramite = "tramite_automatizacion_" + random(1, 1000);//EL NOMBRE DEL TRAMITE SI PUEDE REPETIRSE
		String concepto_y_descripcion = base__formateaConceptoSinCero(codigo)+" "+Descripcion; //concept(formateadoSinCeros(concepto))

		PagosPage.IngresoAmbiente();
		PagosPage.crearTramite(titulo_tramite,true,dep1,concepto_y_descripcion,true, false,"VISA","","","");
		assertTrue(PagosPage.validarCreacionTramite("El trámite se guardó correctamente."));


		TramitesPage tramite = new TramitesPage(driver);

		String concepto = codigo +" "+ Descripcion;

		tramite.ingresoAmbientePortalTramiteAbm();
		tramite.seleccionarTarea("Trámites");
		tramite.crearNuevoTramite(5000);

		/*TODO todo esto deberia estar en un metodo COMPLETAR EDICION TRAMITE*/
		espera(5000);

		tramite.completarInput("data-ng-model","nombre",titulo_tramite);		//PASAR UN LIST LA PROXIMA
		tramite.completarInput("data-ng-model","descripcion",titulo_tramite);
		tramite.completarInput("data-ng-model","usuario","jbauchi");
		tramite.completarInput("data-ng-model","password","Ab123456");
		tramite.seleccionButton("SI","SI","NO");
		espera(5000);
		tramite.agregarNuevoGrupoConceptos(concepto);
		tramite.guardarTramite();
		espera(10000);
		assertTrue(tramite.validarCreacionTramite());



	}
}